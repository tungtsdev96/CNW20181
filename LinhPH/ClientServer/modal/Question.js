module.exports = class Question {

    constructor(vocabulary){
        this.vocabulary = vocabulary;
    }

    checkAnswer(answer) {
        if (answer == this.getCorrectAnswer()) {
            return true;
        }
        return false;
    }

    getCorrectAnswer() {
        return this.vocabulary.vocabularyEn;
    }

}