const Question = require("./Question");

module.exports = class ListenQuestion extends Question {

    constructor(vocab){
        super(vocab);
    }

    checkAnswer(answer){
        return super.checkAnswer(answer);
    }

    getCorrectAnswer() {
        return super.getCorrectAnswer();
    }

}