var MongoClient = require('mongodb').MongoClient;
const URL = "mongodb://root:ngan28091996@ds125073.mlab.com:25073/e-learning";
const DB_NAME = "e-learning";
var ObjectID = require('mongodb').ObjectID;

var DAO = {
    create: (collection, object, callback) => {
        MongoClient.connect(URL, function (err, db) {
            if (err) throw err;
            var dbo = db.db(DB_NAME);
            dbo.collection(collection).insertOne(object, function (err, res) {
                db.close();
                callback(err, res);
            });
        });
    },

    get: (collection, query, sort, limit, page, callback) => {
        var myLimit = 1000;
        if (limit == null || limit != undefined || limit <= 0) {
            myLimit = parseInt(limit + "");
        }

        var skip = (page - 1) * myLimit;
        MongoClient.connect(URL, function (err, db) {
            if (err) throw err;
            var dbo = db.db(DB_NAME);
            dbo.collection(collection).find(query).limit(limit).skip(skip).sort(sort).toArray(function (err, result) {
                db.close();
                callback(err, result);
            });
        });
    },

    update: (collection, query, newObject, callback) => {
        console.log("run db update");
        MongoClient.connect(URL, function (err, db) {
            if (err) throw err;
            var dbo = db.db(DB_NAME);
            dbo.collection(collection).update(query, newObject, function (err, res) {
                db.close();
                callback(err, res);
            });
        });
    },

    delete: (collection, query, callback) => {
        MongoClient.connect(URL, function (err, db) {
            if (err) throw err;
            var dbo = db.db(DB_NAME);
            dbo.collection(collection).deleteOne(query, function (err, res) {
                db.close();
                callback(err, res);

            });
        });
    },

    count : (collection, callback) => {
        MongoClient.connect(URL, function (err, db) {
            if (err) throw err;
            var dbo = db.db(DB_NAME);
            dbo.collection(collection).count().then(result => {
                callback(result)
            });
        });
    }
}


var exam = {
    name: "Actual Test - HUST 11/2018",
    descripton: "Đề thi thật tháng 11 của trường Đại học Bách Khoa Hà Nội",
    content: {
        listening: {
            audio: "http://54.95.40.41:3000/audio/audio1.mp3",
            question: [{
                    part: 1,
                    index: 1,
                    title: "",
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 1,
                    index: 2,
                    title: "",
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 1,
                    index: 3,
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 1,
                    index: 4,
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 1,
                    index: 5,
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 1,
                    index: 6,
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 1,
                    index: 7,
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 1,
                    index: 8,
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 1,
                    index: 9,
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 1,
                    index: 10,
                    image: "https://www.englishclub.com/images/esl-exams/toeic_office.jpg",
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 11,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 12,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 13,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 14,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 15,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 16,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 17,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 18,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 19,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 20,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 21,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 22,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 23,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 24,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 25,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 26,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 27,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 28,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 29,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 30,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 31,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 32,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 33,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 34,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 35,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 36,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 37,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 38,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 39,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 2,
                    index: 40,
                    title: "",
                    A: "",
                    B: "",
                    C: "",
                    D: "",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 41,
                    title: "Where is the man going?",
                    A: "To get lunch",
                    B: "To the store",
                    C: "To a meeting",
                    D: "To grab a report",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 42,
                    title: "How long will the man take?",
                    A: "About a day",
                    B: "About 2 hours",
                    C: "About 12 feet",
                    D: "About 45 minutes",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 43,
                    title: "Why doesn’t the woman go with the man?",
                    A: "She has a meeting",
                    B: "She has to finish a report",
                    C: "She wants a quick bite",
                    D: "She wants a sandwich",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 44,
                    title: "What is “Fast Help”?",
                    A: "It covers workers",
                    B: "It is a temp agency",
                    C: "It is an assistant",
                    D: "It is an office",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 45,
                    title: "Why does the temp need to be very patient?",
                    A: "The hours are long",
                    B: "Jack is crazy",
                    C: "Jack is very particular",
                    D: "The work is hard",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 46,
                    title: "Who is Sue?",
                    A: "She is sick",
                    B: "She is on staff",
                    C: "Mary’s friend",
                    D: "Jack’s assistant",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 47,
                    title: "Why is the woman unsure about attending the retreat?",
                    A: "She has plans to see fireworks",
                    B: "The retreat has been moved",
                    C: "She has a family event at the same time",
                    D: "The president is stopping by",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 48,
                    title: "Why was the company retreat moved?",
                    A: "So the boss can choose project leads",
                    B: "So the president could attend",
                    C: "Because of the family reunion",
                    D: "So the relatives could come",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 49,
                    title: "How long has it been since the woman has seen some of her relatives?",
                    A: "Almost 10 years",
                    B: "The weekend",
                    C: "Since yesterday",
                    D: "Since the last project",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 50,
                    title: "Where is this conversation taking place?",
                    A: " In the office",
                    B: "At the store",
                    C: "Over the phone",
                    D: "At the doctor’s",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 51,
                    title: "What is wrong with Mr. Smith’s computer?",
                    A: "It keeps crashing",
                    B: "It reboots by itself",
                    C: "It has a virus",
                    D: "It keeps freezing",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 52,
                    title: "Why can’t a technician come today?",
                    A: "They are at the doctor’s",
                    B: "They are busy",
                    C: "It’s the only time they have",
                    D: "The appointment is for tomorrow",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 53,
                    title: "Why is the Marriott expensive?",
                    A: "They have event planners",
                    B: "It is nice",
                    C: "The location is central",
                    D: "It’s slightly over the budget",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 54,
                    title: "What happened last year?",
                    A: "They went over budget.",
                    B: "The award presentation was at the office",
                    C: "They didn’t decide on a venue",
                    D: "There was an event planner",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 55,
                    title: "Where is the award presentation going to take place?",
                    A: "At the Marriott Hotel",
                    B: "At a central location",
                    C: "At Kincaid’s Restaurant",
                    D: "At a casual place",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 56,
                    title: "Who is the man speaking to?",
                    A: "A friend",
                    B: "A doctor",
                    C: "A coworker",
                    D: "A pharmacist",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 57,
                    title: "Does the man have a history of asthma in his family?",
                    A: "Yes, he has slight allergies",
                    B: "Yes, he has asthma",
                    C: "No, he is not aware of any",
                    D: "No, only when exercising",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 58,
                    title: "What should the man do until he sees the doctor again?",
                    A: "Take it easy",
                    B: "Exercise hard",
                    C: "Stop all activity",
                    D: "Nothing",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 59,
                    title: "Where is the man going to?",
                    A: "An Electronic Conference",
                    B: "The W Hotel",
                    C: "Hertz",
                    D: "San Jose",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 60,
                    title: "Why couldn’t the man stay at the W hotel?",
                    A: "His plane leaves at 8:00 PM",
                    B: "The Hilton is nice",
                    C: "The hotel was full",
                    D: "It is his favorite",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 61,
                    title: "What does the man want in the rental car?",
                    A: "Directions to the conference center",
                    B: "A map of the area",
                    C: "His wife",
                    D: "Never lost navigation system",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 62,
                    title: "What does the man have to do?",
                    A: "Remove a wart",
                    B: "Schedule surgery",
                    C: "Check with human resources",
                    D: "Have a procedure",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 63,
                    title: "How long will the man be out?",
                    A: "One week",
                    B: "One day",
                    C: "The next day",
                    D: "In and out",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 64,
                    title: "Where is the man having the procedure done?",
                    A: "At the human resources department",
                    B: "At the hospital",
                    C: "Out of the office",
                    D: "In the insurance policy",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 65,
                    title: "What is the woman asking the man to recommend?",
                    A: "New Year’s resolution",
                    B: "Good gyms in the area",
                    C: "Swimming pools",
                    D: "A variety of classes",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 66,
                    title: "Why does the man recommend the Hollywood Gym?",
                    A: "It is in the area",
                    B: "It has a variety of classes",
                    C: "It is the best",
                    D: "It has an Olympic size swimming pool",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 67,
                    title: "What will the woman do next?",
                    A: "Check out Hollywood Gym",
                    B: "Take a variety of classes",
                    C: "Go swimming",
                    D: "Start working out",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 68,
                    title: "Who is Jason Lee?",
                    A: "The best movie",
                    B: "An actor",
                    C: "Pretty to look at",
                    D: "An action movie",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 69,
                    title: "What does the woman think about Jason Lee?",
                    A: "He is a bad actor",
                    B: "He is pretty",
                    C: "He is a star",
                    D: "He is predictable",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 3,
                    index: 70,
                    title: "Why didn’t the woman like the movie?",
                    A: "It had a lot of action",
                    B: "Young women liked the movie",
                    C: "It starred Jason Lee",
                    D: "The acting was bad",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 71,
                    title: "Who is Samantha?",
                    A: "Smoking tour guide",
                    B: "African Safari Tour guide",
                    C: "Tram driver",
                    D: "Safety tour guide",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 72,
                    title: "Who should sit on the inside of the tram?",
                    A: "All children",
                    B: "People requiring assistance",
                    C: "People that need to pull the cord",
                    D: "Children 5 years and under",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 73,
                    title: "What should you keep inside the tram at all times?",
                    A: "Children",
                    B: "Smoking materials",
                    C: "Arms and legs",
                    D: "The cord",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 74,
                    title: "Who is Chan Lee?",
                    A: "A badminton player",
                    B: "A table Tennis player",
                    C: "A tennis player",
                    D: "A diver",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 75,
                    title: "What is Chan Lee’s ranking?",
                    A: "Unranked",
                    B: "Number 5",
                    C: "Number 7",
                    D: "Number 10",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 76,
                    title: "Why is the match historic?",
                    A: "It’s the first time Roger Wake made it to the finals",
                    B: "It’s the first time a Chinese woman made it to the finals",
                    C: "It’s the first time an unranked player made it to the finals",
                    D: "It’s the first time a person from China has reached the finals in a major event",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 77,
                    title: "What does the new technology do?",
                    A: "Leave a greasy film behind",
                    B: "Cures dry flaky skin",
                    C: "Allows lotion to be quickly absorbed into the skin",
                    D: "Gives a special price",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 78,
                    title: "How much is No More Flakes?",
                    A: "16 ounces",
                    B: "$9.99 plus tax and shipping",
                    C: "30 days",
                    D: "$19.98 plus tax and shipping",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 79,
                    title: "When will you see results?",
                    A: "Within days",
                    B: "In 30 days",
                    C: "16 days",
                    D: "Two weeks",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 80,
                    title: "Where are the passengers?",
                    A: "In New York",
                    B: "At an airport",
                    C: "Inside a plane",
                    D: "At the Marriott",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 81,
                    title: "How many people can get on the Northwest flight?",
                    A: "20",
                    B: "45",
                    C: "200",
                    D: "40",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 82,
                    title: "Who is making the announcement?",
                    A: "The mechanic ",
                    B: "A Northwest employee",
                    C: "Agent Jones",
                    D: "A Pan Air employee",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 83,
                    title: "Who is the talk for?",
                    A: "Peter",
                    B: "New employees",
                    C: "People with questions",
                    D: "Company employees",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 84,
                    title: "What should you do if you have questions?",
                    A: "Read the handbook",
                    B: "Ask Peter",
                    C: "Go to the meeting",
                    D: "Raise your hand",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 85,
                    title: "Where is the speaker’s office?",
                    A: "In the conference room",
                    B: "On the third floor",
                    C: "Next to the hall",
                    D: "End of the room",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 86,
                    title: "Where were the two women found?",
                    A: "Rainbow National Park",
                    B: "In a helicopter",
                    C: "Near a waterfall",
                    D: "Bear Trail",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 87,
                    title: "Why did the two women get lost?",
                    A: "They veered off course",
                    B: "They saw a waterfall",
                    C: "They saw a bear",
                    D: "They spotted a helicopter",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 88,
                    title: "When were the two women reported missing?",
                    A: "Two days ago",
                    B: "Wednesday",
                    C: "Friday",
                    D: "Earlier in the day",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 89,
                    title: "Where are you most likely to hear this talk?",
                    A: "On television",
                    B: "Over the telephone",
                    C: "At the bus station",
                    D: "At a hospital",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 90,
                    title: "What is the quickest way to speak to a representative?",
                    A: "Hang up and try again",
                    B: "Wait patiently",
                    C: "Call a different number",
                    D: "Keep pressing 0",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 91,
                    title: "What do you do about a claim?",
                    A: "Press or say 1",
                    B: "Press or say 2",
                    C: "Press or say 3",
                    D: "Call 1-800-555-3483",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 92,
                    title: "How often is the brunch?",
                    A: "Once a year",
                    B: "Monthly",
                    C: "Quarterly",
                    D: "Four times a year",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 93,
                    title: "What is NOT true of the event?",
                    A: "There are over 150 attendees",
                    B: "Everyone is a mystery writer",
                    C: "It is a dinner",
                    D: "Mark Clark is the guest speaker",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 94,
                    title: "Where should table 5 go to get food?",
                    A: "The back table",
                    B: "The table to the left of the speaker",
                    C: "The table to the right of the speaker",
                    D: "The buffet table",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 95,
                    title: "What is NOT true of Billy Thompson?",
                    A: "He was the 35th President of the U.S",
                    B: "He was a resident of Mexico",
                    C: "He had four children",
                    D: "He served two terms as president",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 96,
                    title: "What was the cause of death?",
                    A: "Old age",
                    B: "Heart attack",
                    C: "Unknown",
                    D: "Died peacefully",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 97,
                    title: "Where is the funeral?",
                    A: "Maryland",
                    B: "Nantucket",
                    C: "Maine",
                    D: "National Cemetery",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 98,
                    title: "What is the purpose of this announcement?",
                    A: "Let people know about an accident",
                    B: "Talk about an accident",
                    C: "Give people a traffic update",
                    D: "Tell what the police are doing",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 99,
                    title: "Where was the mini van?",
                    A: "In the left lane",
                    B: "In the right lane",
                    C: "In the middle lane",
                    D: "Driving",
                    answer: "A",
                    explanation: ""
                },
                {
                    part: 4,
                    index: 100,
                    title: "If you are on northbound 75, what should you do?",
                    A: "Wait in traffic",
                    B: "Get off and find another route",
                    C: "Call the police",
                    D: "Go have dinner",
                    answer: "A",
                    explanation: ""
                },
            ]
        },
        reading: {
            part5: [],
            part6: [{
                    common: 'Advertisement',
                    questions: [{
                            part: 6,
                            index: 141,
                            title: "Are you tired of constantly using more minutes than what is allowed in your cell phone plan? Do you want aplan that is truly unlimited? Do you want plan that has unlimited minutes, unlimited texts, unlimited video and picture messaging _____________having to pay an arm and a leg?",
                            A: "throughout",
                            B: "without",
                            C: "between",
                            D: "toward",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 6,
                            index: 142,
                            title: "Freewire now introduces a plan that has all the ___________cell phone users want without the expensive cost.",
                            A: "tools",
                            B: "gadgets",
                            C: "features",
                            D: "texting",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 6,
                            index: 143,
                            title: `With the new Freewire plan, you get unlimited minutes for domestic calls and a special discount for
                            international calls. You also get unlimited texting and unlimited video and picture messaging for a monthly fee
                            of $99.99. For a limited time, if you sign a two year contract, we will also ____________ a new Silverberry
                            phone for free. Imagine owning the new Silverberry phone for absolutely free. This offer is only available for
                            until May, so hurry and visit any Freeiwire store near you`,
                            A: "give",
                            B: "present",
                            C: "throw",
                            D: "include",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: 'Promotion Email',
                    bellow_common: `I want thank you again and encourage each of you to make next year another successful year!
                    Best Regards,
                    Amanda Winthrop
                    VP, Sales`,
                    questions: [{
                            part: 6,
                            index: 144,
                            title: `To: All Sales Employees
                            From: Amanda Winthrop
                            Date: December 20
                            Re: Bonus
                            Dear Sales employees:
                            I want to congratulate you on another successful year! Due to your hard work, we have again achieved the
                            _____________of selling the most number of laptops in the Midwest region. `,
                            A: "record",
                            B: "plan",
                            C: "sales",
                            D: "purpose",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 6,
                            index: 145,
                            title: `Headquarters has recognized your efforts and approved a 10% bonus for all of you! This bonus will be added to
                            your paycheck next month. I am extremely proud of each and every one of you. Headquarters is also
                            determining a salary increase for all sales employees based on their yearly ___________ reviews. `,
                            A: "negative",
                            B: "positive",
                            C: "academic",
                            D: "performance",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 6,
                            index: 146,
                            title: `I have submitted your performance evaluations to headquarters, and the salary increase announcements
                            should be made in the ________ future.
                            `,
                            A: "near",
                            B: "far",
                            C: "further",
                            D: "close",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: 'Real Bank',
                    bellow_common: ``,
                    questions: [{
                            part: 6,
                            index: 147,
                            title: `Are you tired of having to listen to one recording after another when you call your bank? Do you get
                            ____________with all the numbers you need to push in order to just find the balance of your account?`,
                            A: "tired",
                            B: "frustrated",
                            C: "accustomed",
                            D: "mutilated",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 6,
                            index: 148,
                            title: `Do you wish sometimes that you can speak to a real, live person? Well, at Real Bank you can. You can reach a
                            live customer service representative anytime of the day, 24/7. We believe our customers __________the best
                            service.`,
                            A: "deserving",
                            B: "deserve",
                            C: "deserved",
                            D: "deserves",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 6,
                            index: 149,
                            title: `We don’t believe you need to waste your time navigating through a complicated phone tree just find the
                            balance on your account or for any other needs. Every time you call us, all you need to do is push “0” in order
                            to speak __________ a live person. It’s that easy. So, what are you waiting for? Call us!`,
                            A: "with",
                            B: "of",
                            C: "to",
                            D: "for",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            common: 'Reservation Confirmation',
                            bellow_common: `Should you have any questions, please do not hesitate to contact me.
                            Sincerely,
                            Frank Vandersleet
                            Conference Manager`,
                            questions: [{
                                    part: 6,
                                    index: 150,
                                    title: `Autsin Hotel
                                    1111 Francy Street
                                    Austin, TX 7777
                                    May 27
                                    Dear Mr. Smith,
                                    Enclosed, please find a copy of the reservation confirmation form. We are holding the Big Event Conference
                                    Hall for you from August 10 to the 16th. In order for us to __________ this reservation, you will need to fill out 
                                    the form and send us a check for $2,300. `,
                                    A: "confirmation",
                                    B: "confirm",
                                    C: "confirmed",
                                    D: "confirms",
                                    answer: "A",
                                    explanation: ""
                                },
                                {
                                    part: 6,
                                    index: 151,
                                    title: `This amount is 10% of the total price of the conference hall for your event. As I mentioned over the phone, the
                                    maximum _____________for the conference hall is 300 people`,
                                    A: "number",
                                    B: "seats",
                                    C: "capacity",
                                    D: "people",
                                    answer: "A",
                                    explanation: ""
                                },
                                {
                                    part: 6,
                                    index: 152,
                                    title: `If you should require additional seating, we are able to open up the adjacent hall for 100 more people. We will
                                    not be able to accommodate more than 400 people. The meal options are also included on this form. Please
                                    make sure to fill out this ______________ and inform us of your meal choices`,
                                    A: "contract",
                                    B: "memo",
                                    C: "confirmation",
                                    D: "section",
                                    answer: "A",
                                    explanation: ""
                                },
                            ]
                        },
                    ]
                },
            ],
            part7: [
                {
                    common: `IMPORTANT TERMS:
                    1) Room cancellations must be made in writing, twenty (20) days prior to your arrival date.
                    2) If you do not cancel your reservation, you are financially liable and will be charged for your
                    reserved stay for up to three (3) days.
                    3) Guests may not transfer reservations to a third party in the event of a cancellation.
                    4) Nonrefundable payment for entire stay is due at time of check-in.
                    5) If you do not agree with our terms, please do not sign this confirmation form.
                    6) A signed confirmation form & credit card number will guarantee your room.`,
                    questions: [
                        {
                            part: 7,
                            index: 153,
                            title: "What is the purpose of this notice?",
                            A: " To inform guests on how to reserve a room",
                            B: "To inform guests of a new policy",
                            C: "To inform guests of the terms of cancellation",
                            D: " To inform guests on how to pay for their reservation",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 154,
                            title: " In order to cancel a reservation, the guest must:",
                            A: "Call the reservation office 20 days before they arrive",
                            B: "Call the reservation office 3 days before they arrive",
                            C: "Send the hotel a written notice 20 days before they arrive",
                            D: "Send the hotel a written notice 3 days before they arrive",
                            answer: "A",
                            explanation: ""
                        }
                    ]
                }, 
                {
                    common: `Exceptional Mexican Food………….El Torrito
                    Under New Ownership
                    Open 7 days a week, 10 am to 10 pm
                     COUPON COUPON COUPON
                     $5 off 50% off Free Children’s Meal
                     Any order over $15 with this Buy 1 meal and get 2nd one With a purchase of an adult
                     coupon. 50% off. meal.
                     Not valid with any other offers. Not valid with any other offers. Not valid with any other offers.
                     Offer good until Feb. 14 Offer good until Feb. 14 Offer good until Feb. 14`,
                    questions: [
                        {
                            part: 7,
                            index: 155,
                            title: "What is the purpose of this advertisement?",
                            A: "To encourage customers to eat more Mexican food",
                            B: "To advertise a new restaurant",
                            C: "To attract more children to the restaurant",
                            D: "To attract more customers to the restaurant",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 156,
                            title: "What will the customers receive if they spend more than $15?",
                            A: "50% off their meal",
                            B: "A $5 discount on their bill",
                            C: "A free children’s meal",
                            D: "A free second meal",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 157,
                            title: "What will happen after February 14?",
                            A: "There will be a new owner",
                            B: "The coupons will no longer be valid",
                            C: "The restaurant will offer a new menu",
                            D: "The restaurant will change their business hours",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 158,
                            title: "Why can’t the customer use two coupons together?",
                            A: "The coupons are only valid at the El Torrito",
                            B: "The coupons expire on Feb. 14",
                            C: "Each coupon is not valid with other offers",
                            D: " It is the restaurant’s policy",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `Winter Wonderland
                    January 22
                    10 am to 3 pm
                    All employees are invited to Rittco’s annual team-building day. Join us for a fun
                    day of ice skating. Bring your family and enjoy a day at Uncle Jack’s Skating Park.
                    Tickets include:
                    Skate Rental
                    2-30 minute skating sessions
                    Hayride
                    Lunch
                    Cost: $7 for each adult and $4 for each child under the age of 10.
                    Please make your reservations by January 18.
                    You can send an email to Janine in Human Resources.
                    (This event is only open to employees and their family members)`,
                    questions: [
                        {
                            part: 7,
                            index: 159,
                            title: "Why is the company holding this event?",
                            A: "To encourage the employees to skate more",
                            B: "To build team-work between the employees",
                            C: "To help struggling families",
                            D: "To support Uncle Jack’s Skating Park",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 160,
                            title: "The event is only open to?",
                            A: "Employees",
                            B: "Employees and their friends",
                            C: "Employees and their family members",
                            D: "Employees and their spouses",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 161,
                            title: "If 2 parents and their 6 and 11 year old child attend, how much will it cost?",
                            A: "$22",
                            B: "$24",
                            C: "$28",
                            D: "$18",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `Memo
                    To: All Staff
                    From: Judy Lawrence
                    Date: June 1
                    Re: New electronic memo format
                    There have been many complaints of not being able to understand the interoffice memos. In order to increase interoffice
                    communications, we have developed guidelines for all memos. These guidelines will take effect immediately.
                    • Concisely state the purpose of the memo in the subject line.
                    • The first paragraph of the memo should summarize the purpose of your memo.
                    • Use professional, simple, and polite language.
                    • Proofread your memo for any mistakes.
                    • Run the spell checker on your program.
                    • Address the memo to the person(s) that will be responsible for taking action and CC all others that need
                     to know about the action.
                    • Keep the memo to only one page. If you have additional information, attach a file.
                    We hope these new guidelines will increase our communication and in turn increase productivity. If you have any
                    questions, please do not hesitate to contact me. Thank you.`,
                    questions: [
                        {
                            part: 7,
                            index: 162,
                            title: "What is the purpose of this memo?",
                            A: "To inform the staff to communicate effectively",
                            B: "To advise the staff of new email procedures                            ",
                            C: " To inform the staff of a new memo format",
                            D: "To inform the staff of new vacation guidelines",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 163,
                            title: "What should you do if your memo is over two pages long?",
                            A: "Make sure it’s well organized",
                            B: " Inform the recipients that it’s more than two pages long",
                            C: "Use smaller fonts",
                            D: "Put the main points on one page and attach a file",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 164,
                            title: "Why did Judy Lawrence implement these new guidelines?",
                            A: "The employees did not know how to send a memo",
                            B: "The old memo format was hindering communication",
                            C: "The employees asked for a new format",
                            D: " She was new to the position and wanted to start fresh",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 165,
                            title: "If you have five people that will receive the memo, whose name should you put on the “To” field?",
                            A: " Put all five names in the “To” field                            ",
                            B: "Put the person(s) that will take action in the “To” field, and CC all others",
                            C: "List two names on the “To” field and put the other three names on the “CC” field",
                            D: " Send the memo to Judy Lawrence and put the five names on the “CC” field                            ",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `Vital Software Company
                    Information meeting of new products
                    November 3, 9 am to 12 pm
                    Agenda
                    1. Introduction – Yuri Petrov
                    a. Introductions
                    b. Review agenda and objectives
                    2. History of company – James Podell
                    a. Briefly present how company began
                    b. Share mission statement and where the company is headed.
                    3. Project Overview – Shakra Singh
                    a. Introduce development schedule
                    b. Introduce marketing strategy and target countries
                    4. Training & Consulting– Mary Kim
                    a. Introduce training sessions for new employees
                    b. Consulting schedule for companies interested in product
                    5. Wrap up – Yuri Petrov
                    a. Question and Answers
                    b. Distribute handouts
                    c. Announce time and date for next meeting`,
                    questions: [
                        {
                            part: 7,
                            index: 166,
                            title: "Why was this meeting held?",
                            A: "To talk about new products",
                            B: "To talk about new training programs",
                            C: "To train engineers",
                            D: " To review a contract",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 167,
                            title: "Who would be the best person to help you schedule a meeting to review the product?                            ",
                            A: "Yuri Petrov",
                            B: "James Podell",
                            C: "Shakra Singh",
                            D: "Mary Kim",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 168,
                            title: "What is Shakra Singh probably in charge of?",
                            A: "Publicity",
                            B: "Legal Advice",
                            C: "Advertising",
                            D: "Software",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `May 15, 2003
                    Global Distribution Inc.
                    1718 Madison Way
                    Huntville, Maryland 21031
                    Attention: Kirk D. Witt
                    Re: Termination of Distribution Agreement dated February 4, 2001
                    Dear Kirk:
                    New World is in the process of consolidating our distributors and due to the launch of our new
                    product, The Pincher, we have decided to terminate our contract with Global Distribution. Under
                    Section 1.4 of the distribution agreement dated February 4, 2001, New World may terminate the
                    contract anytime with a 30 day written notice. This letter serves as notice that the termination will
                    be effective as of June 16, 2003.
                    Thank you,
                    Robert S. Fountain, Esq.
                    New World`,
                    questions: [
                        {
                            part: 7,
                            index: 169,
                            title: "Who wrote the letter?",
                            A: " A distributor for Global Distribution",
                            B: "A tax preparer for Global Distribution                            ",
                            C: "An attorney for New World",
                            D: "The CEO for New World",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 170,
                            title: "The word consolidating in line 1 is closest in meaning to",
                            A: "recommending",
                            B: "selling",
                            C: "scheduling",
                            D: "combining",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 171,
                            title: "Why does New World want to terminate the contract?",
                            A: "They have a new product",
                            B: "They have a new distributor",
                            C: "They have a new agreement",
                            D: "They have a new owner",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 172,
                            title: "What will happen on June 16, 2003?",
                            A: "The new product will be launched",
                            B: "The contract will end",
                            C: "The contract will end 30 days later",
                            D: "Global Distribution will get a new contract",
                            answer: "A",
                            explanation: ""
                        }
                    ]
                },
                {
                    common: `Marketing Assistant
                    DesignTech is a fast-growing advertising agency. Our company has won three years in a row the Adfly award
                    honoring creative ADVERTISING in package design, print, radio and television. We currently have a position
                    open for a marketing assistant.
                    Position Responsibilities:
                    • Administrative duties in support of the Vice President of Marketing including travel coordination
                    • Prepare presentation materials for meetings and conferences
                    • Assist with coordinating activities and events related to the Marketing Department
                    • Must be flexible with an ever changing environment – able to multi-task and change direction quickly.
                    Position Qualifications:
                    • BA/BS degree preferred
                    • Proven advanced proficiency in Word, Excel, PowerPoint, Outlook, Visio
                    • Ability to work independently
                    • Excellent communication and interpersonal skills as well as good decision making skills
                    • Outstanding organizational and time management skills; exhibit accuracy when working with details
                    Anyone interested in the position should send a resume along with a cover letter to jobs@designtech.com.`,
                    questions: [
                        {
                            part: 7,
                            index: 173,
                            title: " In order to apply for this position, a candidate must have the following requirement?",
                            A: "A college degree",
                            B: " A background in advertising",
                            C: "Presentation skills",
                            D: "Attention to details",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 174,
                            title: "The marketing assistant will be required to?",
                            A: "Be present for all meetings and conferences",
                            B: "Coordinate activities and events for the VP of marketing",
                            C: "Work in a fast-paced environment",
                            D: "Make travel arrangements for the company",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 175,
                            title: "Adfly is an award given to",
                            A: "Marketing Assistants",
                            B: "Radio and Television companies",
                            C: "Marketing Companies",
                            D: "Advertising Companies",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `Used Titus Racer X full suspension XS mountain bike - $1800
                    This bike is very well built, light weight and fast. It’s a full suspension bicycle that climbs hills really well and
                    feels very comfortable. Many have reviewed this as one of the few best full-suspension bikes out there.
                    I bought this bike used from a woman in California. She had 4 other mountain bikes and this one was sitting in
                    the garage. Since purchasing the bike in June 2008, I’ve put fewer than 100 miles on it. Since moving to Texas,
                    I no longer have time to ride this bike. I have also put on new tires and breaks. You can’t get a better deal for
                    your money.
                    This is an XS women’s bike so it will fit a petite woman (5’3 or under) or a child. The price is not negotiable.`,
                    questions: [
                        {
                            part: 7,
                            index: 176,
                            title: "The owner of this mountain bike is probably?",
                            A: "A petite man",
                            B: "A petite woman",
                            C: "A tall woman",
                            D: "A tall man",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 177,
                            title: "Why is the owner selling the bike?                            ",
                            A: "The owner bought the bike used",
                            B: "The owner moved to California",
                            C: "The owner does not have time to ride the bike",
                            D: "The owner has 4 other bikes",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `MEMO
                    Date: August 1, 2008
                    To: All Employees
                    From: Vice President of Human Resources
                    Subject: Company shutdown
                    As you know, it is our policy to close for vacation for a two-week period during the month of August. This year,
                    the offices and the plant will be closed from August 11th to August 24th, and we will reopen August 25th. All
                    employees who have been with us for one year will be paid in full for the two-week period.
                    Employees who have worked less than a year but more than five months will be paid for one week of the
                    vacation. Employees who have been with the company for ten years or more are entitled to one additional
                    week of vacation, which they may take at a time that is mutually convenient for them and their departments.
                    We recognize the importance of rest and recreation, so it is our sincere hope that you will enjoy this period of
                    vacation.`,
                    questions: [
                        {
                            part: 7,
                            index: 178,
                            title: "Why did the VP of Human Resources write this memo?",
                            A: "To inform the employees of the date of the two-week company shutdown",
                            B: "To encourage the employees to take their two-week vacations during August",
                            C: "To explain why the plant will be closed for two weeks",
                            D: "To explain why the company will shut down",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 179,
                            title: "Who will NOT be paid in full for the two-week period?",
                            A: "Employees that have been with the company for at least one year",
                            B: "Employees that have been with the company for at least ten years",
                            C: "Employees that have been with the company for less than five months",
                            D: "Employees that have been with the company for over five months",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 180,
                            title: "What is the probable reason the company is shutting down for two weeks?",
                            A: "To encourage employees to work harder",
                            B: "To increase sales",
                            C: " To encourage competition",
                            D: "To encourage employees to rest",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `The Vacation of Your Dreams!
                    Imagine yourself experiencing the warm, salty breezes of the Caribbean Sea as you take in the sunset from the
                    upper level of a vast cruise ship! You’ll be able to toast to another day in paradise as the sun melts into the
                    crystal blue water and the night life awakes aboard an all-inclusive 5 day/4 night cruise of the Bahaman Islands.
                    The ship leaves from Miami, Florida bright and early on Saturday, September 18th and travels through the
                    tranquil waters of the Caribbean, stopping at three ports of call including: Freeport, Nassau, and Andros Town.
                    (The ship returns to Miami on Wednesday, September 22nd.) Shopping, dining, and local attractions await you
                    at each and every port. While aboard the ship, you will experience gourmet meals, swimming, gym access, spa
                    services, organized activities and games, exciting night life, dancing, shows, or… nothing at all! You choose! All
                    of this is available for the low price of $399/person for inside cabins and $499/person for sea view cabins. You
                    may also upgrade to a suite for $599/person. (Taxes not included.) This is a limited opportunity as the ship will
                    fill fast, and prices like this will not be offered again! How are we able to offer paradise at this price, you ask?
                    This is a maiden voyage for our cruise line, and we are extending savings to you to introduce our services. All
                    we ask is that you fill out our nightly survey on your experiences on the ship. If you are interested in reserving
                    your room on a spacious, brand new, luxury cruise ship for its maiden voyage into the Caribbean Sea, please
                    email me at CarolWatkins@CarribeanCruiseLine.com. The vacation of your dreams is closer than you think!
                    To: CarolWatkins@CarribeanCruiseLine.com
                    From: JWerner@gomail.net
                    Subject: Caribbean Cruise
                    Dear Carol,
                    I am very interested in being a part of Caribbean Cruise Line’s maiden voyage, but I have a few questions
                    before I make reservations with you.
                    Does the price include transportation to Miami?
                    1. Do I need to have a passport to go on a cruise?
                    2. What are the differences in the sea view cabins and the suites?
                    3. What does the entertainment on the ship include?
                    I look forward to hearing back from you as I don’t want to miss out on an opportunity like this! It sounds very
                    exciting! Thanks in advance for answering my questions.
                    Sincerely,
                    John Werner`,
                    questions: [
                        {
                            part: 7,
                            index: 181,
                            title: "What are some activities guests can enjoy aboard the ship?                            ",
                            A: "Gourmet meals and shopping",
                            B: "Swimming and Gym Access",
                            C: "Swimming and local attractions                            ",
                            D: "Gourmet meals and local attractions",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 182,
                            title: "How much will an upgrade to a suite cost per person?",
                            A: "$399",
                            B: " $499",
                            C: " $599",
                            D: "$699",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 183,
                            title: "This cruise if probably best for people who like",
                            A: "An exciting night life",
                            B: "To shop",
                            C: "To relax",
                            D: "All of the above",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 184,
                            title: "Why did John Werner write this email to Carol? ",
                            A: "He wants to make a reservation for the cruise",
                            B: "He has never been on a cruise before and is nervous",
                            C: "He doesn’t want to miss this opportunity",
                            D: "He needs some clarification",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 185,
                            title: "The word opportunity in line 7 of John’s email is closest in meaning to",
                            A: "chance",
                            B: "vacation",
                            C: "cruise",
                            D: "entertainment",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `To: Patty Raymond < praymond@firstamericanbank.net >
                    From: Amy Strain < amy_strain@yahoo.com >
                    Subject: Account #0368338126
                    Dear Ms. Raymond,
                    I recently received my bank statement for the month of November, and I have a couple of concerns. I hope you
                    can help me figure them out.
                    I noticed on November 10th I was charged $2.50 to withdraw funds from an ATM inside Riverside Mall. It was
                    my understanding, when I opened the account, that I was able to use any ATM and not accrue service charges.
                    While $2.50 may not seem like a lot, it can certainly add up!
                    On November 25th, I noticed that $10.00 was withdrawn from my account, and I cannot figure out why. I work
                    hard to keep up with my check register, and I’m quite certain that I haven’t bounced any checks.
                    Your reply is much appreciated because I am really upset by these charges and withdrawals. I would like for my
                    account to be corrected as soon as possible.
                    Thank you,
                    Amy Strain
                    To: Amy Strain < amy_strain@yahoo.com >
                    From: Patty Raymond < praymond@firstamericanbank.net >
                    Subject: RE: Account #0368338126
                    Dear Ms. Strain,
                    I am sorry for the confusion regarding your November statement. Allow me to explain the charge and
                    withdrawal so that you understand where they came from.
                    The ATM fee of $2.50 on November 10th came from the financial institution that owns the ATM machine
                    (National Banking Corp). First American Bank allows you to use outside ATMs without incurring any extra
                    charges, however, we cannot guarantee that there will not be a charge to your account from the bank that
                    owns the machine. In this case, National Banking Corp charged you $2.50 since you are not a customer of their
                    bank. As a customer courtesy, we will credit your account $2.50 to reimburse this fee. In the future, please be
                    sure that you agree to any fees from outside ATMs before completing the transaction.
                    You were charged $10.00 on November 25th because a check you deposited from Cindy White was returned
                    due to insufficient funds. Unfortunately, we must withdraw the money from your account since that check was
                    returned. Perhaps you can request a new check from the individual, or you may ask for cash or a cashier’s
                    check to guarantee funds.
                    I hope your questions have been answered, and I hope that you enjoy being a customer of First American. We
                    are proud to have you as a customer.
                    Sincerely,
                    Patty Raymond
                    Account Representative
                    First American Bank`,
                    questions: [
                        {
                            part: 7,
                            index: 186,
                            title: "Why did Ms. Strain write the email?",
                            A: "She is upset about the new bank policy",
                            B: " She has some questions about her account",
                            C: "She wants to change banks",
                            D: "She wants to change banks",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 187,
                            title: "The word accrue in the first email, second paragraph, line 2 is closest in meaning to",
                            A: "add",
                            B: "subtract",
                            C: "increase",
                            D: "decrease",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 188,
                            title: "Who is Ms. Raymond?",
                            A: "The bank president",
                            B: "A customer",
                            C: "A bank employee",
                            D: "A bank owner",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 189,
                            title: "Who charged Ms. Strain $2.50 on November 10th?",
                            A: "National Bank Corp",
                            B: "First American Bank",
                            C: "Riverside Mall",
                            D: "Cindy White",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 190,
                            title: "Why did the bank withdraw $10 from Ms. Strain’s account?",
                            A: " As a courtesy",
                            B: "For using the ATM",
                            C: "As a service fee",
                            D: "For a returned check",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `Grand Opening Contest (Advertisement)
                    Delectable Deli Grand Opening!
                    We would like to welcome you to the grand opening of the newest, freshest, most exciting deli in town:
                    Delectable Deli! We will be open for business starting February 12, 2010, and we would love for you to join us!
                    Our hours of operation are from 11:00 – 2:30 Monday through Saturday for lunch and 5:00 – 9:00 Monday
                    through Thursday for dinner. We will have extended hours Friday and Saturday (9:00 – 11:00) for drinks and
                    entertainment on the patio.
                    Our menu includes fresh and creative salads, sandwiches, and soups for the lunch hours and an extensive menu
                    of entrees for the dinner hours. We will have a daily lunch and dinner special that will be too good to pass up,
                    but if you do we will understand because the rest of the menu is fabulous, as well! We are located in The
                    Commons shopping center at the Southwest entrance.
                    Now for the fun! We invite you to sign up for our Grand Opening Contest. Simply email us at
                    DelectableDeli@comcast.net to tell us why you are excited to have a new deli in town and what you would like
                    to see from our restaurant. If your name is chosen, you will win one lunch or dinner (your choice) per week
                    through the 2010 year! In this economy, who wouldn’t be excited to win that? We look forward to seeing and
                    hearing from all of you!
                    Dana Dawson, Owner/Operator
                    Delectable Deli
                    (813)547-9832
                    To: DelectableDeli@comcast.net
                    From: A_Smith@yahoo.com
                    Subject: Delectable Deli Contest
                    I am so excited to hear about your opening next week, and I look forward to trying out your new deli. I would
                    love to put my name in the hat to win your awesome contest! Your deli is just down the street from where I
                    work, and not too far from my home. It would be so convenient to frequent your establishment. I love the
                    idea of “fresh and creative” foods, and I look forward to seeing the things that you offer. I really would like to
                    see some flavored teas and small desserts offered to go along with what sounds to be an interesting menu
                    already. It is important, in my opinion, to help support local eateries and entrepreneurs, so I plan on visiting
                    your deli – win or lose! I appreciate you taking the time to offer such a contest to show the public that you
                    appreciate their business. Looking forward to your opening soon!
                    Allison Smith
                    (813)547-0179
                    `,
                    questions: [
                        {
                            part: 7,
                            index: 191,
                            title: "What is Dana Dawson’s job?                            ",
                            A: "A deli worker",
                            B: "A business owner",
                            C: "An advertiser",
                            D: "A waitress",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 192,
                            title: "The word extensive in the advertisement, second paragraph, line 1 is closest in meaning to                            ",
                            A: "wide",
                            B: "broad",
                            C: "variety",
                            D: "delicious",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 193,
                            title: "What is the prize for winning the Grand Opening Contest?",
                            A: "A patio",
                            B: "365 meals in 2010",
                            C: "52 meals in 2010",
                            D: "One free meal",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 194,
                            title: "The word establishment in the email, line 3 is closets in meaning to?",
                            A: "A beginning",
                            B: "A deli",
                            C: "A promise ",
                            D: "A business",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 195,
                            title: "Why did Ms. Smith write this email?                            ",
                            A: " She is excited to visit the deli                            ",
                            B: "She wants to enter the contest",
                            C: "She wants to compliment the owner",
                            D: "She is displeased with the deli’s location",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
                {
                    common: `June 10, 2006
                    Horrizon Cellular
                    222 Main Street
                    Windale, AZ
                    To Whom It May Concern:
                    I would like to have my account closed so that I may transfer it to Western Wireless. There are several reasons
                    why I have become unhappy with Horizon Cellular and have decided to request a transfer to Western.
                    • I feel the $75 monthly fee is rather exorbitant for the services provided. That money only covers 300
                    minutes of phone usage! If I want to use any texting options or internet usage, I have to pay on an
                    individual basis, and these things are extremely costly!
                    • I have too many dropped calls. Your coverage shows that I should have no problem with coverage all
                    over town, yet calls are dropped on a weekly basis.
                    • I would like the opportunity to upgrade my phone more often. With your company, I only have that
                    option once every two years.
                    • My experiences with customer service have been more than lacking. I haven’t had my questions
                    answered or my problems resolved.
                    I hope that you will allow me to close my account without any trouble. I understand that there is a fee for
                    cancelling, but I do not feel I need to pay the fee since you are not meeting your end of the contract. I look
                    forward to hearing from you so that I may move on to Western Wireless.
                    Thank you,
                    Kimberli Steward
                    Cellular phone number: 915-656-0512
                    Account number: 0313938128
                    June 17, 2006
                    Kimberli Steward
                    21 Creekway Circle
                    Plano, TX
                    Dear Ms. Steward,
                    Let me begin by extending my regrets that you have had a negative experience with Horizon Cellular. We strive
                    to have a cellular company you can enjoy and be proud of. I see that you have listed several items that you are
                    unhappy with. While I am saddened to see so many areas that have caused problems for you, I do appreciate
                    you bringing them to my attention. I would like to offer a free upgrade to you at this time. We also have new
                    cellular plans that do include texting and internet usage. Enclosed, you will find a flyer showing the packages
                    we offer. `,
                    questions: [
                        {
                            part: 7,
                            index: 196,
                            title: "Why did Ms. Steward write the letter?",
                            A: "She wants to cancel her contract with Horizon Cellula",
                            B: " She is requesting a new contract with Western Wireless",
                            C: "She is expressing her concerns with the services provided by Western Wireless",
                            D: "She just moved and needs new cellular service",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 197,
                            title: "Why can’t Ms. Steward upgrade her phone every year?",
                            A: " It’s a new phone",
                            B: "It’s not part of her contract",
                            C: "Horizon Cellular doesn’t have many phones",
                            D: "She is a picky customer",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 198,
                            title: "Why does Ms. Steward feel she should not have to pay the cancellation fee for closing her account?                            ",
                            A: "Her contract has expired",
                            B: "She thinks Horizon Cellular broke the contract",
                            C: "Western Wireless is offering a better deal",
                            D: "She doesn’t have enough money",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 199,
                            title: "The word regrets in the second letter, first paragraph, line 1 is closest in meaning to",
                            A: "anger",
                            B: "gladness",
                            C: "sadness",
                            D: "feelings",
                            answer: "A",
                            explanation: ""
                        },
                        {
                            part: 7,
                            index: 200,
                            title: "Why does Mr. Grant offer Ms. Steward a free upgrade?",
                            A: "He is upset with the services Horizon Cellular has offered",
                            B: "He thinks Ms. Steward is a great customer",
                            C: "Horizon Cellular has made a lot of money",
                            D: "He doesn’t want to lose Ms. Steward as a customer",
                            answer: "A",
                            explanation: ""
                        },
                    ]
                },
            
            ],
        }
    }
}
// DAO.create("Examinations", exam, function (err, res) {

// })

module.exports = DAO;