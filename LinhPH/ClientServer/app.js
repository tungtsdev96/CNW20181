var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var upload = require("express-fileupload")
var ExaminationRouter = require('./routes/ExaminationRouter');
var QuizRouter = require('./routes/QuizRouter');
var UserRouter = require('./routes/UserRouter');
var app = express();
app.use(logger('dev'));
app.use(express.json());
app.set("view engine", "ejs");
app.set("views", "./views")
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use('/public', express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "GET,PUT,POST,DELETE,PATCH,OPTIONS, Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next(); 
});
app.use(upload());
app.use('/', ExaminationRouter);
app.use('/', UserRouter);
app.use('/', QuizRouter);


// vocabulary service
const vocabularyService = require('./api/VocabularyService');
app.use('/', vocabularyService);

// question to learn service
const QuestionToLearnService = require('./api/QuestionToLearnService');
app.use('/', QuestionToLearnService);

// detail learn vocab service
const DetailLearnVocabService = require('./api/DetailLearnVocabService');
app.use('/', DetailLearnVocabService);

// detail favorite vocab service
const DetailFavoriteVocabService = require('./api/DetailFavoriteVocabService');
app.use('/', DetailFavoriteVocabService)

// topic service
const TopicService = require('./api/TopicService');
app.use('/', TopicService);

// lesson service
const LessonService = require('./api/LessonService');
app.use('/', LessonService);

// course
const CourseService = require('./api/course-online/CourseService');
app.use('/', CourseService);

// video
const VideoService = require('./api/course-online/VideoService');
app.use('/', VideoService);

// detail learn video
const DetailLearnVideo = require('./api/course-online/DetailLearnVideo');
app.use('/', DetailLearnVideo);

// detail learn course
const DetailLearnCourse = require('./api/course-online/DetailLearnCourse');
app.use('/', DetailLearnCourse);

module.exports = app;