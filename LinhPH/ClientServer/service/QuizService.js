const QuizRepository = require('./../repository/QuizRepository');
const Points = require("./../utils/CalculatePoint")
var QuizService = {
    addNewCompletedQuiz: function (newCompleteQuiz, callback) {
        var {listening, reading, user_id, exam_id, used_time} = newCompleteQuiz;
        var listening_score = Points.points_listen[listening];
        var reading_score = Points.points_read[reading];
        var total_score = listening_score + reading_score;
        var objectToSave = {
            listening, reading, user_id, exam_id, used_time,listening_score,reading_score,total_score
        }
        QuizRepository.addNewCompletedQuiz(objectToSave, function(err, result){
            callback(err, result)
        })
    }
}

module.exports = QuizService;