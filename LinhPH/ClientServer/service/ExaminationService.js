const ExaminationRepository = require('./../repository/ExaminationRepository');
var ExaminationService = {

    getQuizById  : function(id, callback) {
        ExaminationRepository.getQuizById(id, (err, result) => {
            callback(err, result);
        })
    },

    getQuizByPage : function (limit, page, callback) {
        ExaminationRepository.getQuizByPage(limit, page, (err, result) => {
            callback(err, result);
        })
    },

    getCompletedQuiz : function(user_id, callback){
        ExaminationRepository.getCompletedQuiz(user_id, function(err, result){
            callback(err, result);
        })
    },

    countAllQuiz : function(callback){
        ExaminationRepository.getCountOfExam(function(result){
            callback(result);                                                                               
        })
    },
    
    createNewExam : function(exam, callback){
        ExaminationRepository.createNewExamMysql(exam, function(err, result){
            var id  = result.insertId;
            var newExam = {...exam};
            newExam.fID = id;
            ExaminationRepository.createNewExamMongo(newExam, function(err2, result2){
                callback(err2, result2);
            })
        })
    }
}

module.exports = ExaminationService;