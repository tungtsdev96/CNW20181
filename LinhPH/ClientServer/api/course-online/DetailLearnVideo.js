const express = require('express');
const router = express.Router();
const detailLearnVideo = require('../../repository/course-online/DetailLearnVideo');

router.get("/api/course/get-detail-learn-video", function (req, res) {
    var {idCourse, idUser} = req.query;
    detailLearnVideo.getListVideoOfCourseLearning(idCourse, idUser, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

module.exports = router;