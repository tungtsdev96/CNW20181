const express = require('express');
const router = express.Router();
const courseDao = require('../../repository/course-online/CourseDao');

router.get("/api/course/get-popular-course", function (req, res) {
    courseDao.getPopularCourse(function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

router.get("/api/course/get-by-id", function (req, res) {
    var idCourse = req.query.idCourse;
    courseDao.getCourseById(idCourse, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

router.get("/api/course/get-all", function (req, res) {
    courseDao.getListCourse(function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

router.get("/api/course/get-by-type", function (req, res) {
    var {type} = req.query;
    courseDao.getListCourseByType(type, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

// update course
router.post("/api/course/update-course", function(req, res) {
    var course = req.body.course;
    courseDao.updateCourse(course, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
})

//delete course
router.post("/api/course/delete-course", function(req, res) {
    var idCourse = req.query.idCourse;
    courseDao.deletedCourse(idCourse, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
})

// create course
router.post("/api/course/create-course", function (req, res) {
    var course = req.body;
    courseDao.createCourse(course, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

// upload img for course
router.post('/course/img/upload', (req, res, next) => {

    let fileToUpload = req.files.fileToUpload;
    var parts = fileToUpload.name.split(".");
    let nameOfFileUpload = req.body.filename + "." + parts[parts.length - 1];
    
    saveFile("images", "", fileToUpload, nameOfFileUpload, function(url){
        if (url == null) {
            res.status(500).send("Err");
        } else {
            res.json({file: url});
        }
    })
})

// upload video for course
router.post('/course/video/upload', (req, res) => {
    let video = req.files.fileToUpload;
    // console.log(req.files);
    var parts = video.name.split(".");
    let nameOfFileUpload = req.body.filename + "." + parts[parts.length - 1];
    
    saveFile("videos", "", video, nameOfFileUpload, function(url){
        if (url == null) {
            res.status(500).send("Err");
        } else {
            res.json({file: url});
        }
    })
})

function saveFile(typeFolder, parentFolder, fileUpload, nameOfFileUpload, callback) {
    var folder = `./public/${typeFolder}/courses/`;
    folder += parentFolder;
    folder += nameOfFileUpload;
    console.log(folder);
    fileUpload.mv(folder, function (err) {
        if (err) {
            callback(null);
            console.log("Err");
        } else {
            console.log("upload ok");
            callback(folder);
        }
    });
}

module.exports = router;