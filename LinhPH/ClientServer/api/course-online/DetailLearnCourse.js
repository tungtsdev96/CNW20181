const express = require('express');
const router = express.Router();
const detailLearnCourse = require('../../repository/course-online/DetailLearnCourse');

router.get("/api/course/get-detail-learn-course", function (req, res) {
    var {idCourse, idUser} = req.query;
    detailLearnCourse.getDetailLearnCourse(idCourse, idUser, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

module.exports = router;