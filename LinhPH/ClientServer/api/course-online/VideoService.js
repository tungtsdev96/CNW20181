const express = require('express');
const router = express.Router();
const videoDao = require('../../repository/course-online/VideoDao');

router.get("/api/video/get-by-course", function(req, res) {
    var idCourse = req.query.idCourse;
    videoDao.getListVideoByIdCourse(idCourse, (result) => {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    })
})

router.post("/api/video/create-infor-video", function (req, res) {
    var video = req.body;
    videoDao.createVideo(video, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

module.exports = router;