var express = require('express');
var router = express.Router();
var ExaminationService = require('./../service/ExaminationService');
var ExaminationRepository = require("./../repository/ExaminationRepository")
var multer = require("multer");

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/upload/')
    },
    filename: function (req, file, cb) {
        cb(null, new Date().getTime() + "");
    }
});

var upload = multer({ storage: storage })
/* GET home page. */

router.get('/test/:id', function (req, res, next) {
    var id = req.params.id;
    ExaminationService.getQuizById(id, (err, result) => {
        if (err) {
            res.status(400);
            res.json({
                data: null
            });
        } else {
            res.status(200);
            res.json({
                data: result
            })
        }
    })
});

// router.get('/tests', function(req, res, next) {
//     var page = req.query.page;
//     ExaminationService.getQuizByPage(2, page,(err, result) => {
//         if(err){
//             res.status(400);
//             res.json({
//                 data : null
//             });
//         }else{
//             res.status(200);
//             res.json({
//                 data : result
//             })
//         }
//     })
// });



router.get('/tests', function (req, res, next) {
    var page = req.query.page;
    var limit = req.query.limit;
    console.log(page, limit)
    ExaminationService.getQuizByPage(limit, page, (err, result) => {
        if (err) {
            res.status(400);
            res.json({
                data: null
            });
        } else {
            res.status(200);
            res.json({
                data: result
            })
        }
    })
});

router.get('/completed_quiz', function (req, res, next) {
    var id = req.query.id;
    ExaminationService.getCompletedQuiz(id, function (err, result) {
        if (err) {
            res.status(400);
            res.json({
                data: null
            });
        } else {
            res.status(200);
            res.json({
                data: result
            })
        }
    });
});

router.get('/tests/count-all', function (req, res, next) {
    var state = req.query.state;
    ExaminationRepository.getCountOfExam(state, function(err, result){
        if(err) throw err;
        res.json(result)
    })
});

router.post('/tests/update-state', function (req, res, next) {
    var data = req.body;
    console.log(data)
    ExaminationRepository.updateExamState(data.exam_id, data.newState, function(err, result){
        if(err) throw err;
        res.json(result)
    })
});

router.post('/tests', function (req, res, next) {
    var lstFiles = req.files;

    var questions = JSON.parse(req.body.data);
    // Save File Audio
    var audioFile = lstFiles.audio;
    if (audioFile) {
        audioFileName = (new Date()).getTime() + audioFile.name;
        saveFile("audio", audioFileName, audioFile);
    }

    var lstPicNames = [];
    var pic1 = lstFiles.file1;
    var pic1Name = (new Date()).getTime() + pic1.name;
    saveFile("image", pic1Name, pic1);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic1Name);

    var pic2 = lstFiles.file2;
    var pic2Name = (new Date()).getTime() + pic2.name;
    saveFile("image", pic2Name, pic2);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic2Name);

    var pic3 = lstFiles.file3;
    var pic3Name = (new Date()).getTime() + pic3.name;
    saveFile("image", pic3Name, pic3);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic3Name);

    var pic4 = lstFiles.file4;
    var pic4Name = (new Date()).getTime() + pic4.name;
    saveFile("image", pic4Name, pic4);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic4Name);

    var pic5 = lstFiles.file5;
    var pic5Name = (new Date()).getTime() + pic5.name;
    saveFile("image", pic5Name, pic5);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic5Name);

    var pic6 = lstFiles.file6;
    var pic6Name = (new Date()).getTime() + pic6.name;
    saveFile("image", pic6Name, pic6);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic6Name);

    var pic7 = lstFiles.file7;
    var pic7Name = (new Date()).getTime() + pic7.name;
    saveFile("image", pic7Name, pic7);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic7Name);

    var pic8 = lstFiles.file8;
    var pic8Name = (new Date()).getTime() + pic8.name;
    saveFile("image", pic8Name, pic8);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic8Name);

    var pic9 = lstFiles.file9;
    var pic9Name = (new Date()).getTime() + pic9.name;
    saveFile("image", pic9Name, pic9);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic9Name);

    var pic10 = lstFiles.file10;
    var pic10Name = (new Date()).getTime() + pic10.name;
    saveFile("image", pic10Name, pic10);
    lstPicNames.push("http://54.95.40.41:3000/test-source/image/" + pic10Name);

    console.log(lstPicNames);

    // Set image for part 1
    var lstQuestionsPart1 = [...questions.lstQuestionsPart1];
    for(var i =0; i < lstQuestionsPart1.length; i++){
        lstQuestionsPart1[i].image = lstPicNames[i];
    }

    var exam = {
        name : '',
        description : '',
        content : {
            listening : {
                audio : '',
                question : []
            },
            reading : {
                part5 : [],
                part6 : [],
                part7: []
            }
        }
    }
    exam.name = questions.name;
    exam.description = questions.description;
    exam.content.listening.audio = "http://54.95.40.41:3000/test-source/audio/" + audioFileName;
    exam.content.listening.question = exam.content.listening.question.concat(lstQuestionsPart1);
    exam.content.listening.question = exam.content.listening.question.concat(questions.lstQuestionsPart2);
    exam.content.listening.question = exam.content.listening.question.concat(questions.lstQuestionsPart3);
    exam.content.listening.question = exam.content.listening.question.concat(questions.lstQuestionsPart4);
    exam.content.reading.part5 = questions.lstQuestionsPart5;
    exam.content.reading.part6 = questions.lstGroupPart6;
    exam.content.reading.part7 = questions.lstGroupPart7;
    // console.log(exam)

    ExaminationService.createNewExam(exam, function(err, result){
        if(err){
            res.json({
                data : null
            });
        }else{
            res.json({
                data : result
            })
        }
    })
});

function saveFile(type, name, file) {
    var folder = "./public/test-source/";
    folder = folder + type + "/"
    fileName = (new Date()).getTime() + file.name;
    file.mv(folder + name, function (err) {
        if (err) {
            console.log("Err");
        } else {
            console.log("OK, Success !")
        }
    });
}

module.exports = router;