var express = require('express');
var jwt = require('jsonwebtoken')
var router = express.Router();
var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);
var UserRepository = require("./../repository/UserRepository");
var JWTVerify = require("././../utils/JWTVerify");
var nodemailer = require('nodemailer');
/* GET home page. */


router.post('/signup', function (req, res, next) {
    var user = req.body;

    console.log(user);
    var hash = bcrypt.hashSync(user.password, salt);
    var newUser = { ...user };
    newUser.password = hash;
    UserRepository.addNewUser(newUser, function (err, result) {
        if (err) {
            res.sendStatus(400);
        } else {
            res.sendStatus(200);
            var url = "http://54.95.40.41:3000/confirm-signup?email="+user.email;
            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'linhphamdev96@gmail.com',
                    pass: 'ngan28091996'
                }
            });

            var mailOptions = {
                from: 'linhphamdev96@gmail.com',
                to: user.email,
                subject: '[Toeic Nhóm 4] Email confirm Signup',
                text: 'Thank for --->>>>> Please confirm your sign up by clicking this URL :   ' + url
            };

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });

        }
    })
});

router.get('/check-username', function (req, res, next) {
    var username = req.query.username;
    UserRepository.checkUsernameExisted(username, function (err, result) {
        if (result.length > 0) {
            res.json({
                existed: 1
            })
        } else {
            res.json({
                existed: 0
            })
        }
    })
});

router.get('/confirm-signup', function (req, res, next) {
    var email = req.query.email;
    UserRepository.updateStateUser(email, 1, function (err, result) {
        var data = {};
        if (err) {
            data = { result: "Xác thực thất bại, vui lòng thử lại" }
        } else {
            data = { result: "Xác thực thành công!" }
        }
        res.render("Confirm.ejs", data);
    })
});

router.get('/private', JWTVerify, function (req, res, next) {
    jwt.verify(req.token, "toeic-key", (err, authData) => {
        if (err) {
            console.log(err)
            console.log(authData)
            res.sendStatus(403);
        } else {
            res.json({
                name: "Linh",
                age: 21
            })
        }

    })
})


router.post('/login', function (req, res, next) {
    var user_data = req.body;
    UserRepository.getUserByUsername(user_data.username, function (err, result) {
        if (result == null || result.length == 0) {
            console.log(err, result)
            res.sendStatus(403);
        } else {
            dbUser = result[0];
            var compareResult = bcrypt.compareSync(user_data.password, dbUser.password);
            console.log(compareResult);
            if (compareResult) {
                jwt.sign({ dbUser }, "toeic-key", (err, token) => {
                    res.status(200);
                    res.json({
                        token: token,
                        user : {
                            id : dbUser.id,
                            name : dbUser.name
                        }
                    })
                })

            } else {
                res.sendStatus(403)
            }
        }
    })
});

router.get("/users", function(req, res){
    var page =req.query.page;
    UserRepository.getUserByPage(page, function(err, result){
        if(err){
            res.status=400;
            res.json({
                data : null
            })
        }else{
            res.status=200;
            res.json({
                data : result
            })
        }
    })
});

router.get("/users/count", function(req, res){
    UserRepository.getCountOfUser(function(err, result){
        console.log(result);
        res.json({
            data : result[0].count
        })
    })
});

router.post("/users/update-state", function(req, res){
    var user_id = req.body.user_id;
    var state = req.body.state;
    UserRepository.updateUserState(user_id, state, function(err, result){
        if(err){
            console.log(err);
            res.sendStatus(400);
        }else{
            res.json({
                status : "OK"
            })
        }
    })
});

router.get("/users/user-info", function(req, res){
    var user_id = req.query.user_id;
    UserRepository.getUserInfoById(user_id, function(err, result){
        if(err || result.length == 0){
            res.json({
                data : null
            })
        }else{
            res.json({
                data : result[0]
            })
        }
    });
})

module.exports = router;
