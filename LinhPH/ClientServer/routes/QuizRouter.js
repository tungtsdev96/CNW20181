var express = require('express');
var router = express.Router();
var QuizRepository = require('./../repository/QuizRepository');
var QuizService = require('./../service/QuizService');
/* GET home page. */

router.post("/quiz/add-completed-quiz", function (req, res) {
    var addNewCompletedQuiz = req.body;
    console.log(addNewCompletedQuiz);
    console.log("run router");
    QuizService.addNewCompletedQuiz(addNewCompletedQuiz, function(err, result){
        res.json(result);
    })
    
});

router.get("/quiz/completed-quiz-by-user", function(req, res){
    var user_id = req.query.user_id;
    var page = req.query.page;
    QuizRepository.getCompletedQuizByUser(user_id,page, function(err, result){
        res.json(result)
    })
});

router.get("/quiz/count-completed-quiz-by-user", function(req, res){
    var user_id = req.query.user_id;
    QuizRepository.countCompletedQuizOfUser(user_id, function(err, result){
        res.json({
            count : result.length
        })
    })
})


module.exports = router;