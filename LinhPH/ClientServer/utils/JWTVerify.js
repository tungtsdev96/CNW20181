module.exports = function(req, res, next){
    const bearerHeader = req.headers['authorization'];
    console.log(bearerHeader)
    if(typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(" ");
        const token = bearer[1];
        console.log(token)
        req.token = token;
        next();
    }else{
        res.sendStatus(403)
    }
}