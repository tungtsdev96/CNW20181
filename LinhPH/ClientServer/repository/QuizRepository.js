const DAO = require('./../configuration/Database');
var pool = require("./../configuration/MySQLConfig");
var ObjectID = require('mongodb').ObjectID;
var QuizRepository = {

	getCompletedQuizByUser: function (user_id, page, callback) {
		var limit = 10;
		var offset = (page - 1) * limit;
		var sql = "SELECT user.id, user.username, user.name, examination.name, completed_quiz.listening_score, completed_quiz.reading_score, completed_quiz.total_score, completed_quiz.used_time, completed_quiz.time from toeic.user, examination, completed_quiz "
		 + " WHERE user.id = completed_quiz.user_id AND completed_quiz.exam_id = examination.id AND user.id = " + user_id +
		 " limit " + limit + " offset " + offset;
		 console.log(sql);
		pool.query(sql, function(err, result){
			console.log(err);
			callback(err, result);
		})
	},

	addNewCompletedQuiz: function (completedQuiz, callback) {
		var sql = "INSERT INTO completed_quiz(user_id, exam_id, listening, reading, listening_score,"
		+ " reading_score, total_score, used_time) VALUES(" + completedQuiz.user_id + "," + completedQuiz.exam_id
		+ ", " + completedQuiz.listening + ", " + completedQuiz.reading + ", " + completedQuiz.listening_score
		+ ", " + completedQuiz.reading_score + "," + completedQuiz.total_score + ", '" + completedQuiz.used_time
		+ "')";
		console.log(sql);
		pool.query(sql, function(err, result){
			console.log(err);
			callback(err, result);
		})
	},

	updateCompletedQuiz : function (user_id, listCompleted, callback){
		
	},

	countCompletedQuizOfUser(user_id, callback){
		var sql = "SELECT * from completed_quiz WHERE user_id=" + user_id;
		console.log(sql);
		pool.query(sql, function(err, result){
			console.log(err);
			callback(err, result);
		})
	}


}
module.exports = QuizRepository;