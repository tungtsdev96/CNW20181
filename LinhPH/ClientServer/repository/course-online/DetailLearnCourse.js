var pool = require('../../config/db');
var detailLearnVideoDao = require('./DetailLearnVideo');

var detailLearnCourse = {

    getDetailLearnCourse(idCourse, idUser, callback) {
        var sql = 'call toeic.proc_detail_learn_course(' + idUser + ',' + idCourse + ')';

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    var course = result[0][0];
                    console.log(result);
                    detailLearnVideoDao.getListVideoOfCourseLearning(idCourse, idUser, (videos) => {
                        if (videos == null) {
                            callback(null);
                        } else {
                            console.log("ADsd", videos)
                            callback({
                                course: course,
                                videos: videos
                            })
                        }
                    })
                    
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

}

module.exports = detailLearnCourse;