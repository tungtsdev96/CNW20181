var pool = require('../../config/db');

var videoDao = {
    
    getListVideoByIdCourse(idCourse, callback){
        var sql = 'SELECT * FROM toeic.video WHERE courseId=' + idCourse;

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    createVideo: function (video, callback){
        var sql = "INSERT INTO `toeic`.`video` (`courseId`, `titleVideo`, `lengthVideo`, `urlVideo`)" +
                    " VALUES ('" + video.courseId +"', '" +  video.titleVideo + "', '"+ video.lengthVideo +"','"+ video.urlVideo +"')";

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

}

module.exports = videoDao;