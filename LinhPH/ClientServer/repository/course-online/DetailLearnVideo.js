var pool = require('../../config/db');

var detailLearnVideo = {

    getListVideoOfCourseLearning(idCourse, idUser, callback) {
        var sql = 'call toeic.proc_detail_learn_video(' + idUser + ', '+ idCourse + ')';

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result[0]);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

}

module.exports = detailLearnVideo;