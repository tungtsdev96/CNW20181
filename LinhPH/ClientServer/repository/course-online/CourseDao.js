var pool = require('../../config/db');

var courseDao = {

    getPopularCourse(callback) {
        var sql = "call toeic.proc_get_list_popular_course()";
        
        console.log(sql);

        try {
            pool.query(sql, function (err, result) {
                if (result != null) {
                    callback(result[0]);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    // type : 0 free
    // type : 1 not free
    // all
    getListCourseByType(type, callback) {
        var sql = "call toeic.proc_get_list_course(" + type +")";
        
        console.log(sql);

        try {
            pool.query(sql, function (err, result) {
                if (result != null) {
                    callback(result[0]);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }

    },

    getListCourse(callback) {
        var sql = "SELECT * FROM Course WHERE Course.deleted != 1";

        console.log(sql);

        try {
            pool.query(sql, function (err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getCourseById(id, callback) {
        var sql = "SELECT * FROM Course WHERE Course.id=" + id;

        console.log(sql);

        try {
            pool.query(sql, function (err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    updateCourse(course, callback) {
        var sql = "UPDATE toeic.Course SET nameCourse='" + course.nameCourse + "'" +
            ", nameTeacher='" + course.nameTeacher + "'" +
            ", description='" + course.description + "'" +
            ", price='" + course.price + "'" + " WHERE id=" + course.id;

        console.log(sql);

        try {
            pool.query(sql, function (err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    deletedCourse(idCourse, callback) {
        var sql = "UPDATE toeic.Course SET deleted=1 WHERE id=" + idCourse;

        console.log(sql);

        try {
            pool.query(sql, function (err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    createCourse: function (course, callback) {
        var sql = "INSERT INTO Course(Course.nameCourse, Course.nameTeacher, Course.totalVideo, Course.description, Course.price)" +
            " VALUES('" + course.nameCourse + "','" + course.nameTeacher + "','" + course.totalVideo + "','" + course.description + "','" + course.price + "')";

        console.log(sql);

        try {
            pool.query(sql, function (err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

}

module.exports = courseDao;