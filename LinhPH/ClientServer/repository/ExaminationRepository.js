const DAO = require('./../configuration/Database');
var pool = require("./../configuration/MySQLConfig")
var ObjectID = require('mongodb').ObjectID;
var ExaminationRepository = {

    getQuizById: function (id, callback) {
        var query = {
            "fID": parseInt(id)
        };
        console.log(query)
        DAO.get('Examinations', query, {}, 1, 1, (err, result) => {
            console.log(result)
            if (result != null && result.length > 0) {
                callback(err, result[0]);
            } else {
                callback(err, null);
            }

        });
    },


    updateExamState : function(exam_id, newState, callback){
        var sql = "UPDATE examination SET state = " + newState +" WHERE id = " + exam_id;
        console.log(sql)
        pool.query(sql, function (err, result) {
            callback(err, result);
        });
    },

    getQuizByPage: function (limit, page, callback) {
        var offset = (page - 1) * limit;
        var sql = "select * from examination order by id ASC limit " + limit + " offset " + offset;
        console.log(sql)
        pool.query(sql, function (err, result) {
            callback(err, result);
        });
    },

    getCompletedQuiz: function (user_id, callback) {
        var query = {
            user_id: parseInt(user_id)
        };
        DAO.get('QuizComplete', query, {}, 100, 1, (err, result) => {
            console.log(query, result)
            callback(err, result);
        });
    },

    getCountOfExam: function (state, callback) {
        var sql = "";
        if (state == 0) {
            sql = "SELECT COUNT(*) as count FROM examination";
        } else {
            sql = "SELECT COUNT(*) as count FROM examination WHERE examination.state = 1";
        }
        pool.query(sql, function (err, result) {
            callback(err, result);
        });
    },

    createNewExamMysql: function (newExam, callback) {
        var sql = "INSERT INTO examination(name, description) values('" + newExam.name + "','" + newExam.description + "');"
        console.log(sql)
        pool.query(sql, function (err, result) {
            callback(err, result);
        });
    },

    createNewExamMongo: function (newExam, callback) {
        DAO.create("Examinations", newExam, function (err, result) {
            callback(err, result);
        })
    }

}

module.exports = ExaminationRepository;