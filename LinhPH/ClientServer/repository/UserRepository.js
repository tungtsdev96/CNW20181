var pool = require("./../configuration/MySQLConfig");

var UserRepository = {

	getUserByUsername : function(username, callback){
        var sql = "SELECT * FROM toeic.user WHERE username ='" + username +"' AND state = 1;";
        console.log(sql);
        pool.query(sql, function(err, result){
            if(err){
                callback(err, null);
            }else{
                callback(err, result);
            }
        })
    },

    checkUsernameExisted :  function(username, callback) {
        var sql = "SELECT * FROM toeic.user WHERE username ='" + username +"'";
        console.log(sql);
        pool.query(sql, function(err, result){
            if(err){
                callback(err, null);
            }else{
                callback(err, result);
            }
        })
    },

    updateStateUser : function(email, state, callback){
        var sql = "UPDATE toeic.user SET state = " + state +" WHERE state = 0 AND email='" + email + "';";
        console.log(sql);
        pool.query(sql, function(err, result){
            if(err){
                callback(err, null);
            }else{
                callback(err, result);
            }
        })
    },

    addNewUser : function (user, callback) {
        var sql = "INSERT INTO toeic.user(username, password, name, address, year_of_birth, email, phone_number) VALUES('"
         + user.username + "', '" + user.password + "','" +  user.name + "','" +  user.address + "'," + user.year_of_birth + ", '" + 
            user.email + "','" + user.phone_number + "');";
        console.log(sql);
        pool.query(sql, function(err, result){
            if(err){
                console.log(err);
                callback(err, null)
            }else{
                callback(err, result);
            }
        });
    },

    getUserByPage : function(page, callback){
        var offset = (page - 1) * 15;
        var sql = "SELECT * FROM toeic.user ORDER BY id DESC LIMIT 10 OFFSET " + offset;
        pool.query(sql, function(err, result){
            callback(err, result)
        })
    },

    getCountOfUser: function ( callback) {
        var sql = "SELECT COUNT(*) as count FROM toeic.user";
        pool.query(sql, function (err, result) {
            callback(err, result);
        });
    },

    updateUserState : function(user_id, state, callback){
        var sql = "UPDATE toeic.user SET state = " + state  +" WHERE id = " + user_id;
        console.log(sql)
        pool.query(sql, function (err, result) {
            callback(err, result);
        });
    },

    getUserInfoById : function(user_id, callback){
        var sql = "SELECT * FROM toeic.user WHERE id = " + user_id;
        console.log(sql)
        pool.query(sql, function (err, result) {
            callback(err, result);
        });
    }
}
module.exports = UserRepository;