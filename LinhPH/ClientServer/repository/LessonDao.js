var pool = require('../config/db');

var lessonDao = {
    
    getPopularLesson(callback) {
        var sql = "call toeic.proc_popular_lesson()";
        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getListLessionByIdTopic(idTopic, callback){
        var sql = "SELECT * FROM toeic.lesson WHERE lesson.topicId =  " + idTopic;

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getRecommendLesson: function(callback) {
        var sql = 'SELECT * FROM toeic.lesson LIMIT 4;';
        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getAllLession: function(callback) {
        var sql = 'SELECT * FROM toeic.lesson';
        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    deleteLesson (id, callback) {
        var sql = "DELETE FROM vocabulary WHERE lessonId=" + id;
        console.log(sql);
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    var sql = "DELETE FROM lesson WHERE id=" + id;
                    try {
                        pool.query(sql, function(err, result) {
                            if (result != null) {
                                callback(result);
                            } else {
                                callback(null);
                            }
                        })
                    } catch (error) {
                        callback(null);
                    }
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    updateLeson (lesson, callback) {
        var sql = "UPDATE `toeic`.`lesson` SET " +
            " lessonNameEn='" +  lesson.lessonNameEn + "', " +
            " lessonNameVi='" +  lesson.lessonNameVi + "', " +
            " topicId='" + lesson.topicId + "' WHERE id=" + lesson.id; 

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getLessionByIdLesson(idLesson, callback){
        var sql = "SELECT * FROM toeic.lesson WHERE lesson.id = " + idLesson;

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    createLesson: function (lesson, callback){
        var sql = 'INSERT INTO lesson(lesson.topicId, lesson.lessonNameEn, lesson.lessonNameVi, lesson.totalVocab, lesson.imageUrl)'
            + ' VALUES("' 
            + lesson.topicId + '","' 
            + lesson.lessonNameEn + '","' 
            + lesson.lessonNameVi + '","' 
            + lesson.totalVocab + '","' 
            + lesson.imageUrl + '")';

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

}

module.exports = lessonDao;