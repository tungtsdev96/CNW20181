

getRandomArbitrary = (min, max) => {
    var index = Math.floor(Math.random() * (max - min + 1)) + min;
    //console.log(min, max, index);
    return index;
}

shuffleArray = (a) => {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

module.exports = {getRandomArbitrary, shuffleArray};
