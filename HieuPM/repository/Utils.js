

getRandomArbitrary = (min, max) => {
    var index = Math.floor(Math.random() * (max - min + 1)) + min;
    //console.log(min, max, index);
    return index;
}

module.exports = {getRandomArbitrary};
