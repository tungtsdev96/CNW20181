const pool = require('../../config/db');
const vocabDao = require('../VocabularyDao');

const ListenQuestion = require("../../modal/ListenQuestion");
const MultiChooseQuestion = require("../../modal/MultiChooseQuestion");
const FillQuestion = require("../../modal/FillQuestion");
const Utils = require('../Utils');

questionToLearnDao = {

    makeListMutilChooseQuestion : (idLesson, callback) => {
        var listQuestion = new Array();
        vocabDao.getListVocabularyByIdLesson(idLesson, (vocabs) => {
            if (vocabs == null) {
                callback(null);
            } else {

                // make list MultiChooseQuestion
                vocabs.map( function(vocab, index) {
                    var coppyOfVocab = [];
                    coppyOfVocab = coppyOfVocab.concat(vocabs); 
                    coppyOfVocab.splice(index, 1);

                    var indexAnswerA = Utils.getRandomArbitrary(0, coppyOfVocab.length - 1);
                    var answerA = coppyOfVocab[indexAnswerA].vocabularyEn; 
                    coppyOfVocab.splice(indexAnswerA, 1);

                    var indexAnswerB = Utils.getRandomArbitrary(0, coppyOfVocab.length - 1);
                    var answerB = coppyOfVocab[indexAnswerB].vocabularyEn; 
                    coppyOfVocab.splice(indexAnswerB, 1);

                    var indexAnswerC = Utils.getRandomArbitrary(0, coppyOfVocab.length - 1);
                    var answerC = coppyOfVocab[indexAnswerC].vocabularyEn; 
                    coppyOfVocab.splice(indexAnswerC, 1);

                    var indexAnswerD = Utils.getRandomArbitrary(0, coppyOfVocab.length - 1);
                    var answerD = coppyOfVocab[indexAnswerD].vocabularyEn; 
                    coppyOfVocab.splice(indexAnswerD, 1);

                    var question = new MultiChooseQuestion(vocab, answerA, answerB, answerC, answerD);
                    listQuestion.push(question);
                });
                
                callback(listQuestion);
            }
        });

    },

    makeListListenQuestion : (idLesson, callback) => {

        var listQuestion = new Array();
        vocabDao.getListVocabularyByIdLesson(idLesson, (vocabs) => {
            if (vocabs == null) {
                callback(null);
            } else {
                vocabs.map( function(vocab, index) {
                    var question = new ListenQuestion(vocab);
                    listQuestion.push(question);
                });
                
                callback(listQuestion);
            }
        });

    },

    makeListFillQuestion : (idLesson, callback) => {

        var listQuestion = new Array();
        vocabDao.getListVocabularyByIdLesson(idLesson, (vocabs) => {
            if (vocabs == null) {
                callback(null);
            } else {
                vocabs.map( function(vocab, index) {
                    var question = new FillQuestion(vocab);
                    listQuestion.push(question);
                });
                
                callback(listQuestion);
            }
        });

    }

}

module.exports = questionToLearnDao;