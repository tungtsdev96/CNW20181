var pool = require('../config/db');

var lessonDao = {
    
    getListLessionByIdTopic(idTopic, callback){
        var sql = "SELECT * FROM toeic.lesson WHERE lesson.topicId =  " + idTopic;

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getRecommendLesson: function(callback) {
        var sql = 'SELECT * FROM toeic.lesson LIMIT 4;';
        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getAllLession: function(callback) {
        var sql = 'SELECT * FROM toeic.lesson';
        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getLessionByIdLesson(idLesson, callback){
        var sql = "SELECT * FROM toeic.lesson WHERE lesson.id = " + idLesson;

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    createLesson: function (lesson, callback){
        var sql = 'INSERT INTO lesson(lesson.id, lesson.topicId, lesson.lessonNameEn, lesson.lessonNameVi, lesson.imageUrl)'
            + ' VALUES("' 
            + lesson.id + '","' 
            + lesson.topicId + '","' 
            + lesson.lessonNameEn + '","' 
            + lesson.lessonNameVi + '","' 
            + lesson.imageUrl + '")';

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

}

module.exports = lessonDao;