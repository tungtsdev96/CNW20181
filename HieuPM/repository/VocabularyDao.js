var pool = require('../config/db');

var vocabularyDao = {

    getListVocabularyByIdLesson: function(idLesson, callback){
        var sql = "SELECT * FROM toeic.vocabulary WHERE vocabulary.lessonId = " + idLesson;
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getDetailVocabularyById: function(idVocab, callback){
        var sql = "SELECT * FROM toeic.vocabulary WHERE vocabulary.id = " + idVocab;
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    getAllVocabulary: function(callback){
        var sql = "SELECT * FROM toeic.vocabulary";
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    createVocabulary: function (vocabulary, callback){
        var sql = 'INSERT INTO vocabulary(id, lessonId, vocabularyEn, pronunciation, vocabularyType, vocabularyVi, explantion, exampleEn, exampleVi, imageUrl, audioUrl)'
            + 'VALUES("' + vocabulary.id + '", "' 
                         + vocabulary.lessonId + '", "' 
                         + vocabulary.vocabularyEn + '", "'
                         + vocabulary.pronunciation + '", "'
                         + vocabulary.vocabularyType + '", "'
                         + vocabulary.vocabularyVi + '", "'
                         + vocabulary.explantion + '", "'
                         + vocabulary.exampleEn + '", "'
                         + vocabulary.exampleVi + '", "'
                         + vocabulary.imageUrl + '", "'
                         + vocabulary.audioUrl + '")';

        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }

    },

}

module.exports = vocabularyDao;