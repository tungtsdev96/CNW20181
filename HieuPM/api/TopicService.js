const express = require('express');
const router = express.Router();
const topicDao = require('../repository/TopicDao')

// Get List topic
router.get("/api/topic/get-all", function (req, res) {
    topicDao.getAllTopic(function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

module.exports = router;