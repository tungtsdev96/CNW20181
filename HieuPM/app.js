const express = require('express');
const app = express();
const server = require('http').Server(app);
const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: false
}))

// parse application/json
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// vocabulary service
const vocabularyService = require('./api/VocabularyService');
app.use('/', vocabularyService);

// question to learn service
const QuestionToLearnService = require('./api/QuestionToLearnService');
app.use('/', QuestionToLearnService);

// detail learn vocab service
const DetailLearnVocabService = require('./api/DetailLearnVocabService');
app.use('/', DetailLearnVocabService);

// topic service
const TopicService = require('./api/TopicService');
app.use('/', TopicService);

// lesson service
const LessonService = require('./api/LessonService');
app.use('/', LessonService);

server.listen(3000, function () {
    console.log("Server started in port 3000...");
});

