const express = require('express');
const router = express.Router();
const detailFavoriteDao = require('../repository/learn-vocab/DetailFavoriteVocabDao');

router.get("/api/vocabulary/check-favorite", function (req, res) {
    var idVocab = req.query.idVocab;
    var idLesson = req.query.idLesson;
    var idUser = req.query.idUser;
    detailFavoriteDao.checkFavoriteVocab(idUser, idVocab, idLesson, (result) => {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json({isFavorite: result});
        }
    })
})

router.post("/api/vocabulary/add-to-favorite", function (req, res) {
    var idVocab = req.body.idVocab;
    var idLesson = req.body.idLesson;
    var idUser = req.body.idUser;
    detailFavoriteDao.addFavoriteVocab(idUser, idVocab, idLesson, (result) => {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json({result: "ok"});
        }
    })
})

router.delete("/api/vocabulary/delete-vocab-favorite", function(req, res) {
    var idVocab = req.body.idVocab;
    var idUser = req.body.idUser;
    detailFavoriteDao.deleteFavoriteVocab(idUser, idVocab, (result) => {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json({result: "ok"});
        }
    })
})

module.exports = router;