const express = require('express');
const router = express.Router();
const vocabularyDao = require('../repository/VocabularyDao');

// Get List vocab by IdLesson
router.get("/api/vocabulary/get-list-vocab-by-id-lesson", function (req, res) {
    var idLesson = req.query.idLesson;
    vocabularyDao.getListVocabularyByIdLesson(idLesson, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

// Detail Vocab by id
router.get("/api/vocabulary/get-detail-vocab", function (req, res) {
    var idVocab = req.query.idVocab;
    vocabularyDao.getDetailVocabularyById(idVocab ,function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

router.get("/api/vocabulary/get-all", function (req, res) {
    vocabularyDao.getAllVocabulary(function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

// search vocab
router.get("/api/vocabulary/search", function(req, res) {
    var page = req.query.page;
    var totalRecord = req.query.totalRecord;
    var idLesson = req.query.idLesson;
    var query = req.query.query;
    // console.log(req.query);
    vocabularyDao.searchVocabulary(page, totalRecord, idLesson, query, function(result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    })

})

router.post("/api/vocabulary/create", function (req, res) {
    var vocab = req.body;
    vocabularyDao.createVocabulary(vocab, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

router.put("/api/tree/update", function (req, res) {
    var vocab = req.body;
    // // console.log(newTree);
    // treeDao.updateTree(newTree, function (result) {
    //     if (result == null) {
    //         res.status(404).json({ result: null });
    //     } else {
    //         res.status(200).json(result);
    //     }
    // });
});

router.delete("/api/tree/delete", function (req, res) {
    var vocab = req.body;
    // treeDao.deleteTree(newTree.tree_id, function (result) {
    //     if (result == null) {
    //         res.status(404).json({ result: null });
    //     } else {
    //         res.status(200).json(result);
    //     }
    // });
});

module.exports = router;