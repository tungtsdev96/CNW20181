const express = require('express');
const router = express.Router();
const detailLearnVocab = require('../repository/learn-vocab/DetailLearnVocabDao');

// handle learn vocab
router.post("/api/detail-learn-vocab/handle-score-vocab", function(req, res) {
    var idVocab = req.body.idVocab;
    var idUser = req.body.idUser;
    var idLesson = req.body.idLesson;
    var isCorrect = req.body.isCorrect;

    detailLearnVocab.handleScore(idUser, idVocab, idLesson, isCorrect, function(result) {
        if (result != null && result) {
            res.status(200).send({result: true})
        } else {
            res.status(404).json({ result: false });
        }
    })

});

// get score vocab
router.get("/api/detail-learn-vocab/get-score-vocab", function (req, res) {
    var idVocab = req.query.idVocab;
    var idUser = req.query.idUser;
    detailLearnVocab.getScoreVocab(idUser, idVocab, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).send({
                countCorrect: result[0].countCorrect,
                countIncorrect: result[0].countIncorrect
            });
        }
    });
});

// get list vocab learn with idUser (logined)
router.get("/api/detail-learn-vocab/get-vocab-learn-by-user", function(req, res) {
    var idUser = req.query.idUser;
    var idLesson = req.query.idLesson;
    var type = req.query.type;

    detailLearnVocab.getListVocabLearnByUser(idUser, idLesson, type, function(result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.json(result);
        }
    })

})


module.exports = router;