const express = require('express');
const router = express.Router();
const lessonDao = require('../repository/LessonDao')

// Get all lesson
router.get("/api/lesson/get-all", function(req, res) {
    lessonDao.getAllLession(function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    })
})

// Get List lesson by topic
router.get("/api/lesson/get-list-lesson-topic", function (req, res) {
    var {idTopic} = req.query;
    lessonDao.getListLessionByIdTopic(idTopic, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

// Get lesson by idLesson
router.get("/api/lesson/get-lesson", function (req, res) {
    var {idLesson} = req.query;
    lessonDao.getLessionByIdLesson(idLesson, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).send(result[0]);
        }
    });
});

// get recommend Lesson
router.get("/api/lesson/get-recommend-lesson", function (req, res) {
    // var {idLesson} = req.query;
    lessonDao.getRecommendLesson(function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).send(result);
        }
    });
});

module.exports = router;