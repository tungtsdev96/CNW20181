const express = require('express');
const router = express.Router();
const questionToLearnDao = require('../repository/learn-vocab/QuestionToLearnVocabDao');

// Get List Mutil Choose Question 
router.get("/api/question/make-list-mutil-choose", function (req, res) {
    var idLesson = req.query.idLesson;
    questionToLearnDao.makeListMutilChooseQuestion(idLesson, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

// Get List Fill Question 
router.get("/api/question/make-list-fill", function (req, res) {
    var idLesson = req.query.idLesson;
    questionToLearnDao.makeListListenQuestion(idLesson, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

// Get List Listen Question 
router.get("/api/question/make-list-listen", function (req, res) {
    var idLesson = req.query.idLesson;
    questionToLearnDao.makeListFillQuestion(idLesson, function (result) {
        if (result == null) {
            res.status(404).json({ result: null });
        } else {
            res.status(200).json(result);
        }
    });
});

module.exports = router;
