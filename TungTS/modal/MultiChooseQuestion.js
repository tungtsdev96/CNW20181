const Question = require("./Question");

module.exports = class MutilChooseQuestion extends Question {
    
    constructor(vocabulary, answerA, answerB, answerC, answerD){
        super(vocabulary);
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
    }

    checkAnswer(answer){
        return super.checkAnswer(answer);
    }

    getCorrectAnswer() {
        return super.getCorrectAnswer();
    }

}