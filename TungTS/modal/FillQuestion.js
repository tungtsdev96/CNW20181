const Question = require("./Question");

module.exports = class FillQuestion extends Question {

    constructor(vocabulary){
        super(vocabulary);
    }

    checkAnswer(answer){
        return super.checkAnswer(answer);
    }

    getCorrectAnswer() {
        return super.getCorrectAnswer();
    }

}