var pool = require('../../config/db');

var detailLearnVocab = {

    getListVocabLearnByUser(idUser, idLesson, type, callback) {
        var sql = '';
        if (type == 1) {
           // get vocab learned and score > 0
            sql = "call toeic.proc_user_detail_learn_vocab("+ idUser + "," + idLesson +  "," + true + ")";
        } else if (type == 2) {
           // get vocab learned and score < 0
           sql = "call toeic.proc_user_detail_learn_vocab("+ idUser + "," + idLesson +  "," + false + ")";
        } else if (type == 3) {
            // get vocab never answer yet
            sql = "call toeic.proc_list_vocab_no_answer_yet(" + idUser + "," + idLesson + ")";
        } else {
            sql = "SELECT * FROM toeic.vocabulary WHERE vocabulary.lessonId = " + idLesson;
        }
        try {
            pool.query(sql, function (err, result) {
                if (err) callback(null);
                else {
                    if (result != null) {
                        if (type == 1 || type == 2 || type == 3) callback(result[0]);
                        else callback(result);
                    } else {
                        callback(null);
                    }
                }
            })
        } catch (err) {
            callback(null);
        }
        
    },

    handleScore (idUser, idVocab, idLesson, isCorrect, callback) {

        this.checkVocabIsLearning(idUser, idVocab, (isExits) => {
            if (isExits) {
                this.updateScoreVocabIsLearning(idUser, idVocab, isCorrect, (result) => {
                    callback(result);
                });
            } else {
                this.addScoreVocabIsLearning (idUser, idVocab, idLesson, isCorrect, (result) => {
                    callback(result);
                });
            }
        })

    },

    addScoreVocabIsLearning (idUser, idVocab, idLesson, isCorrect, callback) {
        var countCorrect = isCorrect ? 1 : 0;
        var countIncorrect = countCorrect > 0 ? 0 : 1;
        var sql = "INSERT INTO `toeic`.`detaillearnvocab` (`idVocab`, `idUser`, `idLesson`, `countCorrect`, `countIncorrect`) " + 
                  "VALUES ('" + idVocab + "', '" + idUser + "', '" +  idLesson +"', '" + countCorrect +"', '" + countIncorrect + "')";
        
        try {
            pool.query(sql, function (err, result) {
                if (err) callback(false);
                else {
                    if (result != null) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                }
            })
        } catch (err) {
            callback(false);
        }
    },

    updateScoreVocabIsLearning (idUser, idVocab, isCorrect, callback) {
        var sqlUpdate = isCorrect ? "`countCorrect`= detaillearnvocab.countCorrect + 1" : "`countIncorrect`= detaillearnvocab.countIncorrect + 1";

        var sql = "UPDATE `toeic`.`detaillearnvocab` SET " + sqlUpdate + " WHERE `idVocab`='"+ idVocab +"' AND `idUser`='"+ idUser +"'";

        try {
            pool.query(sql, function (err, result) {
                if (result != null) {
                    callback(true);
                } else {
                    callback(false);
                }
            })
        } catch (err) {
            callback(false);
        }
    },

    checkVocabIsLearning (idUser, idVocab, callback) {
        var sql = "SELECT count(*) AS 'count' " + 
                  " FROM  detaillearnvocab " + 
                  " WHERE detaillearnvocab.idVocab = " + idVocab + 
                  " AND detaillearnvocab.idUser = " + idUser;
        
        try {
            pool.query(sql, function (err, result) {
                if (result != null && result[0].count > 0) {
                    callback(true);
                } else {
                    callback(false);
                }
            })
        } catch (error) {
            callback(false);
        }
    },

    getScoreVocab(idUser, idVocab, callback) {
        var sql = "SELECT detaillearnvocab.countCorrect ,detaillearnvocab.countIncorrect " +
                "FROM detaillearnvocab " +
                "WHERE detaillearnvocab.idUser = " + idUser + " " +
                "AND detaillearnvocab.idVocab = " + idVocab;

        //console.log(sql);

        try {
            pool.query(sql, function (err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            //console.log(error);
            callback(null);
        }
    }

}

module.exports = detailLearnVocab;
