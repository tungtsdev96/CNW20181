var pool = require('../../config/db');

var detailFavoriteVocab = {    
    
    checkFavoriteVocab : (idUser, idVocab, idLesson, callback) => {
        var sql = "SELECT count(*) AS `count` FROM detailfavoritevocabulary where idUser = "
                + idUser + " AND idVocab = " + idVocab +   " AND idLesson = "+ idLesson;
        try {
            pool.query(sql, function (err, result) {
                if (err) callback(false);
                else {
                    if (result != null) {
                        //console.log(result);
                        if (result[0].count > 0) callback(true);
                        else callback(false);
                    } else {
                        callback(null);
                    }
                }
            })
        } catch (err) {
            callback(false);
        }
    },

    addFavoriteVocab: (idUser, idVocab, idLesson, callback) => {
        var sql ="INSERT INTO `toeic`.`detailfavoritevocabulary` (`idUser`, `idVocab`, `idLesson`)" 
                 + " VALUES (' " + idUser + "', '"+ idVocab +"', '"+ idLesson +"');"

        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    },

    deleteFavoriteVocab: (idUser, idVocab, callback) => {
        var sql = "DELETE FROM `toeic`.`detailfavoritevocabulary` WHERE `idUser`='"+ idUser +"' and`idVocab`='"+ idVocab +"';";
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (error) {
            callback(null);
        }
    }

}

module.exports = detailFavoriteVocab;