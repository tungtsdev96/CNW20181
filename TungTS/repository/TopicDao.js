var pool = require('../config/db');

var topicDao = {

    getAllTopic: function (callback){
        var sql = "SELECT * FROM topic";
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (err) {
            callback(null);
        }
    },

    createTopic: function (topic, callback){
        var sql = "INSERT INTO topic(topic.id, topic.topicEn, topic.topicVi, topic.imageUrl)"
            + " VALUES('" + topic.id + "','" + topic.topicEn + "','" + topic.topicVi + "','" + topic.imageUrl + "')";
        console.log(sql);
        
        try {
            pool.query(sql, function(err, result) {
                if (result != null) {
                    callback(result);
                } else {
                    callback(null);
                }
            })
        } catch (err) {
            callback(null);
        }

    },

    updateTopic: function (topic, callback){

    }

}

module.exports = topicDao;