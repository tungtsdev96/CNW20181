import React from 'react';
import callApi from './../../utils/ApiCaller';
import {Link} from 'react-router-dom'
class UserManager extends React.Component {

	constructor(props) {
	    super(props);
	    this.state = {
	        page: 1,
	        numberOfPage: 1,
	        lstUsers: []
	    }
    }
    
    componentWillMount(){
        this.fetchUser(1);
        var url = "https://jawz2hjps7.execute-api.ap-northeast-1.amazonaws.com/prd/admin/users/count";
        callApi('GET', url, {}).then(res => {
            var count = res.data.data;
            console.log(count)
			var numberOfPage = 0;
			if(count % 10 == 0){
				numberOfPage = Math.floor(count / 10);
			}else{
				numberOfPage = Math.floor(count / 10) + 1;
			}
			console.log(numberOfPage);
			this.setState({
				numberOfPage : numberOfPage
			})
		});
        
	}
	
	updateUserStatus = (user_id, currentState) => {
		var state = 1;
		if(currentState == 1 || currentState == "1"){
			state = 0;
		}
		var url = "http://54.95.40.41:3000/users/update-state";
		callApi('POST', url, {
			user_id : user_id,
			state : state
		}).then(res => {
			console.log(res)
			this.fetchUser(this.state.page)
	    });
	}


	fetchUser = (page) => {
	    var url = "https://jawz2hjps7.execute-api.ap-northeast-1.amazonaws.com/prd/admin/users?page=" + page;
	    callApi('GET', url, {}).then(res => {
            console.log(res.data)
            this.setState({
                lstUsers : res.data.data
            })
	    })
    }
    
    showPagitionItem = (arr) =>{
		if(arr == null || arr.length == 0) return null;
		var result = "";
		result = arr.map((element, index) => {
			return (
				<li onClick= {() => this.onChangePage(element)} key={index} onClick={() => this.onChangePage(element)} className={element == this.state.page ? "page-item active" : "page-item"}><a  className={element == this.state.page ? "page-link active" : "page-link"}>{element}</a></li>
			)
		});
		return result;
	}

	onChangePage = (page) => {
		this.setState({
			page : page
        })
        this.fetchUser(page);
	}


	showPagition = (numberOfPage) => {
		var arr = [];
		for(var i = 1; i<= numberOfPage; i++){
			
			arr.push(i);
		}
		
		return (
			<ul className="pagination">
				{this.showPagitionItem(arr)}
			</ul>
		)
		
    }
    
    showUserTable = (lstUsers) => {
        var result = null;
        console.log(lstUsers)
        if(lstUsers == null || lstUsers.length == 0) return null;
		result = lstUsers.map( (user, index ) => {
			var linkU = "/CompledQuizByUser/" + user.id;
			return (
				<tr key={index}>
					<td>
						{user.id}
					</td>
					<td>
						{user.username}
					</td>
					<td>
						{user.address}
					</td>
                    <td>
						{user.year_of_birth}
					</td>
                    <td>
						{user.phone_number}
					</td>
                    <td>
						{user.email}
					</td>
					<td>
						<Link className="btn btn-success" to={linkU}>Các bài kiểm tra đã làm</Link>
						<button onClick= { () => this.updateUserStatus(user.id, user.state)} className={user.state == 1 ? "btn btn-info" : "btn btn-danger"}>
							{user.state == 1 ? "Enable" : "Disabled"}
						</button>
					</td>
				</tr>
			);
		});
		return result;
    }


	render() {
	    return ( 
	        <div>		
				<table className="table">
					<thead>
						<tr>
							<th>
								ID
							</th>
							<th>
								Tài khoản
							</th>
							<th>
								Năm sinh
							</th>
                            <th>
								Địa chỉ
							</th>
                            <th>
								Số điện thoại
							</th>
                            <th>
								Email
							</th>
							<th style={{alignItems : "center", justifyContent: "center"}}>
								Action
							</th>
						</tr>
					</thead>
					<tbody>
					{this.showUserTable(this.state.lstUsers)}
					</tbody>
				</table><br />
				{this.showPagition(this.state.numberOfPage)} 
			</div>
	    );
	}
}
export default UserManager;
