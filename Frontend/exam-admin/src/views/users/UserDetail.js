import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import callApi from './../../utils/ApiCaller'

class UserDetail extends Component {

    constructor(props){
        super(props);
        this.state = {
            currentUser : {},
            isUpdateInfo : false,
            name : '',
            year_of_birth : 0,
            phone_number : '',
            email : '',
            lstCompletedQuiz: [],
            currentPage : 1,
            numberOfPage : 0
        }
    }

    fetchCompletedQuiz = (page) => {
        var currentUserLocal = JSON.parse(localStorage.getItem("currentUser"));
        var user_id = currentUserLocal.id;
        var url = "https://jawz2hjps7.execute-api.ap-northeast-1.amazonaws.com/prd/admin/quiz/completed-quiz-by-user?user_id=" + user_id+"&page="+page;
        console.log(url);
        callApi('GET', url, {}).then(res => {
            console.log(res.data);
            this.setState({
                lstCompletedQuiz : res.data
            })
        })
    }

    componentWillMount(){
        var currentUserLocal = JSON.parse(localStorage.getItem("currentUser"));
        var user_id = currentUserLocal.id;
        var url = "http://54.95.40.41:3000/users/user-info?user_id=" + user_id;
        console.log(url);
        callApi('GET', url, {}).then(res => {
            console.log(res.data.data);
            var {name, year_of_birth, phone_number, email} = res.data.data;
            this.setState({
                currentUser :  res.data.data,
                name, year_of_birth, phone_number, email
            })
        });
        

        var url2 = "https://jawz2hjps7.execute-api.ap-northeast-1.amazonaws.com/prd/admin/quiz/count-completed-quiz-by-user?user_id=" + user_id;
		callApi('GET', url2, {}).then(res => {
            console.log(url2);
            console.log(res.data);
			var result = res.data;
			var count = result.count;
			var numberOfPage = 0;
			if(count % 10 == 0){
				numberOfPage = Math.floor(count / 10);
			}else{
				numberOfPage = Math.floor(count / 10) + 1;
			}
			console.log(numberOfPage);
			this.setState({
				numberOfPage : numberOfPage
			})
        });
        
        this.fetchCompletedQuiz(1);
    }

    onChangePage = (page) => {
        this.fetchCompletedQuiz(page);
        this.setState({
            currentPage : page
        })
    }

    convertDateTime = (utcDate) => {
        var localDate = new Date(utcDate);
        return localDate.toLocaleString();
    }

    showTableItem = () => {
        var {lstCompletedQuiz} = this.state;
        console.log(lstCompletedQuiz)
        var result = null;
        result = lstCompletedQuiz.map((element, index) => {
            return(
                <tr key={index}>
                    <td>{element.username}</td>
                    <td>{element.name}</td>
                    <td>{element.name}</td>
                    <td>{element.listening_score}</td>
                    <td>{element.reading_score}</td>
                    <td>{element.total_score}</td>
                    <td>{element.used_time}</td>
                    <td>{this.convertDateTime(element.time)}</td>
                </tr>
            )
        });
        return result;
    }

    render() {
        var currentUser = this.state.currentUser;
        var isUpdateInfo = this.state.isUpdateInfo;
        var {name, year_of_birth, phone_number, email} = this.state;
        if(isUpdateInfo){
            return (
                <div>
                   <div className="container">
                        <Link to="/ChangePassword" className="btn btn-info">Đổi mật khẩu</Link>
                        <button onClick={this.onClickChangeInfo} className={!isUpdateInfo ? "btn btn-primary" : "btn btn-danger"}>{!isUpdateInfo ? "Cập nhật thông tin" : "Hủy"}</button>
                        <form style={{marginTop : 20}}>
                            <div className="form-group" style={{marginTop:20}}>
                                <label htmlFor="username">Tài khoản</label>
                                <input type="text" name="username" id="username" readOnly={!isUpdateInfo}  onChange={this.onChangeInput}  />
                            </div>
                            <div className="form-group" style={{marginTop:20}}>
                                <label htmlFor="name">Tên</label>
                                <input type="text" name="name" id="name" readOnly={!isUpdateInfo}  onChange={this.onChangeInput} />
                            </div>
                            <div className="form-group" style={{marginTop:20}}>
                                <label htmlFor="year_of_birth">Năm sinh</label>
                                <input type="number" name="year_of_birth" id="year_of_birth" readOnly={!isUpdateInfo}  onChange={this.onChangeInput} />
                            </div>
                            <div className="form-group" style={{marginTop:20}}>
                                <label htmlFor="phone_number">Số điện thoại</label>
                                <input type="text" name="phone_number" id="phone_number" readOnly={!isUpdateInfo}  onChange={this.onChangeInput} />
                            </div>
                            <div className="form-group" style={{marginTop:20}}>
                                <label htmlFor="email">Email</label>
                                <input type="text" name="email" id="email" readOnly={!isUpdateInfo}  onChange={this.onChangeInput} />
                            </div>
                        </form>
                        <button onClick={this.onClickChangeInfo} className="btn btn-success">Cập nhật</button>
                   </div>
                </div>
            );
        }
        return (
            <div>
               <div className="container">
                    <Link to="/ChangePassword" className="btn btn-info">Đổi mật khẩu</Link>
                    <button onClick={this.onClickChangeInfo} className={!isUpdateInfo ? "btn btn-primary" : "btn btn-danger"}>{!isUpdateInfo ? "Cập nhật thông tin" : "Hủy"}</button>
                    <form style={{marginTop : 20}}>
                    <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Tài khoản</label>
                            <input type="text" name="username" id="username" readOnly={!isUpdateInfo} value={currentUser.username} onChange={this.onChangeInput}  />
                        </div>
                        <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Tên</label>
                            <input type="text" name="name" id="name" readOnly={!isUpdateInfo} value={currentUser.name} onChange={this.onChangeInput} />
                        </div>
                        <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Năm sinh</label>
                            <input type="number" name="year_of_birth" id="year_of_birth" readOnly={!isUpdateInfo} value={currentUser.year_of_birth} onChange={this.onChangeInput} />
                        </div>
                        <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Số điện thoại</label>
                            <input type="text" name="phone_number" id="phone_number" readOnly={!isUpdateInfo} value={currentUser.phone_number} onChange={this.onChangeInput} />
                        </div>
                        <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Email</label>
                            <input type="text" name="email" id="email" readOnly={!isUpdateInfo} value={currentUser.email} onChange={this.onChangeInput} />
                        </div>
                    </form>
               </div>
               <table className="table table-striped">
                   <thead >
                       <tr>
                           <td style={{fontWeight:"bold"}}>Tài khoản</td>
                           <td style={{fontWeight:"bold"}}>Tên</td>
                           <td style={{fontWeight:"bold"}}>Bài thi</td>
                           <td style={{fontWeight:"bold"}}>Điểm nghe</td>
                           <td style={{fontWeight:"bold"}}>Điểm đọc</td>
                           <td style={{fontWeight:"bold"}}>Tổng điểm</td>
                           <td style={{fontWeight:"bold"}}>Thời gian làm bài</td>
                           <td style={{fontWeight:"bold"}}>Ngày thi</td>
                       </tr>
                   </thead>
                   <tbody>
                       {this.showTableItem()}
                   </tbody>
               </table>
               {this.showPagition(this.state.numberOfPage)}
            </div>
        );
    }

    onClickChangeInfo = () => {
        var isUpdateInfo = this.state.isUpdateInfo;
        if(isUpdateInfo){
            this.setState({
                name : '',
                year_of_birth : 0,
                phone_number : '',
                email : ''   
            })
        }else{
            var {name, year_of_birth, phone_number, email} = this.state.currentUser;
            this.setState({
                name, year_of_birth, phone_number, email 
            })
        }
        this.setState({
            isUpdateInfo : !isUpdateInfo
        })
    }

    showPagition = (numberOfPage) => {
		var arr = [];
		for(var i = 1; i<= numberOfPage; i++){
			
			arr.push(i);
		}
		console.log(arr);
		
		return (
			<ul className="pagination">
				{this.showPagitionItem(arr)}
			</ul>
		)
    }
    
    showPagitionItem = (arr) =>{
		if(arr == null || arr.length == 0) return null;
		var result = "";
		result = arr.map((element, index) => {
			console.log(element);
			return (
				<li key={index} onClick={() => this.onChangePage(element)} className={element == this.state.page ? "page-item active" : "page-item"}><a  className={element == this.state.page ? "page-link active" : "page-link"}>{element}</a></li>
			)
		});
		return result;
	}

    onChangeInput = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name] : value
        });
    }

}

export default UserDetail;