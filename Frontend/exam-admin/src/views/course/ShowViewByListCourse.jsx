import React from 'react';
import ShowListVideoByCourse from './ShowListVideoByCourse'
import ModalEditCourse from '../course/modal/ModalEditCourse'
import ModalDeleteCourse from '../course/modal/ModalDeleteCourse';
import callApi from '../../utils/ApiCaller';

class ShowViewByListCourse extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listCourse: [],
            idCourseSelected: -1
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            listCourse: nextProps.courses
        })
    }

    componentWillMount() {

    }

    showRowListCourse = () => {
        var courses = this.state.listCourse;
        if (courses.length == 0) return null;
        var results = courses.map((course, index) => {
            return (
                <tr key={index}>
                    <th scope='row'>{index + 1}</th>
                    <td>{course.nameCourse}</td>
                    <td>{course.nameTeacher}</td>
                    <td>{course.totalVideo}</td>
                    <td>{(course.price == 0) ? "Free" : course.price + '$'}</td>
                    <td>
                        <a href="#detailCourseModal" className="assignment" data-toggle="modal" style={{ color: '#FFC107', padding: 5 }} onClick={() => this.setState({idCourseSelected: course.id})}>
                            <i style={this.styleMaterialIcons} data-toggle="tooltip" title="" data-original-title="Assignment">assignment</i>
                        </a>
                        <a href="#editCourseModal" className="edit" data-toggle="modal" style={{ color: '#FFC107', padding: 5 }} onClick={() => this.setState({idCourseSelected: course.id})}>
                            <i style={this.styleMaterialIcons} data-toggle="tooltip" title="" data-original-title="Edit"></i>
                        </a>
                        <a href="#deleteCourseModal" className="delete" data-toggle="modal" style={{ color: '#F44336', padding: 5 }} onClick={() => this.setState({idCourseSelected: course.id})}>
                            <i style={this.styleMaterialIcons} data-toggle="tooltip" title="" data-original-title="Delete"></i>
                        </a>
                    </td>
                </tr>
            )
        })
        return results;
    }

    render() {
        return (
            <div className="table-course">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name Course</th>
                            <th scope="col">Name Teacher</th>
                            <th scope="col">Total Lesson</th>
                            <th scope="col">Price</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.showRowListCourse()}
                    </tbody>
                </table>
                <ShowListVideoByCourse idCourse={this.state.idCourseSelected} handleUpdate={this.handleUpdate}/>
                <ModalEditCourse idCourse={this.state.idCourseSelected} handleUpdate={this.handleUpdate}/>
                <ModalDeleteCourse idCourse={this.state.idCourseSelected} handleUpdate={this.handleUpdate}/>
            </div>
        )
    }

    handleUpdate = (flag) => {
        if (flag) {
            var listCourse = [];
            callApi("GET", "http://54.95.40.41:3000/api/course/get-all", {})
                .then((response) => {
                    listCourse = response.data;
                }).then(() => this.setState({listCourse: listCourse}))
        }
    }


    styleMaterialIcons = {
        fontFamily: 'Material Icons',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontSize: '24px',
        lineHeight: 1,
        letterSpacing: 'normal',
        textTransform: 'none',
        display: 'inline-block',
        whiteSpace: 'nowrap',
        wordWrap: 'normal',
        direction: 'ltr',
        WebkitFontFeatureSettings: 'liga',
        WebkitFontSmoothing: 'antialiased'
    }
}

export default ShowViewByListCourse;