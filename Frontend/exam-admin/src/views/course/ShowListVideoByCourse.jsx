import React from 'react';
import '../../assets/course/Course.css';
import callApi from '../../utils/ApiCaller'
import srcIconViews from '../../assets/scss/all/icons/views.png';

class ShowListVideoByCourse extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            videos: []
        }
    }

    componentWillMount() {
        //get list video

    }

    componentWillReceiveProps(nextProps) {
        // console.log(nextProps);
        var {idCourse} = nextProps;
        // console.log(idCourse);
        if (idCourse)
            callApi('GET', "http://54.95.40.41:3000/api/video/get-by-course?idCourse=" + idCourse, {})
            .then((response) => {
                this.setState({
                    videos: response.data
                })
            })
    }

    showListVideo = () => {
        var listVideo = this.state.videos;

        if (!listVideo) return null;
        var url = "http://54.95.40.41:3000/public/images/lesson/ls_1.jpg"
        var results = listVideo.map((video, index) => {
            return (
                <div className="col-sm-6" style={{ marginBottom: 20, cursor: 'pointer' }} key={index}>
                    <div className="row m-0">
                        <div className="col-sm-5 p-0">
                            <img src={url} width='100%' alt=""/>
                            <span className="duration">17:30</span>
                        </div>
                        <div className="col-sm-7" style={{ paddingTop: 0 }}>
                            <h5>{video.title}</h5>
                            <div className="row m-0 meta_info views">34,000 views</div>
                            <div className="row m-0 meta_info views">1 year ago</div>
                        </div>
                    </div>
                </div>
            )
        })
        return results;
    }

    render() {
        return (
            <div id="detailCourseModal" className="modal fade">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Course Name</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div className="modal-body">
                            <div className="infor-course">
                                
                            </div>
                            <div className="row" style={{marginTop: '20px'}}>
                                {this.showListVideo()}
                            </div>
                        </div>
                        <div className="modal-footer">
                            <input type="button" className="btn btn-default" data-dismiss="modal" value="Thoát" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    styleMaterialIcons = {
        fontFamily: 'Material Icons',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontSize: '24px',
        lineHeight: 1,
        letterSpacing: 'normal',
        textTransform: 'none',
        display: 'inline-block',
        whiteSpace: 'nowrap',
        wordWrap: 'normal',
        direction: 'ltr',
        WebkitFontFeatureSettings: 'liga',
        WebkitFontSmoothing: 'antialiased'
    }
}

export default ShowListVideoByCourse;