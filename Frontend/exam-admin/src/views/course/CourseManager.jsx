import React from 'react';
import callApi from '../../utils/ApiCaller';
import ShowViewByListCourse from './ShowViewByListCourse';
import ModalAddNewCourse from './modal/ModalAddNewCourse';

class CourseManager extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            viewType : 1,
            listCourse : [],
            allCourses: [],
            queryText: '',
            flag: false
        }
    }

    componentWillMount() {
        // get list course
        callApi("GET", "http://54.95.40.41:3000/api/course/get-all", {})
        .then((response) => {
            this.setState({
                listCourse: response.data,
                allCourses: response.data
            })
        })

    }

    showListCourse = () => {
        var courses = this.state.listCourse;
        if (!courses) return null;
        var results = courses.map((course, index) => {
            return (
                <option key={index}>{course.name}</option>
            )
        })
        return results;
    }

    showViewContent = () => {    
        return (
            <ShowViewByListCourse courses={this.state.listCourse}/>
        )
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-body">
                            <h2>Vocabulary Manager</h2>

                            <div className="table-title d-flex flex-row" style={{ marginTop: '20px', textAlign: 'center' }}>
                                <div className="btn-add-course">
                                    <button href="#addCourseModal" className="btn btn-success d-flex align-items-center" data-toggle="modal">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Thêm khóa học mới</span>
                                    </button>
                                </div>
                                <div className='btn-del-course' style={{ marginLeft: '10px' }}>
                                    <button href="#deleteCourseModal" className="btn btn-danger d-flex align-items-center" data-toggle="modal">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Xóa tất cả</span>
                                    </button>
                                </div>
                            </div>

                            {/* table filter */}
                            <div className="table-filter" style={this.styleTableFilters}>
                                <div className="row">
                                    <div className="col-sm-12">
                                        {/* <h3>Hiển thị</h3> */}
                                        <div
                                            className="type-view d-flex flex-row align-items-center"
                                            style={{ marginTop: "15px" }}>
                                            {/* <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="radio-view-by-course" name="radio-type-view" className="custom-control-input" value="1" onChange={(e) => console.log("tungts", e.target.value)}/>
                                                <label className="custom-control-label" for="radio-view-by-course">Theo khóa học</label>
                                            </div>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="radio-view-by-video" name="radio-type-view" className="custom-control-input" value="2" onChange={(e) => console.log("tungts", e.target.value)}/>
                                                <label className="custom-control-label" for="radio-view-by-video">Theo video</label>
                                            </div> */}
                                            {/* <select 
                                                className={"form-control custom-control-inline" + (this.state.viewType != 1 ? " d-none " : "")} 
                                                style={{ marginLeft: '15px', width: 'auto'}}>
                                                {this.showListCourse()}
                                            </select> */}
                                            <div className="d-flex flex-row align-items-center">
                                                <span>Name</span>
                                                <input type="text" className="form-control" style={{ marginLeft: '5px' }} onChange={(e) => this.onChangeQueryText(e.target.value)} />
                                                <button type="button" className="btn btn-primary" style={{ marginLeft: '5px' }} onClick={() => this.search()}>
                                                    <i className="fa fa-search" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            {/* View content */}
                            {this.showViewContent()}
                            
                            {/* ui uploading */}

                        </div>
                    </div>
                </div>
                <ModalAddNewCourse/>
            </div>
        )
    }

    onChangeQueryText = (queryText) => {
        var courses = this.state.listCourse;
        if (!courses || !queryText) {
            this.setState({
                listCourse: this.state.allCourses
            })
            return;
        }
        var newCourses = [];
        courses.map((course, i) => {
            if (course.nameCourse.toLowerCase().includes(queryText.toLowerCase()) || course.nameTeacher.toLowerCase().includes(queryText.toLowerCase())) {
                newCourses.push(course);
            }
        })
        this.setState({
            listCourse: newCourses
        })
    }

    search = () => {

    }

    styleTableFilters = {
        marginTop: '20px',
        padding: '5px 0 20px',
        marginBottom: '5px'
    }

}

export default CourseManager;
