import React from 'react';
import callApi from '../../../utils/ApiCaller'

class ModalDeleteCourse extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            idCourse: -1,
            nextProps: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            idCourse: nextProps.idCourse,
            nextProps: nextProps
        })
    }

    render() {
        return (
            <div id="deleteCourseModal" className="modal fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h4 className="modal-title">Xóa khóa học</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div className="modal-body">
                                <p>Bạn có chắc chắn muốn xoa khóa học này?</p>
                                {/* <p className="text-warning"><small>This action cannot be undone.</small></p> */}
                            </div>
                            <div className="modal-footer">
                                <input type="button" className="btn btn-default" data-dismiss="modal" value="Cancel" />
                                <input type="submit" className="btn btn-danger" data-dismiss="modal" value="Delete" onClick={() => this.deletecourse()}/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    deletecourse = () => {
        var idCourse = this.state.idCourse;
        callApi('POST', "http://54.95.40.41:3000/api/course/delete-course?idCourse=" + idCourse, {})
        .then((response) => {
            console.log(response.data);
        }).then(
            () => this.state.nextProps.handleUpdate(true)
        )
    }

}

export default ModalDeleteCourse;