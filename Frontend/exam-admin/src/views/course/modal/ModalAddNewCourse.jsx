import React from 'react';
import axios from 'axios';

class ModalAddNewCourse extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            videoUploaded: false,
            course: {},
            imgCourse: null,
            listFileUpload: [],   // choose from client
            videos: [],             // list file video upload
            videoCurentUpload: {},   // file, infor video
            indexVideoCurrentUpload: -1,
            progress: 0
        }
    }

    componentWillReceiveProps(nextProps) {

    }

    showUiDownloadProgress = () => {
        var { listFileUpload } = this.state;
        if (this.state.videos.length == 0 && listFileUpload.length == 0) return null;

        for (var i = 0; i < listFileUpload.length; i++) {
            this.state.videos.push({
                index: i,
                file: listFileUpload[i],
                title: ""
            });
        }
        this.state.listFileUpload = [];

        var results = this.state.videos.map((file, index) => {
            return (
                <div className="ui-upLoading h-100" key={index}>
                    <div className="d-flex flex-row align-items-center">
                        <div className="col-md-6" style={{ padding: 0 }}>
                            <input placeholder="Thêm tiêu đề video !!" type="text" className="form-control" required onChange={(e) => this.onChangetitleVideo(index, e.target.value)} />
                        </div>

                        <div className="col-md-4" style={{ marginLeft: '10px', padding: '0 10 0 0' }}>
                            <div class={(this.state.indexVideoCurrentUpload >= index) ? "progress" : "d-none"} style={{ height: '8px' }}>
                                <div
                                    class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                                    role="progressbar"
                                    aria-valuenow={(this.state.indexVideoCurrentUpload > index) ? "100" : this.state.progress}
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                    style={{ width: (this.state.indexVideoCurrentUpload > index) ? "100%" : this.state.progress + "%" }}>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <button className={(this.state.indexVideoCurrentUpload >= index) ? "d-none" : "btn bg-white"} style={{ color: "red" }} onClick={() => this.clickCancelDownload(index)}>
                                <i className="fa fa-times-circle" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            )
        });
        return results;
    }

    onChangetitleVideo = (i, title) => {
        this.state.videos[i].title = title;
    }

    clickCancelDownload = (index) => {

    }

    render() {
        var videos = this.state.videos;
        return (
            <div id="addCourseModal" className="modal fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h4 className="modal-title">Thêm khóa học</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label>Tên khóa học</label>
                                    <input type="text" className="form-control" required onChange={(e) => this.onChangeNameCourse(e.target.value)} />
                                </div>
                                <div className="form-group">
                                    <label>Tên giảng viên</label>
                                    <input type="text" className="form-control" required onChange={(e) => this.onChangeNameTeacher(e.target.value)} />
                                </div>
                                <div className="form-group">
                                    <label>Giá</label>
                                    <input type="text" className="form-control" required onChange={(e) => this.onChangePrice(e.target.value)} />
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Mô tả khóa học: </label>
                                    <textarea class="form-control" id="description-text" onChange={(e) => this.onChangeDescription(e.target.value)}></textarea>
                                </div>
                                <div className="form-group">
                                    <div className="custom-file">
                                        <label className="custom-file-label">
                                            {this.state.imgCourse != null ? this.state.imgCourse.name : "Chọn ảnh"}
                                        </label>
                                        <input type="file" className="custom-file-input" id="chooseImage" onChange={(e) => this.onChooseImage(e)} required />
                                    </div>
                                </div>
                                <h5>Thêm video cho khóa học</h5>
                                <div>
                                    {this.showUiDownloadProgress()}
                                </div>
                                <div className={(this.state.indexVideoCurrentUpload == -1) ? "d-flex flex-row align-items-center" : "d-none"}>
                                    <div className="upload-videos">
                                        <i className="fa fa-plus icon-upload" aria-hidden="true"></i>
                                        <input
                                            type="file" className="custom-file-input" id="chooseVideo"
                                            style={{ height: '100%', zIndex: '3' }}
                                            onChange={(e) => this.onChooseListVideoUpload(e.target.files)}
                                            required multiple />
                                    </div>
                                    <div className={(videos.length == 0) ? "d-none" : ""}>
                                        <div
                                            className="btn bg-success"
                                            style={{ width: '50px', height: '50px', marginLeft: "10px", color: "white" }}
                                            onClick={() => this.clickUploadAllVideo()}>
                                            <i className="fa fa-upload" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <input type="button" className="btn btn-default" data-dismiss="modal" value="Hủy bỏ" />
                                <input 
                                    type="button" 
                                    className="btn btn-success" 
                                    data-dismiss={(this.state.videoUploaded) ? "modal" : ""} 
                                    value="Thêm" onClick={(this.state.videoUploaded) ? this.addNewCourse : () => alert('Bạn phải upload xong video đã')} />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    clickUploadAllVideo = () => {
        var videos = this.state.videos;
        var indexVideoCurrentUpload = this.state.indexVideoCurrentUpload;
        if (indexVideoCurrentUpload == -1) indexVideoCurrentUpload = 0;
        this.addVideoToDB(videos, indexVideoCurrentUpload);
    }

    addNewCourse = () => {
        console.log("add new course");
        // add infor new course 
        var {course} = this.state;
        if (course != undefined)
            axios.post('http://54.95.40.41:3000/api/course/create-course', course)
                .then(function (response) {
                    console.log("added infor course")
                    course.id = response.data.insertId;
                }).then(() => this.uploadImage(course.id))
                .catch(function (error) {
                    console.log("add course err", error);
                    alert('Đã có lỗi xảy ra xin vui lòng thử lại');
                });
    }

    uploadImage = (courseId) => {
        this.state.course.id = courseId;
        // upload image
        const data = new FormData();
        data.append('fileToUpload', this.state.imgCourse);
        data.append('filename', this.state.course.id);

        axios.post('http://54.95.40.41:3000/course/img/upload', data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(function (response) {
            console.log('postImgOK', response);
            alert('Đã thêm vào DB');
        }).catch(function (error) {
            console.log('postImgErr', error);
        });
    }

    addVideoToDB = (videos, indexVideoCurrentUpload) => {
        console.log("indexVideoCurrentUpload", indexVideoCurrentUpload);
        this.setState({
            progress: 0
        })

        if (indexVideoCurrentUpload == videos.length){
            alert("upload done");
            this.state.course.totalVideo = videos.length;
            this.setState({
                videoUploaded: true
            })
            return;
        } 

        var video = videos[indexVideoCurrentUpload];

        // post infor video
        if (video.title){
            
            this.postInforVideo(video, async (idVideo) => {
                await this.setState({
                    videoCurentUpload: video,
                    indexVideoCurrentUpload: indexVideoCurrentUpload
                })
                // console.log(video);
                const data = new FormData();
                data.append("filename", idVideo);
                data.append("fileToUpload", video.file);
    
                // upload video
                this.uploadVideo(data, videos, indexVideoCurrentUpload);
    
            });
        } else {
            alert("Bạn phải thêm tiêu đề cho video")
        }

    }

    uploadVideo = (data, videos, indexVideoCurrentUpload) => {
        console.log("indexVideoCurrentUpload", indexVideoCurrentUpload);
        axios.post('http://54.95.40.41:3000/course/video/upload', data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },

            onUploadProgress: progressEvent => {
                var propgress = Math.round(progressEvent.loaded / progressEvent.total * 100);
                this.setState({
                    progress: propgress
                })
                console.log(propgress);
            }
        }).then(function (response) {
            console.log('postVideoOK', response);
        }).then(() => this.addVideoToDB(videos, indexVideoCurrentUpload + 1))
        .catch(function (error) {
            console.log('postVideoErr', error);
        });

    }

    postInforVideo = (video, callback) => {
        axios.post('http://54.95.40.41:3000/api/video/create-infor-video', {
            courseId: 1,
            titleVideo: video.title,
            lengthVideo: "0",
            urlVideo: ""
        }).then((response) => {
            var idVideo = response.data.insertId;
            callback(idVideo);
        })
    }

    onChooseListVideoUpload = (listFile) => {
        // console.log(listFile);
        this.setState({
            listFileUpload: listFile
        })
    }

    onChooseImage = (e) => {
        this.setState({
            imgCourse: e.target.files[0]
        })
    }

    onChangeNameCourse = (nameCourse) => {
        this.state.course.nameCourse = nameCourse;
    }

    onChangeNameTeacher = (nameTeacher) => {
        this.state.course.nameTeacher = nameTeacher;
    }

    onChangePrice = (price) => {
        this.state.course.price = price;
    }

    onChangeDescription = (description) => {
        this.state.course.description = description;
    }

}

export default ModalAddNewCourse;