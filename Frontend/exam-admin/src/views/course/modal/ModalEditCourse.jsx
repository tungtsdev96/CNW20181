import React from 'react';
import callApi from '../../../utils/ApiCaller';

class ModalEditCourse extends React.Component {

    constructor(props) {
        super(props);
        this.state= {
            course : {
                nameCourse: "",
                nameTeacher: "",
                price: "",
                description: ""
            },
            nextProps: {}            
        }
    }

    componentWillReceiveProps(nextProps){
        var idCourse = nextProps.idCourse;
        
        if (idCourse == -1) return;
        callApi('GET', "http://54.95.40.41:3000/api/course/get-by-id?idCourse=" + idCourse, {})
        .then((response) => {
            this.setState({
                course: response.data[0],
                nextProps: nextProps
            })
        })
    }

    componentWillMount() {

    }

    render() {
        var {course} = this.state;
        // if (!course) return null;
        return (
            <div id="editCourseModal" className="modal fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h4 className="modal-title">{course.nameCourse}</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label>Tên khóa học</label>
                                    <input type="text" className="form-control" required placeholder={course.nameCourse} onChange={(e) => this.onChangeNameCourse(e, e.target.value)}/>
                                </div>
                                <div className="form-group">
                                    <label>Tên giảng viên</label>
                                    <input type="text" className="form-control" required placeholder={course.nameTeacher} onChange={(e) => this.onChangeNameTeacher(e, e.target.value)}/>
                                </div>
                                <div className="form-group">
                                    <label>Giá</label>
                                    <input type="text" className="form-control" required placeholder={course.price} onChange={(e) => this.onChangePrice(e, e.target.value)}/>
                                </div>
                                <div className="form-group">
                                    <label className="col-form-label">Mô tả khóa học: </label>
                                    <textarea className="form-control" id="description-text" placeholder={course.description} onChange={(e) => this.onChangeDescription(e, e.target.value)} required></textarea>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <input type="button" className="btn btn-default" data-dismiss="modal" value="Hủy bỏ" />
                                <input type="button" className="btn btn-success" data-dismiss="modal" value="Cập nhật" onClick={() => this.updateCourse()}/>
                            </div>
                            <script type="text/javascript"></script>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
    
    updateCourse = () => {
        console.log("update",this.state.course);
        callApi('POST', "http://54.95.40.41:3000/api/course/update-course", {course: this.state.course})
        .then((response) => {
            console.log(response.data);
            this.state.nextProps.handleUpdate(true)
        })
    } 

    onChangeNameCourse = (event, nameCourse) => {
        event.preventDefault();
        this.state.course.nameCourse = nameCourse;
    }
    
    onChangeNameTeacher = (event, nameTeacher) => {
        event.preventDefault();
        this.state.course.nameTeacher = nameTeacher;
    }

    onChangePrice = (event, price) => {
        event.preventDefault();
        this.state.course.price = price;
    }

    onChangeDescription = (event, description) => {
        event.preventDefault();
        this.state.course.description = description;
    }

}

export default ModalEditCourse;