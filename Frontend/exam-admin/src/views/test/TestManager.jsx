import React from 'react';
import {Link} from 'react-router-dom';
import callApi from './../../utils/ApiCaller'
import {Redirect} from 'react-router-dom'
class TestManager extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			page : 1,
			numberOfPage : 0,
			listTest : []
		}
	}

	componentWillMount(){
		this.loadData(this.state.page, 7);
	}

	loadData = (page, limit) => {
		var url = 'http://54.95.40.41:3000/tests?page=' + page + "&limit=7";
		callApi('GET', url, {}).then(result => {
			console.log(result.data.data)
			this.setState({
				listTest : result.data.data
			})
		});
	}

	onChangeTestState = (exam_id, currentState) => {
		var url = 'http://54.95.40.41:3000/tests/update-state';
		var newState = 1 - parseInt(currentState + "");
		var data = {
			exam_id , newState
		}
		console.log(data)
		
		callApi('POST', url, data).then(result => {
			console.log(result);
			this.loadData(this.state.page, 7)
		});
	}

	
	componentDidMount (){
		var url = 'http://54.95.40.41:3000/tests?page=' + this.state.page + "&limit=7";
		callApi('GET', url, {}).then(result => {
			console.log(result.data)
			this.setState({
				listTest : result.data.data
			})
		});
		var url2 = "http://54.95.40.41:3000/tests/count-all?state=0";
		callApi('GET', url2, {}).then(res => {
			console.log(res.data);
			var result = res.data[0];
			var count = result.count;
			var numberOfPage = 0;
			if(count % 10 == 0){
				numberOfPage = Math.floor(count / 10);
			}else{
				numberOfPage = Math.floor(count / 10) + 1;
			}
			console.log(numberOfPage);
			this.setState({
				numberOfPage : numberOfPage
			})
		});
	}


	showPagitionItem = (arr) =>{
		if(arr == null || arr.length == 0) return null;
		var result = "";
		result = arr.map((element, index) => {
			console.log(element);
			return (
				<li key={index} onClick={() => this.onChangePage(element)} className={element == this.state.page ? "page-item active" : "page-item"}><a  className={element == this.state.page ? "page-link active" : "page-link"}>{element}</a></li>
			)
		});
		return result;
	}

	onChangePage = (page) => {
		this.setState({
			page : page
		})
		var url = 'http://54.95.40.41:3000/tests?page=' + page + "&limit=7";
		callApi('GET', url, {}).then(result => {
			console.log(result.data)
			this.setState({
				listTest : result.data.data
			})
		});
	}


	showPagition = (numberOfPage) => {
		var arr = [];
		for(var i = 1; i<= numberOfPage; i++){
			
			arr.push(i);
		}
		console.log(arr);
		
		return (
			<ul className="pagination">
				{this.showPagitionItem(arr)}
			</ul>
		)
		
	}

	showTestTable = (listTest) => {
		if(listTest == null) return null;
		var result = null;
		result = listTest.map( (test, index ) => {
			var editLink = "/test-manager/edit/" + test.id;
			var analyticLink = "/test-manager/analytic/" + test.id;
			return (
				<tr key={index}>
					<td>
						{test.id}
					</td>
					<td>
						{test.name}
					</td>
					<td>
						{test.description}
					</td>
					<td>
						<Link to={editLink} className="btn btn-link">Sửa</Link>
						<Link to={analyticLink} className="btn btn-warning" style={{marginLeft : 10}}>Xem thống kê</Link>
						<button onClick={() => this.onChangeTestState(test.id, test.state)} className={test.state == 1 ? "btn btn-danger" : "btn btn-info"} style={{marginLeft : 10}}>{test.state == 1 ? "Disable" : "Enable"}</button>
					</td>
				</tr>
			);
		});
		return result;
		
	}


	render() {
		var Session = localStorage.getItem("Session");
		var username = localStorage.getItem("username");
		if(!(Session && username) ){
		return  <Redirect to='/Login'  />
		}
		return (
			<div>
				<Link className="btn btn-primary" to="/add-new-test">Add new Test</Link>
				
				<table className="table">
					<thead>
						<tr>
							<th>
								ID
							</th>
							<th>
								Tên
							</th>
							<th>
								Mô tả
							</th>
							<th style={{alignItems : "center", justifyContent: "center"}}>
								Action
							</th>
						</tr>
					</thead>
					<tbody>
					{this.showTestTable(this.state.listTest)}
					</tbody>
				</table><br />
				{this.showPagition(this.state.numberOfPage)} 
			</div>
		);
	}
}
export default TestManager;
