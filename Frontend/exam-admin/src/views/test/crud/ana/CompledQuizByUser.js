import React from 'react';
import callApi from './../../../../utils/ApiCaller'
class CompledQuizByUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser : {},
            lstCompletedQuiz : [],
            page : 1,
            numberOfPage : [],
            current_user_id : 0
        }
    }

    showPagition = (numberOfPage) => {
		var arr = [];
		for(var i = 1; i<= numberOfPage; i++){
			
			arr.push(i);
		}
		console.log(arr);
		
		return (
			<ul className="pagination">
				{this.showPagitionItem(arr)}
			</ul>
		)
    }

    fetchCompletedQuiz = (page) => {
        var {match} = this.props;
        var current_user_id = match.params.user_id;
        var user_id = current_user_id;
        var url = "http://54.95.40.41:3000/quiz/completed-quiz-by-user?user_id=" + user_id+"&page="+page;
        console.log(url);
        callApi('GET', url, {}).then(res => {
            console.log(res.data);
            this.setState({
                lstCompletedQuiz : res.data
            })
        })
    }

    onChangePage = (page) => {
        this.fetchCompletedQuiz(page);
        this.setState({
            currentPage : page
        })
    }
    
    showPagitionItem = (arr) =>{
		if(arr == null || arr.length == 0) return null;
		var result = "";
		result = arr.map((element, index) => {
			console.log(element);
			return (
				<li key={index} onClick={() => this.onChangePage(element)} className="page-item"><a  className={element == this.state.page ? "page-link active" : "page-link"}>{element}</a></li>
			)
		});
		return result;
	}

    componentWillMount(){
        var {match} = this.props;
        var current_user_id = match.params.user_id;
        var user_id = current_user_id;
        var url = "http://54.95.40.41:3000/users/user-info?user_id=" + user_id;
        console.log(url);
        callApi('GET', url, {}).then(res => {
            console.log(res.data.data);
            var {name, year_of_birth, phone_number, email} = res.data.data;
            this.setState({
                currentUser :  res.data.data,
                name, year_of_birth, phone_number, email
            })
        });
        console.log(current_user_id);
        this.setState({
            current_user_id : current_user_id
        });

        var url2 = "http://54.95.40.41:3000/quiz/count-completed-quiz-by-user?user_id=" + user_id;
		callApi('GET', url2, {}).then(res => {
            console.log(url2);
            console.log(res.data);
			var result = res.data;
			var count = result.count;
			var numberOfPage = 0;
			if(count % 10 == 0){
				numberOfPage = Math.floor(count / 10);
			}else{
				numberOfPage = Math.floor(count / 10) + 1;
			}
			console.log(numberOfPage);
			this.setState({
				numberOfPage : numberOfPage
			})
        });
         this.fetchCompletedQuiz(1);
    }

    convertDateTime = (utcDate) => {
        var localDate = new Date(utcDate);
        return localDate.toLocaleString();
    }

    fetchData = (current_user_id, page) => {
        var url = "http://54.95.40.41:3000/quiz/completed-quiz-by-user?user_id="+ current_user_id + "&page=" + page;
        callApi('GET', url, {}).then(res => {
            console.log(res.data)
        })
    }

    showTableItem = () => {
        var {lstCompletedQuiz} = this.state;
        console.log(lstCompletedQuiz)
        var result = null;
        result = lstCompletedQuiz.map((element, index) => {
            return(
                <tr key={index}>
                    <td>{element.username}</td>
                    <td>{element.name}</td>
                    <td>{element.name}</td>
                    <td>{element.listening_score}</td>
                    <td>{element.reading_score}</td>
                    <td>{element.total_score}</td>
                    <td>{element.used_time}</td>
                    <td>{this.convertDateTime(element.time)}</td>
                </tr>
            )
        });
        return result;
    }

    render() {
        var currentUser = this.state.currentUser
        return (
            <div>
                <h2>Thông tin chi tiết</h2>
               <div className="container">
                    <form style={{marginTop : 20}}>
                        <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Tài khoản</label>
                            <input className="form-control" type="text" name="username" id="username" readOnly value={currentUser.username}   />
                        </div>
                        <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Tên</label>
                            <input className="form-control" type="text" name="name" id="name" readOnly value={currentUser.name}  />
                        </div>
                        <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Năm sinh</label>
                            <input className="form-control" type="number" name="year_of_birth" id="year_of_birth" readOnly value={currentUser.year_of_birth}  />
                        </div>
                        <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Số điện thoại</label>
                            <input className="form-control" type="text" name="phone_number" id="phone_number" readOnly value={currentUser.phone_number} />
                        </div>
                        <div className="form-group" style={{marginTop:20}}>
                            <label htmlFor="">Email</label>
                            <input className="form-control" type="text" name="email" id="email" readOnly value={currentUser.email} />
                        </div>
                    </form>
               </div>
               <table className="table table-striped">
                   <thead >
                       <tr>
                           <td style={{fontWeight:"bold"}}>Tài khoản</td>
                           <td style={{fontWeight:"bold"}}>Tên</td>
                           <td style={{fontWeight:"bold"}}>Bài thi</td>
                           <td style={{fontWeight:"bold"}}>Điểm nghe</td>
                           <td style={{fontWeight:"bold"}}>Điểm đọc</td>
                           <td style={{fontWeight:"bold"}}>Tổng điểm</td>
                           <td style={{fontWeight:"bold"}}>Thời gian làm bài</td>
                           <td style={{fontWeight:"bold"}}>Ngày thi</td>
                       </tr>
                   </thead>
                   <tbody>
                        {this.showTableItem()}
                   </tbody>
               </table>
               {this.showPagition(this.state.numberOfPage)}
            </div>
        );
    }
}

export default CompledQuizByUser;
