import React from 'react';
import callApi from "./../../../../utils/ApiCaller"
import axios from 'axios'
import * as XLSX from 'xlsx';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import { Auth } from "aws-amplify";
class CreateNewTest extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            audio: {},
            pictures: [],
            excelFile: {},
            lstQuestionsPart1: [],
            lstQuestionsPart2: [],
            lstQuestionsPart3: [],
            lstQuestionsPart4: [],
            lstQuestionsPart5: [],
            lstGroupPart6: [],
            lstGroupPart7: []
        }
    }


    onPicureChange = (e) => {
        var pictures = e.target.files;
        this.setState({
            pictures: pictures
        });
        console.log(e.target.id)
        console.log(this.state.pictures)
    }

    onAudioChange = (e) => {
        var audio = e.target.files[0];
        this.setState({
            audio: audio
        });
    }

    onExcelChange = (e) => {
        this.readQuestionPart1(e);

    }

    isValid = () => {
        var {audio, pictures, excelFile} = this.state;
        if(audio == null || audio == undefined || audio == {}){
            return false;
        }
        if(pictures == null || pictures == undefined || pictures.length == 0){
            return false;
        }
        if(excelFile == null || excelFile == undefined || excelFile == {}){
            return false;
        }
        return true;
    }

    submit = () => {

    };

    onSubmitForm = async e => {
        e.preventDefault();
        const reader = new FileReader();
        var formData = new FormData();
        for (var i = 0; i < this.state.pictures.length; i++) {
            let file = this.state.pictures[i];
            var indexOfDot = file.name.length - 1;
            while(indexOfDot >= 0){
                if(file.name.charAt(indexOfDot) == '.'){
                    break;
                }
                indexOfDot --;
            }
            formData.append('file' + file.name.substring(0, indexOfDot) + '', file, file.name);
        }
        formData.append('audio', this.state.audio, this.state.audio.name);
        formData.append('excelFile', this.state.excelFile, this.state.excelFile.name);
        console.log(this.refs.name.value);

        var data = {
            lstQuestionsPart1 : this.state.lstQuestionsPart1,
            lstQuestionsPart2 : this.state.lstQuestionsPart2,
            lstQuestionsPart3 : this.state.lstQuestionsPart3,
            lstQuestionsPart4 : this.state.lstQuestionsPart4,
            lstQuestionsPart5 : this.state.lstQuestionsPart5,
            lstGroupPart6 : this.state.lstGroupPart6,
            lstGroupPart7 : this.state.lstGroupPart7,
            name : this.refs.name.value,
            description : this.refs.description.value,
        };
        console.log(data)
        formData.append("data", JSON.stringify(data));

        var url = 'http://54.95.40.41:3000/tests';
        axios.post(url, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            console.log(res)
        })
    }

    componentDidMount(){
        Auth.currentAuthenticatedUser().then(function(user) {
            console.log("USER: " + JSON.stringify(user))
          })
    }

    readQuestionPart1(e) {
        var excelFile = e.target.files[0];
        console.log(e)
        this.setState({
            excelFile: excelFile
        });
        console.log(excelFile);
        const reader = new FileReader();
        reader.onload = (e) => {
            var data = e.target.result;
            var workbook = XLSX.read(data, {
                type: 'binary'
            });
            console.log((workbook))
            var sheet = workbook.Sheets["Sheet1"];
            var XL_row_object = XLSX.utils.sheet_to_json(sheet);
            var json_object = JSON.stringify(XL_row_object);
            var lstQuestionsPart1 = JSON.parse(json_object);
            console.log(lstQuestionsPart1);

            sheet = workbook.Sheets["Sheet2"];
            XL_row_object = XLSX.utils.sheet_to_json(sheet);
            json_object = JSON.stringify(XL_row_object);
            var lstQuestionsPart2 = JSON.parse(json_object);
            console.log(lstQuestionsPart2);

            sheet = workbook.Sheets["Sheet3"];
            XL_row_object = XLSX.utils.sheet_to_json(sheet);
            json_object = JSON.stringify(XL_row_object);
            var lstQuestionsPart3 = JSON.parse(json_object);
            console.log(lstQuestionsPart3);

            sheet = workbook.Sheets["Sheet4"];
            XL_row_object = XLSX.utils.sheet_to_json(sheet);
            json_object = JSON.stringify(XL_row_object);
            var lstQuestionsPart4 = JSON.parse(json_object);
            console.log(lstQuestionsPart4);

            sheet = workbook.Sheets["Sheet5"];
            XL_row_object = XLSX.utils.sheet_to_json(sheet);
            json_object = JSON.stringify(XL_row_object);
            var lstQuestionsPart5 = JSON.parse(json_object);
            console.log(lstQuestionsPart5);


            var lstGroupPart6 = [];
            sheet = workbook.Sheets["Sheet6"];
            XL_row_object = XLSX.utils.sheet_to_json(sheet);
            json_object = JSON.stringify(XL_row_object);
            var parsedData = JSON.parse(json_object)
            // console.log(parsedData);
            var group = null;
            var indexOfStart = [];
            var i = 0;
            while(i < parsedData.length){
                var question = parsedData[i];
                question.part = 6;
                if(question.common){
                    if(group != null && group != undefined){
                        lstGroupPart6.push(group);
                    }
                    group = new Object();
                    group.common = question.common;
                    group.questions = new Array();
                    group.questions.push({index : question.index, title : question.title, A : question.A,B : question.B,C : question.C,D : question.D,answer : question.answer, explanation : question.explanation, part : 6 })
                }else{
                    group.questions.push(question);
                    if(i == parsedData.length - 1){
                        lstGroupPart6.push(group);
                    }
                }
            i ++;
            }
            var lstGroupPart7 = [];
            sheet = workbook.Sheets["Sheet7"];
            XL_row_object = XLSX.utils.sheet_to_json(sheet);
            json_object = JSON.stringify(XL_row_object);
            parsedData = JSON.parse(json_object)
            // console.log(parsedData);
            group = null;
            var indexOfStart = [];
            var i = 0;
            while(i < parsedData.length){
                var question = parsedData[i];
                question.part = 7;
                if(question.common){
                    if(group != null){
                        lstGroupPart7.push(group);
                    }
                    group = new Object();
                    group.common = question.common;
                    group.questions = new Array();
                    group.questions.push({index : question.index, title : question.title, A : question.A,B : question.B,C : question.C,D : question.D,answer : question.answer, explanation : question.explanation, part : 7 })
                }else{
                    group.questions.push(question);
                    if(i == parsedData.length - 1){
                        lstGroupPart7.push(group);
                    }
                }
                i ++;
            }
            console.log("Part 7",lstGroupPart7)
            this.setState({
                lstGroupPart7,
                lstGroupPart6,
                lstQuestionsPart1,
                lstQuestionsPart2,
                lstQuestionsPart3,
                lstQuestionsPart4,
                lstQuestionsPart5,

            })
        }
        reader.readAsBinaryString(excelFile);

    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmitForm}>
                    <div className="form-group">
                        <label>Tên bài kiểm tra</label>
                        <input type="text" className="form-control" id="name" name="name" placeholder="" ref="name"/>
                    </div>
                    <div className="form-group">
                        <label>Mô tả</label>
                        <input type="text" className="form-control" id="description" name="description" placeholder="" ref="description"/>
                    </div>

                    <div className="form-group">
                        <label>Am thanh</label>
                        <input type="file" className="form-control" id="audio" name="audio"
                               onChange={e => this.onAudioChange(e)} placeholder=""/>
                    </div>

                    <div className="form-group">
                        <label>Anh Part 1</label>
                        <input type="file" multiple className="form-control" id="pictures" name="pictures"
                               placeholder="" onChange={e => this.onPicureChange(e)}/>
                    </div>

                    <div className="form-group">
                        <label>File Excel</label>
                        <input type="file" multiple className="form-control" id="excelFile" name="excelFile"
                               placeholder="" onChange={e => this.onExcelChange(e)}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        );
    }
}

export default CreateNewTest;
