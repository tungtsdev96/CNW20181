import React from 'react';
import { Auth } from "aws-amplify";
import {Redirect} from 'react-router-dom';
var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
class Login extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			username: '',
			password: '',
			isLogin : false
		}
	}

	onChangeInput = (e) => {

		var target = e.target;
		var name = target.name;
		var value = target.value;
		this.setState({
			[name]: value
		}); 
	}

	onSubmit = async (e) => {
		e.preventDefault();
		try {
			var data = await Auth.signIn(this.state.username, this.state.password);
			alert("Logged in");
			console.log(data);
			localStorage.setItem("Session", data.Session);
			localStorage.setItem("username", data.username);
		} catch (e) {
			alert(e.message);
		}
	}

	render() {
		if(!this.state.isLogin){
			return <Redirect to="/test-manager" />
		}
		return (
			<div>
				<form onSubmit={this.onSubmit}>
					<div className="form-group">
						<label htmlFor="username">Tài khoản</label>
						<input type="text" className="form-control" name="username" id="username" onChange={this.onChangeInput}></input>
					</div>
					<div className="form-group">
						<label htmlFor="password">Mật khẩu</label>
						<input type="password" className="form-control" name="password" id="password" onChange={this.onChangeInput}></input>
					</div>
					<input type="submit" value="Đăng nhập" />
				</form>
			</div>
		);
	}
}
export default Login;
