import React from 'react';
import ModalAddVocabulary from './ModalAddVocabulary';
import ModalEditVocabulary from './ModalEditVocabulary';
import ModalDeleteVocabulary from './ModalDeteleVocabulary';
import callApi from '../../utils/ApiCaller'
import axios from 'axios';

class VocabularyCrud extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listTopic: [],
            idCurrentTopic: 0,
            listLesson: [],
            idCurrentLesson: 0,
            listVocab: [],
            totalRecord: 20,
            currentPage: 1,
            textQuery: "",
            vocabSelected: {}
        }
    }

    componentWillMount() {
        //get all topic 
        callApi('GET', "http://54.95.40.41:3000/api/topic/get-all", {})
            .then((response) => {
                this.setState({
                    listTopic: response.data
                })
            })

        //get all lesson
        callApi('GET', "http://54.95.40.41:3000/api/lesson/get-all", {})
            .then((response) => {
                this.setState({
                    listLesson: response.data
                })
            })

        // get List Vocab
        callApi('GET', 'http://54.95.40.41:3000/api/vocabulary/search?page=1&totalRecord=20', {})
            .then((response) => {
                this.setState({
                    listVocab: response.data,
                })
            })

    }

    showItemLessonSelect = () => {
        var { listLesson } = this.state;
        if (listLesson.length == 0) return null;
        var results = listLesson.map((lesson, index) => {
            return (
                <option key={index}>{lesson.lessonNameEn}</option>
            )
        })
        return results;
    }

    showItemTopicSelect = () => {
        var { listTopic } = this.state;
        if (listTopic.length == 0) return null;
        var results = listTopic.map((topic, index) => {
            return (
                <option key={index}>{topic.topicEn}</option>
            )
        })
        return results;
    }

    showListVocab = () => {
        var listVocab = this.state.listVocab;
        if (!listVocab) return null;
        var results = listVocab.map((vocab, index) => {
            return (
                <tr key={index}>
                    <th scope="row">{vocab.id}</th>
                    <td>{vocab.vocabularyEn}</td>
                    <td>{vocab.pronunciation}</td>
                    <td>{vocab.explantion}</td>
                    <td>{vocab.vocabularyVi}</td>
                    <td>
                        <a href="#editVocabModal" className="edit" data-toggle="modal" style={{ color: '#FFC107', padding: 5 }} onClick={() => this.setState({vocabSelected: vocab})}>
                            <i style={this.styleMaterialIcons} data-toggle="tooltip" title="" data-original-title="Edit"></i>
                        </a>
                        <a href="#deleteVocabModal" className="delete" data-toggle="modal" style={{ color: '#F44336', padding: 5 }} onClick={() => this.setState({vocabSelected: vocab})}>
                            <i style={this.styleMaterialIcons} data-toggle="tooltip" title="" data-original-title="Delete"></i>
                        </a>
                    </td>
                </tr>
            )
        })
        return results;
    }

    showPagination = () => {
        var currentPage = this.state.currentPage;
        var indexPage = Math.floor((currentPage - 1) / 5);
        var pages = [];
        for (var i = 0; i < 5; i++) {
            pages.push(indexPage * 5 + i + 1);
        }

        var results = pages.map((value, index) => {
            var nameClass = "page-item " + (currentPage == value ? "active" : "");
            return (
                <li className={nameClass} key={index} onClick={() => this.onChangePage(value)}>
                    <a className="page-link" style={{ border: 'none' }} key={index}>{value}</a>
                </li>
            )
        })
        return results;
    }

    onChangePage = async (page) => {
        await this.setState({
            currentPage: page
        })

        this.getListVocabToshow();
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-body">
                            <h2>Vocabulary Manager</h2>

                            <div className="table-title d-flex flex-row" style={{ marginTop: '20px', textAlign: 'center' }}>
                                <div className="btn-add-vocab">
                                    <button href="#addVocabModal" className="btn btn-success d-flex align-items-center" data-toggle="modal">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Add New Vocabulary</span>
                                    </button>
                                </div>
                                <div className='btn-del-vocab' style={{ marginLeft: '10px' }}>
                                    <button href="#deleteVocabModal" className="btn btn-danger d-flex align-items-center" data-toggle="modal">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Delete</span>
                                    </button>
                                </div>
                                <div className='btn-import-vocab' style={{ marginLeft: '10px' }}>
                                    <button href="#" className="btn btn-secondary d-flex align-items-center">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Import from Excel</span>
                                    </button>
                                </div>
                                <div className='btn-export-vocab' style={{ marginLeft: '10px' }}>
                                    <button href="#" className="btn btn-secondary d-flex align-items-center">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Export to Excel</span>
                                    </button>
                                </div>
                            </div>

                            {/* table filter */}
                            <div className="table-filter" style={this.styleTableFilters}>
                                <div className="row">
                                    <div className="col-sm-3">
                                        <div className="d-flex justify-content-start align-items-center">
                                            <span>Hiển thị</span>
                                            <select className="form-control" style={{ marginLeft: '15px', width: '70px' }} onChange={(e) => this.onChangeTotalRecord(e.target.value)}>
                                                <option>20</option>
                                                <option>30</option>
                                                <option>45</option>
                                                <option>60</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-sm-9">
                                        <div className="d-flex justify-content-end align-items-center">
                                            <span className="filter-icon"><i className="fa fa-filter"></i></span>
                                            <div className="d-flex flex-row align-items-center" style={{ float: 'right', marginLeft: '15px' }}>
                                                <span>Topic</span>
                                                <select className="form-control" style={{ marginLeft: '5px' }} onChange={(e) => this.onChangeTopic(e)}>
                                                    <option>All</option>
                                                    {this.showItemTopicSelect()}
                                                </select>
                                            </div>
                                            <div className="d-flex flex-row align-items-center" style={{ float: 'right', marginLeft: '15px' }}>
                                                <span>Lesson</span>
                                                <select className="form-control" style={{ marginLeft: '5px' }} onChange={(e) => this.onChangeLesson(e)}>
                                                    <option>All</option>
                                                    {this.showItemLessonSelect()}
                                                </select>
                                            </div>
                                            <div className="d-flex flex-row align-items-center" style={{ float: 'right', marginLeft: '15px' }}>
                                                <span>Tìm kiếm</span>
                                                <input type="text" className="form-control" style={{ marginLeft: '5px' }} onChange={(e) => this.onChangeQueryText(e.target.value)}/>
                                                <button type="button" className="btn btn-primary" style={{ marginLeft: '5px' }} onClick={() => this.getListVocabToshow()}>
                                                    <i className="fa fa-search" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/* table vocab */}
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">VocabularyEn</th>
                                        <th scope="col">Pronunciation</th>
                                        <th scope="col">Explantion</th>
                                        <th scope="col">VocabularyVi</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.showListVocab()}
                                </tbody>
                            </table>

                            {/* pagination */}
                            <div className="page">
                                <ul className="pagination" style={{cursor: 'pointer'}}>
                                    <li className="page-item" onClick={() => this.onPreviousPage()}>
                                        <a className="page-link" aria-label="Previous" style={{ border: 'none' }}>
                                            <span aria-hidden="true">Previous</span>
                                            <span className="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    {this.showPagination()}
                                    <li className="page-item" onClick={() => this.onNextPage()}>
                                        <a className="page-link" aria-label="Next" style={{ border: 'none' }}>
                                            <span aria-hidden="true">Next</span>
                                            <span className="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            {/* <!-- Add Modal HTML --> */}
                            <ModalAddVocabulary lessons={this.state.listLesson} />

                            {/* <!-- Edit Modal HTML --> */}
                            <ModalEditVocabulary lessons={this.state.listLesson} topics={this.state.listTopic} vocab={this.state.vocabSelected}/>

                            {/* <!-- Delete Modal HTML --> */}
                            <ModalDeleteVocabulary vocab={this.state.vocabSelected}/>

                        </div>
                    </div>
                </div>
            </div>
        );
    }

    onPreviousPage = async () => {
        var currentPage = this.state.currentPage;
        if (currentPage == 1) return;
        currentPage--;
        await this.setState({
            currentPage : currentPage
        })

        this.getListVocabToshow();

    }

    onNextPage = async () => {
        var currentPage = this.state.currentPage;
        if (currentPage == 31) return;
        currentPage++;
        await this.setState({
            currentPage : currentPage
        })

        this.getListVocabToshow();

    }


    onChangeTotalRecord = async (totalRecord) => {
        await this.setState({
            totalRecord: totalRecord
        })

        this.getListVocabToshow();
    }

    onChangeQueryText = (queryText) => {
        this.setState({
            textQuery: queryText
        })
    }

    onChangeTopic = (e) => {
        var topicName = e.target.value;
        var idTopic = 0;
        var listTopic = this.state.listTopic;
        
        for (var i = 0; i < listTopic.length; i++) {
            var topic = listTopic[i];
            if (topicName == topic.topicEn) {
                idTopic = topic.id;
                this.setState({
                    idCurrentTopic: idTopic
                })
                break;
            }
        }

        if (idTopic != 0) {
             callApi('GET', "http://54.95.40.41:3000/api/lesson/get-list-lesson-topic?idTopic=" + idTopic, {})
            .then((response) => {
                this.setState({
                    listLesson: response.data
                })
            })
        } else {
            callApi('GET', "http://54.95.40.41:3000/api/lesson/get-all", {})
            .then((response) => {
                this.setState({
                    listLesson: response.data
                })
            })
        }
       
    }

    onChangeLesson = (e) => {
        var lessonName = e.target.value;
        var idLesson = 0;
        var listLesson = this.state.listLesson;
        
        for (var i = 0; i < listLesson.length; i++) {
            var lesson = listLesson[i];
            if (lessonName == lesson.lessonNameEn) {
                idLesson = lesson.id;
                break;
            }
        }

        this.setState({
            idCurrentLesson: idLesson
        })
    }

    getListVocabToshow = () => {
        var page = this.state.currentPage;
        var totalRecord = this.state.totalRecord;
        var idLesson = this.state.idCurrentLesson;
        var query = this.state.textQuery;

        axios.get('http://54.95.40.41:3000/api/vocabulary/search', {
            params: {
                page: page,
                idLesson: idLesson,
                totalRecord: totalRecord,
                query: query
            }
          }).then((response) => {
            this.setState({
                listVocab: response.data
            })
          });

    }

    styleTableFilters = {
        marginTop: '20px',
        padding: '5px 0 20px',
        marginBottom: '5px'
    }

    styleMaterialIcons = {
        fontFamily: 'Material Icons',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontSize: '24px',
        lineHeight: 1,
        letterSpacing: 'normal',
        textTransform: 'none',
        display: 'inline-block',
        whiteSpace: 'nowrap',
        wordWrap: 'normal',
        direction: 'ltr',
        WebkitFontFeatureSettings: 'liga',
        WebkitFontSmoothing: 'antialiased'
    }
}

export default VocabularyCrud;