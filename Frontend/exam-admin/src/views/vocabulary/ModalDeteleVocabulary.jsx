import React from 'react';
import callApi from '../../utils/ApiCaller';

class ModalDeleteVocabulary extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vocab: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            vocab: nextProps.vocab
        })
    }

    render() {
        return (
            <div id="deleteVocabModal" className="modal fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h4 className="modal-title">Xóa từ vựng</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div className="modal-body">
                                <p>Bạn có chắc chắn muốn xóa từ này không?</p>
                                {/* <p className="text-warning"><small>This action cannot be undone.</small></p> */}
                            </div>
                            <div className="modal-footer">
                                <input type="button" className="btn btn-default" data-dismiss="modal" value="Cancel" />
                                <input type="submit" className="btn btn-danger" data-dismiss="modal" value="Delete" onClick={this.deleteVocab}/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    deleteVocab = () => {
        console.log(this.state.vocab.id);
        callApi('POST', "http://54.95.40.41:3000/api/vocabulary/delete?id=" + this.state.vocab.id, {})
            .then((response) => {
                console.log(response.data);
                alert("Đã xóa!!")
            })
    }

}

export default ModalDeleteVocabulary;