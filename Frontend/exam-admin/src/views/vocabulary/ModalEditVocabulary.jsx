import React from 'react';
import callApi from '../../utils/ApiCaller';

class ModalEditVocabulary extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            lessons: [],
            topics: [],
            vocab : {},
            fileAudio: null,
            fileImage: null
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            lessons: nextProps.lessons,
            topics: nextProps.topics,
            vocab: nextProps.vocab
        })
    }

    showListSelectTopic = (topics) => {
        if (!topics) return null;

        var results = topics.map((topic, index) => {
            return (
                <option key={index}>{topic.topicEn}</option>
            );
        })
        return results;
    }

    showListSelectLesson = (lessons) => {
        if (!lessons) return null;
        
        var results = lessons.map((lesson, index) => {
            return (
                <option key={index}>{lesson.lessonNameEn}</option>
            );
        })
        return results;
    }

    render() {
        var vocab = this.state.vocab;
        if (!vocab) return null;
        return (
            <div id="editVocabModal" className="modal fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h4 className="modal-title">Thêm từ mới</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group d-flex flex-row align-items-center">
                                    <div className="col-sm-4" style={{ marginLeft: -15 }}>
                                        <label>Chọn Topic</label>
                                    </div>
                                    <div className="col-sm-8">
                                        <select className="form-control" style={{ marginLeft: '20px' }}>
                                            {this.showListSelectTopic(this.state.topics)}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group d-flex flex-row align-items-center">
                                    <div className="col-sm-4" style={{ marginLeft: -15 }}>
                                        <label>Chọn Lesson</label>
                                    </div>
                                    <div className="col-sm-8">
                                        <select className="form-control" style={{ marginLeft: '20px' }}>
                                            {this.showListSelectLesson(this.state.lessons)}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Vocabulary English</label>
                                    <input type="text" className="form-control" placeholder={vocab.vocabularyEn} onChange={(e) => this.onChangeVocabularyEn(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Pronunciation</label>
                                    <input type="" className="form-control" placeholder={vocab.pronunciation} onChange={(e) => this.onChangePronunciation(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Vocabulary Type</label>
                                    <input type="text" className="form-control" placeholder={vocab.vocabularyType} onChange={(e) => this.onChangeVocabularyType(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Vocabulary Vietnamese</label>
                                    <input type="text" className="form-control" placeholder={vocab.vocabularyVi} onChange={(e) => this.onChangeVocabularyVi(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Explantion</label>
                                    <textarea type="text" className="form-control" placeholder={vocab.explantion} onChange={(e) => this.onChangeExplantion(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Example English</label>
                                    <textarea type="text" className="form-control" placeholder={vocab.exampleEn} onChange={(e) => this.onChangeExampleEn(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Example Vietnamese</label>
                                    <textarea type="text" className="form-control" placeholder={vocab.exampleVi} onChange={(e) => this.onChangeExampleVi(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <div className="custom-file">
                                        <label className="custom-file-label">
                                            {this.state.fileImage != null ? this.state.fileImage.name : "Chọn ảnh"}
                                        </label>
                                        <input type="file" className="custom-file-input" id="chooseImage" onChange={(e) => this.onChooseImage(e)} required />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="custom-file">
                                        <label className="custom-file-label">
                                            {this.state.fileAudio != null ? this.state.fileAudio.name : "Chọn Audio"}
                                        </label>
                                        <input type="file" className="custom-file-input" id="chooseAudio" onChange={(e) => this.onChooseAudio(e)} required />
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <input type="button" className="btn btn-default" data-dismiss="modal" value="Hủy bỏ" />
                                <input type="submit" className="btn btn-info" data-dismiss="modal" value="Lưu" onClick={this.onUpdateVocab}/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    onChangeVocabularyEn = (vocabularyEn) => {
        this.state.vocab.vocabularyEn = vocabularyEn;
    }
    
    onChangePronunciation = (pronunciation) => {
        this.state.vocab.pronunciation = pronunciation;
    }
    
    onChangeVocabularyType = (vocabularyType) => {
        this.state.vocab.vocabularyType = vocabularyType;
    }
    
    onChangeVocabularyVi = (vocabularyVi) => {
        this.state.vocab.vocabularyVi = vocabularyVi;
    }
    
    onChangeExplantion = (explantion) => {
        this.state.vocab.explantion = explantion;
    }
    
    onChangeExampleEn = (exampleEn) => {
        this.state.vocab.exampleEn = exampleEn;
    }
    
    onChangeExampleVi = (exampleVi) => {
        this.state.vocab.exampleVi = exampleVi;
    }
    
    onChooseAudio = (e) => {
        var file = e.target.files[0];
        this.setState({
            fileAudio: file
        })
    }
    
    onChooseImage = (e) => {
        var file = e.target.files[0];
        this.setState({
            fileImage: file
        })
    }
    
    onUpdateVocab = () => {
        console.log(this.state.vocab);
        callApi("POST", "http://54.95.40.41:3000/api/vocabulary/update", {vocab: this.state.vocab})
            .then((response) => {
                console.log(response.data);
                alert('Đã cập nhập');
            })
    }

}

export default ModalEditVocabulary;