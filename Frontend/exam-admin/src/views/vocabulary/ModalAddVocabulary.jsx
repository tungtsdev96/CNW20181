import React from 'react';
import callApi from '../../utils/ApiCaller'

class ModalAddVocabulary extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            lessons: [],
            vocab : {
                lessonId: 1,
                vocabularyEn: "",
                pronunciation: "",
                vocabularyType: "",
                vocabularyVi: "",
                explantion: "",
                exampleEn: "",
                exampleVi: "",
                imageUrl: "",
                audioUrl: ""
            },
            fileAudio: null,
            fileImage: null
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            lessons: nextProps.lessons
        })
    }

    showListSelectLesson = (lessons) => {
        if (!lessons) return null;
        
        var results = lessons.map((lesson, index) => {
            return (
                <option key={index}>{lesson.lessonNameEn}</option>
            );
        })
        return results;
    }

    render() {
        return (
            <div id="addVocabModal" className="modal fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h4 className="modal-title">Thêm từ mới</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group d-flex flex-row align-items-center">
                                    <div className="col-sm-4" style={{marginLeft: -15}}>
                                        <label>Chọn Lesson</label>
                                    </div>
                                    <div className="col-sm-8">
                                        <select className="form-control" style={{ marginLeft: '20px' }}>
                                            {this.showListSelectLesson(this.state.lessons)}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Vocabulary English</label>
                                    <input type="text" className="form-control" onChange={(e) => this.onChangeVocabularyEn(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Pronunciation</label>
                                    <input type="text" className="form-control" onChange={(e) => this.onChangePronunciation(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Vocabulary Type</label>
                                    <input type="text" className="form-control" onChange={(e) => this.onChangeVocabularyType(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Vocabulary Vietnamese</label>
                                    <input type="text" className="form-control" onChange={(e) => this.onChangeVocabularyVi(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Explantion</label>
                                    <input type="text" className="form-control" onChange={(e) => this.onChangeExplantion(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Example English</label>
                                    <input type="text" className="form-control" onChange={(e) => this.onChangeExampleEn(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Example Vietnamese</label>
                                    <input type="text" className="form-control" onChange={(e) => this.onChangeExampleVi(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <div className="custom-file">
                                        <label className="custom-file-label">{this.state.fileImage != null ? this.state.fileImage.name : "Chọn ảnh"}</label>
                                        <input type="file" className="custom-file-input" id="chooseImage" onChange={(e) => this.onChooseImage(e)} required/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="custom-file">
                                        <label className="custom-file-label">{this.state.fileAudio != null ? this.state.fileAudio.name : "Chọn Audio"}</label>
                                        <input type="file" className="custom-file-input" id="chooseAudio" onChange={(e) => this.onChooseAudio(e)} required/>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <input type="button" className="btn btn-default" data-dismiss="modal" value="Hủy bỏ" />
                                <input type="submit" className="btn btn-success" data-dismiss="modal" value="Thêm" onClick={this.onSubmitAddVocab}/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    onChangeVocabularyEn = (vocabularyEn) => {
        this.state.vocab.vocabularyEn = vocabularyEn;
    }

    onChangePronunciation = (pronunciation) => {
        this.state.vocab.pronunciation = pronunciation;
    }

    onChangeVocabularyType = (vocabularyType) => {
        this.state.vocab.vocabularyType = vocabularyType;
    }

    onChangeVocabularyVi = (vocabularyVi) => {
        this.state.vocab.vocabularyVi = vocabularyVi;
    }

    onChangeExplantion = (explantion) => {
        this.state.vocab.explantion = explantion;
    }

    onChangeExampleEn = (exampleEn) => {
        this.state.vocab.exampleEn = exampleEn;
    }

    onChangeExampleVi = (exampleVi) => {
        this.state.vocab.exampleVi = exampleVi;
    }

    onChooseAudio = (e) => {
        var file = e.target.files[0];
        this.setState({
            fileAudio: file
        })
    }

    onChooseImage = (e) => {
        var file = e.target.files[0];
        this.setState({
            fileImage: file
        })
    }

    onSubmitAddVocab = () => {
        // close modal
        console.log(this.state.vocab);
        callApi("POST", "http://54.95.40.41:3000/api/vocabulary/create", this.state.vocab)
            .then((response) => {
                console.log(response.data);
                alert("Thêm thành công");
            })
    }

}

export default ModalAddVocabulary;