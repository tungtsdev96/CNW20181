import React from 'react';
import callApi from '../../utils/ApiCaller';

class ModalEditLesson extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listTopic: [],
            lesson: {},
            fileImage: null,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            listTopic: nextProps.topics,
            lesson: nextProps.lesson
        })
    }

    showItemTopicSelect = () => {
        var {listTopic} = this.state;
        if (listTopic.length == 0) return null;
        var results = listTopic.map((topic, index) => {
            return (
                <option key={index}>{topic.topicEn}</option>
            )
        })
        return results;
    }


    render() {
        var {lesson} = this.state;
        if (!lesson) return null;
        return (
            <div id="editLessonModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form>
                            <div class="modal-header">
                                <h4 class="modal-title">Thêm bài mới</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div className="form-group d-flex flex-row align-items-center">
                                    <div className="col-sm-4" style={{ marginLeft: -15 }}>
                                        <label>Chọn Topic</label>
                                    </div>
                                    <div className="col-sm-8">
                                        <select className="form-control" style={{ marginLeft: '20px' }}>
                                            {this.showItemTopicSelect()}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Name Lesson English</label>
                                    <input type="text" className="form-control" placeHolder={lesson.lessonNameEn} onChange={(e) => this.onChangeLessonNameEn(e.target.value)}/>
                                </div>
                                <div className="form-group">
                                    <label>Name Lesson VietNamese</label>
                                    <input type="text" className="form-control" placeHolder={lesson.lessonNameEn} onChange={(e) => this.onChangeLessonNameVi(e.target.value)}/>
                                </div>
                                <div className="form-group">
                                    <div className="custom-file">
                                        <label className="custom-file-label">{this.state.fileImage != null ? this.state.fileImage.name : "Chọn ảnh"}</label>
                                        <input type="file" className="custom-file-input" id="chooseImage" onChange={(e) => this.onChooseImage(e)} required/>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <input type="button" className="btn btn-default" data-dismiss="modal" value="Thoát" />
                                <input type="submit" className="btn btn-success" data-dismiss="modal" value="Cập nhật" onClick={this.updateLesson}/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    updateLesson = () => {
        console.log(this.state.lesson);
        callApi('POST', "http://54.95.40.41:3000/api/lesson/update", {lesson: this.state.lesson})
            .then((response) => {
                console.log(response.data);
                alert('Cập nhật thành công');
            })
    }

    onChangeLessonNameEn = (lessonNameEn) => {
        this.state.lesson.lessonNameEn = lessonNameEn;
    }

    onChangeLessonNameVi = (lessonNameVi) => {
        this.state.lesson.lessonNameVi = lessonNameVi;
    }

    onChooseImage = (e) => {
        var file = e.target.files[0];
        this.setState({
            fileImage: file
        })
    }
}

export default ModalEditLesson;