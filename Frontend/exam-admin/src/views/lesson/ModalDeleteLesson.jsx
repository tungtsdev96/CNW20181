import React from 'react';
import callApi from '../../utils/ApiCaller';

class ModalDeleteLesson extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listTopic: [],
            lesson : {
            },
            fileImage: null,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            listTopic: nextProps.topics,
            lesson: nextProps.lesson
        })
    }

    render() {
        return (
            <div id="deleteLessonModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form>
                            <div class="modal-header">
                                <h4 class="modal-title">Xóa bài học</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure you want to delete these Records?</p>
                                {/* <p class="text-warning"><small>This action cannot be undone.</small></p> */}
                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />
                                <input type="submit" class="btn btn-danger" data-dismiss="modal" value="Delete" onClick={this.deleteLesson}/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    deleteLesson = () => {
        console.log(this.state.lesson);
        callApi("POST", "http://54.95.40.41:3000/api/lesson/delete?id=" + this.state.lesson.id, {})
            .then((response) => {
                console.log(response.data);
                alert('Đã xóa')
            })
    }
}

export default ModalDeleteLesson;