import React from 'react';
import callApi from '../../utils/ApiCaller';

class ModalAddLesson extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listTopic: [],
            lesson : {
                topicId: 1,
                lessonNameEn: "",
                lessonNameVi: "",
                imageUrl: "",
                totalVocab: '0'
            },
            fileImage: null,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            listTopic: nextProps.topics
        })
    }

    showItemTopicSelect = () => {
        var { listTopic } = this.state;
        if (listTopic.length == 0) return null;
        var results = listTopic.map((topic, index) => {
            return (
                <option key={index}>{topic.topicEn}</option>
            )
        })
        return results;
    }

    render() {
        return (
            <div id="addLessonModal" className="modal fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form>
                            <div className="modal-header">
                                <h4 className="modal-title">Thêm bài mới</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group d-flex flex-row align-items-center">
                                    <div className="col-sm-4" style={{marginLeft: -15}}>
                                        <label>Chọn Topic</label>
                                    </div>
                                    <div className="col-sm-8">
                                        <select className="form-control" style={{ marginLeft: '20px' }}>
                                            {this.showItemTopicSelect()}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Name Lesson English</label>
                                    <input type="text" className="form-control" onChange={(e) => this.onChangeLessonNameEn(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Name Lesson VietNamese</label>
                                    <input type="text" className="form-control" onChange={(e) => this.onChangeLessonNameVi(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <div className="custom-file">
                                        <label className="custom-file-label">{this.state.fileImage != null ? this.state.fileImage.name : "Chọn ảnh"}</label>
                                        <input type="file" className="custom-file-input" id="chooseImage" onChange={(e) => this.onChooseImage(e)} required/>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <input type="button" className="btn btn-default" data-dismiss="modal" value="Cancel" />
                                <input type="submit" className="btn btn-success" data-dismiss="modal" value="Add" onClick={this.addLesson}/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    addLesson = () => {
        console.log(this.state.lesson);
        callApi('POST', "http://54.95.40.41:3000/api/lesson/create", {lesson: this.state.lesson})
            .then((response) => {
                console.log(response.data);
                alert('Thêm thành công');
            })
    }

    onChangeLessonNameEn = (lessonNameEn) => {
        this.state.lesson.lessonNameEn = lessonNameEn;
    }

    onChangeLessonNameVi = (lessonNameVi) => {
        this.state.lesson.lessonNameVi = lessonNameVi;
    }

    onChooseImage = (e) => {
        var file = e.target.files[0];
        this.setState({
            fileImage: file
        })
    }
}

export default ModalAddLesson;