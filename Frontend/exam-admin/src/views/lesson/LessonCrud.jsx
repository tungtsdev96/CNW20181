import React from 'react';
import ModalAddLesson from './ModalAddLesson';
import ModalDetailLesson from './ModalDetailLesson';
import ModalEditLesson from './ModalEditLesson';
import ModalDeleteLesson from './ModalDeleteLesson';
import callApi from '../../utils/ApiCaller';

class LessonCrud extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listTopic: [],
            listLesson: [],
            listLessonCoppy: [],
            lessonSelected: {}
        }
    }

    componentWillMount() {
        //get all topic 
        callApi('GET', "http://54.95.40.41:3000/api/topic/get-all", {})
            .then((response) => {
                this.setState({
                    listTopic: response.data
                })
            })

        //get all lesson
        callApi('GET', "http://54.95.40.41:3000/api/lesson/get-all", {})
            .then((response) => {
                this.setState({
                    listLesson: response.data,
                    listLessonCoppy: response.data
                })
            })
    }

    showItemTopicSelect = () => {
        var { listTopic } = this.state;
        if (listTopic.length == 0) return null;
        var results = listTopic.map((topic, index) => {
            return (
                <option key={index}>{topic.topicEn}</option>
            )
        })
        return results;
    }

    showListLesonTotable = () => {
        var {listLesson} = this.state;
        if (listLesson.length == 0) return null;
        var results = listLesson.map((lesson, index) => {
            return (
                <tr key={index}>
                    <th scope="row">{lesson.id}</th>
                    <td>{lesson.lessonNameEn}</td>
                    <td>{lesson.lessonNameVi}</td>
                    <td>{lesson.totalVocab}</td>
                    <td>
                        <a href="#detailLessonModal" className="assignment" data-toggle="modal" style={{ color: '#FFC107', padding: 5 }} onClick={() => this.setState({lessonSelected: lesson})}>
                            <i style={this.styleMaterialIcons} data-toggle="tooltip" title="Assignment" data-original-title="Assignment">assignment</i>
                        </a>
                        <a href="#editLessonModal" className="edit" data-toggle="modal" style={{ color: '#FFC107', padding: 5 }} onClick={() => this.setState({lessonSelected: lesson})}>
                            <i style={this.styleMaterialIcons} data-toggle="tooltip" title="Edit" data-original-title="Edit"></i>
                        </a>
                        <a href="#deleteLessonModal" className="delete" data-toggle="modal" style={{ color: '#F44336', padding: 5 }} onClick={() => this.setState({lessonSelected: lesson})}>
                            <i style={this.styleMaterialIcons} data-toggle="tooltip" title="Delete" data-original-title="Delete"></i>
                        </a>
                    </td>
                </tr>
            )
        })
        return results;
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-body">
                            <h2>Lesson Manager</h2>

                            <div className="table-title d-flex flex-row" style={{ marginTop: '20px', textAlign: 'center' }}>
                                <div className="btn-add-lesson">
                                    <button href="#addLessonModal" className="btn btn-success d-flex align-items-center" data-toggle="modal">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Add New Lesson</span>
                                    </button>
                                </div>
                                <div className='btn-del-lesson' style={{ marginLeft: '10px' }}>
                                    <button href="#deleteLessonModal" className="btn btn-danger d-flex align-items-center" data-toggle="modal">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Delete</span>
                                    </button>
                                </div>
                                <div className='btn-import-lesson' style={{ marginLeft: '10px' }}>
                                    <button href="#" className="btn btn-secondary d-flex align-items-center">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Import from Excel</span>
                                    </button>
                                </div>
                                <div className='btn-export-lesson' style={{ marginLeft: '10px' }}>
                                    <button href="#" className="btn btn-secondary d-flex align-items-center">
                                        <i className="material-icons"></i>
                                        <span style={{ marginLeft: '5px' }}>Export to Excel</span>
                                    </button>
                                </div>
                            </div>


                            {/* table filter */}
                            <div className="table-filter" style={this.styleTableFilters}>
                                <div className="row">
                                    <div className="col-md-1">
                                        {/* <div className="d-flex justify-content-start align-items-center"> */}
                                        <div className="d-none justify-content-start align-items-center">
                                            <span>Show</span>
                                            <select className="form-control" style={{ marginLeft: '15px', width: '70px' }}>
                                                <option>5</option>
                                                <option>10</option>
                                                <option>15</option>
                                                <option>20</option>
                                            </select>
                                            <span style={{ marginLeft: '15px' }}>entries</span>
                                        </div>
                                    </div>
                                    <div className="col-md-11">
                                        <div className="d-flex justify-content-end align-items-center">
                                            <span className="filter-icon"><i className="fa fa-filter"></i></span>
                                            <div className="d-flex flex-row align-items-center" style={{ float: 'right', marginLeft: '15px' }}>
                                                <span>Topic</span>
                                                <select className="form-control" style={{ marginLeft: '5px' }} onChange={(e) => this.onChangeTopic(e)}>
                                                    <option>All</option>
                                                    {this.showItemTopicSelect()}
                                                </select>
                                            </div>
                                            <div className="d-flex flex-row align-items-center" style={{ float: 'right', marginLeft: '15px' }}>
                                                <span>Tìm kiếm</span>
                                                <input type="text" className="form-control" style={{ marginLeft: '5px' }} onChange={(e) => this.onChangeQueryText(e.target.value)}/>
                                                <button type="button" className="btn btn-primary" style={{ marginLeft: '5px' }} onClick={() => this.searchLesson()}>
                                                    <i className="fa fa-search" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/* table lesson */}
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">LessonNameEn</th>
                                        <th scope="col">LessonNameVi</th>
                                        <th scope="col">TotalVocab</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.showListLesonTotable()}
                                </tbody>
                            </table>

                            {/* pagination     */}
                            <div className="page d-none">
                                <ul className="pagination">
                                    <li className="page-item">
                                        <a href="#" className="page-link" aria-label="Previous" style={{ border: 'none' }}>
                                            <span aria-hidden="true">Previous</span>
                                            <span className="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <li className="page-item">
                                        <a href="#" className="page-link" style={{ border: 'none' }}>1</a>
                                    </li>
                                    <li className="page-item active">
                                        <a href="#" className="page-link" style={{ border: 'none' }}>2</a>
                                    </li>
                                    <li className="page-item">
                                        <a href="#" className="page-link" style={{ border: 'none' }}>3</a>
                                    </li>
                                    <li className="page-item">
                                        <a href="#" className="page-link" style={{ border: 'none' }}>4</a>
                                    </li>
                                    <li className="page-item">
                                        <a href="#" className="page-link" style={{ border: 'none' }}>5</a>
                                    </li>
                                    <li className="page-item">
                                        <a href="#" className="page-link" aria-label="Next" style={{ border: 'none' }}>
                                            <span aria-hidden="true">Next</span>
                                            <span className="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            {/* <!-- Add Modal HTML --> */}
                            <ModalAddLesson topics={this.state.listTopic}/>

                            {/* <!-- Detail Modal HTML --> */}
                            <ModalDetailLesson topics={this.state.listTopic} lesson={this.state.lessonSelected}/>

                            {/* <!-- Edit Modal HTML --> */}
                            <ModalEditLesson topics={this.state.listTopic} lesson={this.state.lessonSelected}/>

                            {/* <!-- Delete Modal HTML --> */}
                            <ModalDeleteLesson topics={this.state.listTopic} lesson={this.state.lessonSelected}/>

                        </div>
                    </div>
                </div>
            </div>
        );
    }

    onChangeQueryText = (queryText) => {
        var {listLesson, listLessonCoppy} = this.state;
        var lessons = [];
        listLessonCoppy.map((lesson, i) => {
            if (lesson.lessonNameEn.toLowerCase().includes(queryText.toLowerCase())
                || lesson.lessonNameVi.toLowerCase().includes(queryText.toLowerCase())) {
                    lessons.push(lesson);
                }
        })
        this.setState({
            listLesson: lessons
        })
    }

    onChangeTopic = (e) => {
        var topicName = e.target.value;
        var idTopic = 0;
        var listTopic = this.state.listTopic;
        
        for (var i = 0; i < listTopic.length; i++) {
            var topic = listTopic[i];
            if (topicName == topic.topicEn) {
                idTopic = topic.id;
                this.setState({
                    idCurrentTopic: idTopic
                })
                break;
            }
        }

        if (idTopic != 0) {
             callApi('GET', "http://54.95.40.41:3000/api/lesson/get-list-lesson-topic?idTopic=" + idTopic, {})
            .then((response) => {
                this.setState({
                    listLesson: response.data,
                    listLessonCoppy: response.data
                })
            })
        } else {
            callApi('GET', "http://54.95.40.41:3000/api/lesson/get-all", {})
            .then((response) => {
                this.setState({
                    listLesson: response.data,
                    listLessonCoppy: response.data
                })
            })
        }
       
    }

    styleTableFilters = {
        marginTop: '20px',
        padding: '5px 0 20px',
        // borderBottom: '1px solid #e9e9e9',
        marginBottom: '5px'
    }

    styleMaterialIcons = {
        fontFamily: 'Material Icons',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontSize: '24px',
        lineHeight: 1,
        letterSpacing: 'normal',
        textTransform: 'none',
        display: 'inline-block',
        whiteSpace: 'nowrap',
        wordWrap: 'normal',
        direction: 'ltr',
        WebkitFontFeatureSettings: 'liga',
        WebkitFontSmoothing: 'antialiased'
    }
}

export default LessonCrud;