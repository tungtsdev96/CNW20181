import React from 'react';

class ModalDetailLesson extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listTopic: [],
            lesson: {},
            fileImage: null,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            listTopic: nextProps.topics,
            lesson: nextProps.lesson
        })
    }

    showItemTopicSelect = () => {
        var {listTopic} = this.state;
        if (listTopic.length == 0) return null;
        var results = listTopic.map((topic, index) => {
            if (topic.id != this.state.lesson.topicId) return null;
            return (
                <option key={index}>{topic.topicEn}</option>
            )
        })
        return results;
    }

    render() {
        var {lesson} = this.state;
        if (!lesson) return null;
        return (
            <div id="detailLessonModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form>
                            <div class="modal-header">
                                <h4 class="modal-title">Thêm bài mới</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div className="form-group d-flex flex-row align-items-center">
                                    <div className="col-sm-4" style={{ marginLeft: -15 }}>
                                        <label>Chọn Topic</label>
                                    </div>
                                    <div className="col-sm-8">
                                        <select className="form-control" style={{ marginLeft: '20px' }}>
                                            {this.showItemTopicSelect()}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Name Lesson English</label>
                                    <input type="text" className="form-control" value={lesson.lessonNameEn}/>
                                </div>
                                <div className="form-group">
                                    <label>Name Lesson VietNamese</label>
                                    <input type="text" className="form-control" value={lesson.lessonNameEn}/>
                                </div>
                                <div className="form-group">
                                    <label>Số lượng từ vựng</label>
                                    <input type="text" className="form-control" value={lesson.totalVocab}/>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}

export default ModalDetailLesson;