import React from 'react';
import DashBoard from '../views/DashBoard.jsx'
import TestManager from '../views/test/TestManager.jsx'
import CreateNewTest from '../views/test/crud/create/CreateNewTest'
import VocabularyCrud from '../views/vocabulary/VocabularyCrud'
import LessonCrud from '../views/lesson/LessonCrud'
import CourseManager from '../views/course/CourseManager'
import UserManager from '../views/users/UserManager'
import Analytics from '../views/test/crud/ana/Analytics'
import CompledQuizByUser from '../views/test/crud/ana/CompledQuizByUser'
import Login from './../views/common/Login/Login.jsx'
import UserDetail from './../views/users/UserDetail'


var ThemeRoutes = [
  { 
    path: '/', 
    name: 'DashBoard', 
    icon: 'ti-loop',
    exact : true,
    isMenu : false,
    component: DashBoard 
  },
  { 
    path: '/UserDetail', 
    name: 'UserDetail', 
    icon: 'ti-loop',
    exact : false,
    isMenu : false,
    component: UserDetail 
  },
  { 
    path: '/Login', 
    name: 'Login', 
    icon: 'ti-loop',
    exact : true,
    isMenu : false,
    component: Login 
  },
  { 
    path: '/CompledQuizByUser/:user_id', 
    name: 'CompledQuizByUser', 
    icon: 'ti-loop',
    exact : true,
    isMenu : false,
    component:  ({match}) => <CompledQuizByUser match={match} />
  },  
  { 
    path: '/users-manager', 
    name: 'User Manager', 
    icon: 'ti-loop',
    exact : true,
    isMenu : true,
    component: UserManager 
  }, 
  { 
    path: '/test-manager', 
    name: 'Test Manager', 
    exact : true,
    icon: 'ti-book', 
    isMenu : true,
    component: TestManager 
  },
  { 
    path: '/add-new-test', 
    name: 'Create Test', 
    exact : true,
    icon: 'ti-book',
    isMenu : false,
    component: CreateNewTest 
  },
  { 
    path: '/lesson-manager', 
    name: 'Lesson Manager', 
    exact : true,
    icon: 'ti-book',
    isMenu : true,
    component: LessonCrud 
  },
  { 
    path: '/vocab-manager', 
    name: 'Vocabulary Manager', 
    exact : true,
    icon: 'ti-book',
    isMenu : true,
    component: VocabularyCrud 
  },
  { 
    path: '/course-manager', 
    name: 'Course Manager', 
    exact : true,
    icon: 'ti-video-clapper',
    isMenu : true,
    component: CourseManager 
  },
  { path: '/', pathTo: '/test-manager', name: 'Test Manager', redirect: true }
];
export default ThemeRoutes;
