export default {
	apiGateway: {
		REGION: 'YOUR_API_GATEWAY_REGION',
		URL: 'YOUR_API_GATEWAY_URL'
	},
	cognito: {
		REGION: 'ap-northeast-1',
		USER_POOL_ID: 'ap-northeast-1_iRGX3cIF2',
		APP_CLIENT_ID: '7du9vptfaulm8ur6s681ccclik',
		IDENTITY_POOL_ID: 'ap-northeast-1:e54e068c-cb5f-4253-ac74-fc53ad3de16f'
	}
};