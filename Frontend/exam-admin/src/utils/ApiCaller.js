const HOST = "http://54.95.40.41:3000";
const axios = require('axios');

var callApi = (method, endpoint, data) => {
    return axios({
        method : method,
        url : endpoint,
        data : data
    }).catch(err => {
        console.log(err);
    })
}


export default callApi;
