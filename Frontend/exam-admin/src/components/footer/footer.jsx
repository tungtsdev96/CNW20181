import React from 'react';


class Footer extends React.Component {
  render() {
    return (
      <footer className="footer text-center">
        Developed by LinhPH Cloud
      </footer>
    );
  }
}
export default Footer;
