import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import callApi from '../../utils/ApiCaller';
import Header from '../../components/common/Header'
import Footer from '../../components/common/Footer'
import '../../css/DetailCourse.css'

class DetailCourse extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listVideo: [],
            course: {}
        }
    }

    componentWillMount() {
        var idCourse = this.props.match.params.id;
        callApi('GET', "http://localhost:3000/api/course/get-by-id?idCourse=" + idCourse, {})
            .then((response) => {
                var course = response.data;
                callApi('GET', "http://localhost:3000/api/course/get-detail-learn-video?idUser=1&idCourse=" + idCourse, {})
                    .then((response) => {
                        this.setState({
                            listVideo: response.data,
                            course: course[0]
                        });
                    })
            })


    }

    showListVideo = () => {
        var { listVideo } = this.state;
        if (listVideo.length == 0) return null;
        var result = listVideo.map((video, i) => {
            // chua xem #e1e5ea fa fa-play-circle
            // dang xem #ffab20 pause-circle
            // complete #464646 fa fa-check-circle
            var status = {}

            // dang hoc
            if (video.state == 0) {
                status.style = '#ffab20';
                status.class = 'fa fa-pause-circle';
            } else if (video.state == 1) {
                status.style = '#464646';
                status.class = 'fa fa-check-circle';
            } else {
                status.style = '#e1e5ea';
                status.class = 'fa fa-play-circle';
            }
            return (
                <Link to={"/course/learning/" + video.courseId + "/" + video.id} key={i}>
                    <div className="d-flex justify-content-center">
                        <div className="col-9">
                            <div className="infor-video d-flex flex-row align-items-center">
                                <div className="col-md-9">
                                    <div className="d-flex flex-row align-items-center">
                                        <div className="status-video" style={{ color: status.style }}>
                                            <i className={status.class}></i>
                                        </div>
                                        <div>
                                            <div className="title-video">
                                                <h6>{"Bài " + (i + 1)}</h6>
                                                {video.titleVideo}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="lesson-time">{this.fancyTimeFormat(video.lengthVideo)}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Link>
            )
        })
        return result;
    }

    fancyTimeFormat = (time) => {
        // Hours, minutes and seconds
        var hrs = ~~(time / 3600);
        var mins = ~~((time % 3600) / 60);
        var secs = ~~time % 60;

        // Output like "1:01" or "4:03:59" or "123:03:59"
        var ret = "";

        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }

        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
    }

    render() {
        var course = this.state.course;
        return (
            <div>
                <Header />
                <div className="cover-course">
                    <div className="course-learning-info">
                        <div className="info-progress" data-percent="23">
                            <svg viewBox="0 0 100 100" style={{ display: 'block', width: '100%' }}>
                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="rgba(255,122,32,0.3)" strokeWidth="6" fillOpacity="0"></path><path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="rgb(255,122,32)" strokeWidth="6" fillOpacity="0" style={{ strokeDasharray: '295.416, 295.416', strokeDashoffset: '227.47' }}></path>
                            </svg>
                            <div className="progressbar-text">
                                23%
                            </div>
                        </div>
                        <input id="progress-course" type="hidden" value="23" />
                    </div>
                </div>

                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="detail-course-name text-center">
                                <h1 class="title-course">{course.nameCourse}</h1>
                                <p>{course.description}</p>
                            </div>
                        </div>
                    </div>

                    {this.showListVideo()}
                </div>

                <Footer />
            </div>
        )
    }

}

export default DetailCourse;