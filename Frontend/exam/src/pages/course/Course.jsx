import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import callApi from '../../utils/ApiCaller';
import '../../css/Course.css'
import HeaderInContent from '../../components/common/HeaderInContent'
import Footer from '../../components/common/Footer'

class Course extends Component {

    constructor(props) {
        super(props);
        this.state = {
            popularCourse: [],
            listcourseToShow: [],
            tabActive: 2
        }
    }

    componentWillMount() {
        // get popular couse
        callApi("GET", "http://localhost:3000/api/course/get-popular-course", {})
            .then((response) => {
                this.setState({
                    popularCourse: response.data
                })
            })

        // get course by type
        callApi("GET", "http://localhost:3000/api/course/get-by-type?type=5", {})
            .then((response) => {
                this.setState({
                    listcourseToShow: response.data
                })
            })

    }

    showPopularCourse() {
        var { popularCourse } = this.state;
        if (popularCourse.length == 0) return null;
        return this.showListCourse(popularCourse);
    }

    showListToeicCourse() {
        var { listcourseToShow } = this.state;
        if (listcourseToShow.length == 0) return null;
        return this.showListCourse(listcourseToShow);
    }

    showListCourse(listcourse) {
        var results = listcourse.map((course, i) => {
            return (
                // {/* <!-- Single Course --> */}
                <div className="col-12 col-md-6 col-lg-3" key={{ i }}>
                    <Link to={"/course/detail-course/" + course.id}>
                        <div className="single-popular-course">

                            <img src="https://d1nzpkv5wwh1xf.cloudfront.net/320/k-57b67684047c99584fc4a66e/20170605-/slide1.png" alt="" />

                            {/* <!-- Course Content --> */}
                            <div className="course-content">
                                <h4>{course.nameCourse}</h4>
                                <div className="meta d-flex align-items-center">
                                    <a>{course.nameTeacher}</a>
                                    {/* <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="https://colorlib.com/preview/theme/clever/?fbclid=IwAR2a8nOn-579AwAj5Us_klTyfGLsKrMN-6oB0ONYlvSX8AQswfODVre0ups#">Art &amp; Design</a> */}
                                </div>
                                {/* <p>{course.description}</p> */}
                            </div>

                            {/* <!-- Seat Rating Fee --> */}
                            <div class="seat-rating-fee d-flex justify-content-between">
                                <div class="seat-rating h-100 d-flex align-items-center">
                                    <div class="seat">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        {(course.numberUserLearn == null) ? "0" : course.numberUserLearn}
                                    </div>
                                    {/* <div class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    4.5
                                </div> */}
                                </div>
                                <div class="course-fee h-100">
                                    <a className={(course.price == 0) ? "free" : ""}>{(course.price == 0) ? "Free" : "$" + course.price}</a>
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
            )
        })
        return results;
    }

    render() {
        return (
            <div>
                {/* header */}
                <HeaderInContent />

                {/* listcourse popular-courses*/}
                <section className="popular-courses-area">
                    <div className="container" >
                        {/* heading */}
                        <div className="row">
                            <div className="col-12">
                                <div className="section-heading">
                                    <h3>Khóa học được nhiều người học nhất</h3>
                                </div>
                            </div>
                        </div>

                        {/* course */}
                        <div className="row">
                            {this.showPopularCourse()}
                        </div>

                    </div>
                </section>

                {/* listcourse popular-courses*/}
                <section className="popular-courses-area">
                    <div className="container" >
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class={(this.state.tabActive == 2) ? "nav-link active" : "nav-link"} onClick={() => this.reloadListcourseByTab(2)}>Tất cả</a>
                            </li>
                            <li class="nav-item">
                                <a class={(this.state.tabActive == 0) ? "nav-link active" : "nav-link"} onClick={() => this.reloadListcourseByTab(0)}>Miễn phí</a>
                            </li>
                            <li class="nav-item">
                                <a class={(this.state.tabActive == 1) ? "nav-link active" : "nav-link"} onClick={() => this.reloadListcourseByTab(1)}>Trả phí</a>
                            </li>
                        </ul>
                        {/* heading */}
                        <div className="row">
                            <div className="col-12">
                                <div className="section-heading">
                                    <h3>Các khóa học toeic</h3>
                                </div>
                            </div>
                        </div>

                        {/* course */}
                        <div className="row">
                            {this.showListToeicCourse()}
                        </div>

                    </div>
                </section>

                {/* footer */}
                <Footer />
            </div>
        )
    }

    reloadListcourseByTab(type) {
        // get course by type
        callApi("GET", "http://localhost:3000/api/course/get-by-type?type=" + type, {})
            .then((response) => {
                this.setState({
                    listcourseToShow: response.data,
                    tabActive: type
                })
            })
    }

    styleItemCourse = {
        cursor: 'pointer',
        borderBottom: 0,
        boxShadow: '0 1px 10px 0 rgba(0, 0, 0, 0.09)',
        transition: 'all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1)'
    }

}

export default Course;