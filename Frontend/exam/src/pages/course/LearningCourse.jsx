import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import callApi from '../../utils/ApiCaller';
import '../../css/DetailCourse.css'
import { Player, ControlBar, ReplayControl, ForwardControl, BigPlayButton, LoadingSpinner } from 'video-react';

class LearningCourse extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listVideo: [],
            course: null,
            currentIdVideo: -1
        }
    }

    componentWillMount() {
        var { idCourse, idVideo } = this.props.match.params;
        callApi('GET', "http://localhost:3000/api/course/get-detail-learn-course?idCourse=" + idCourse + "&idUser=1")
            .then((response) => {
                this.setState({
                    listVideo: response.data.videos,
                    course: response.data.course,
                    currentIdVideo: idVideo
                })
            })
    }

    componentDidMount() {
        // subscribe state change
        this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
        window.addEventListener("resize", this.updateWindowDimensions);
    }

    updateWindowDimensions = () => {
        this.setState({
            widthWindow: window.innerWidth,
            heightWindow: window.innerHeight,
        });
        // console.log(this.state.widthWindow, this.state.heightWindow);
    };

    handleStateChange(state, prevState) {
        // copy player state to this component's state
        this.setState({
            player: state
        });

    }

    showListVideo = () => {
        var { idCourse, idVideo } = this.props.match.params;
        var { listVideo } = this.state;
        if (listVideo.length == 0) return null;
        var results = listVideo.map((video, i) => {

            var status = {}

            // dang hoc
            if (video.state == 0) {
                status.style = '#ffab20';
                status.class = 'fa fa-pause-circle';
            } else if (video.state == 1) {
                status.style = '#464646';
                status.class = 'fa fa-check-circle';
            } else {
                status.style = '#e1e5ea';
                status.class = 'fa fa-play-circle';
            }

            return (
                <Link to={"/course/learning/" + idCourse + "/" + (parseInt(video.id))}>

                    <div className="row item-video-learn" >
                        <div className="col-12">
                            <div className="row d-flex flex-row align-items-center">
                                <div className="col-md-9">
                                    <div className="d-flex flex-row align-items-center">
                                        <div className="status-video" style={{ color: status.style, fontSize: "24px" }}>
                                            <i class={status.class}></i>
                                        </div>
                                        <div className="title-video" style={{ paddingLeft: '10px' }}>
                                            <h6 style={{ marginBottom: '2px' }}>{"Bài " + (i + 1)}</h6>
                                            {video.titleVideo}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div class="lesson-time">{this.fancyTimeFormat(500)}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Link>
            )
        })
        return results;
    }

    showButtonControl = () => {
        var { idCourse, idVideo } = this.props.match.params;
        var linkTo = "";
        var videos = this.state.listVideo;
        if (videos.length == 0) return null;

        console.log(idVideo, (parseInt(idVideo) + 1),videos[0].id);


        if (idVideo == videos[0].id) {
            linkTo = "/course/learning/" + idCourse + "/" + (parseInt(idVideo) + 1);
            console.log(linkTo);
            return (
                <div className="d-flex flex-row align-items-center" style={{ paddingTop: '15px' }}>
                    {this.getButtonControlBack()}
                    <Link to={linkTo}>
                        {this.getButtonControlForward()}
                    </Link>
                </div>
                
            )
        } else if (idVideo == videos[videos.length - 1].id) {
            linkTo = "/course/learning/" + idCourse + "/" + (parseInt(idVideo) - 1);
            console.log(linkTo);
            return (
                <div className="d-flex flex-row align-items-center" style={{ paddingTop: '15px' }}>
                    <Link to={linkTo + parseInt(idVideo - 1)}>
                        {this.getButtonControlBack()}                        
                    </Link>
                    {this.getButtonControlForward()}
                </div>
            )
        } else {
            var linkToNext ="/course/learning/" + idCourse + "/" + (parseInt(idVideo) + 1);
            var linkToBack ="/course/learning/" + idCourse + "/" + (parseInt(idVideo) - 1);
            return (
                <div className="d-flex flex-row align-items-center" style={{ paddingTop: '15px' }}>
                    <Link to={linkToBack}>
                        {this.getButtonControlBack()}                        
                    </Link>
                    <Link to={linkToNext}>
                        {this.getButtonControlForward()}
                    </Link>
                </div>
            )
        }
    }

    getButtonControlForward = () => {
        return (
            <div className="btn-next d-flex flex-row align-items-center">
                    <span style={{ color: '#FFF' }}> Bài tiếp </span>
                    <div className="icon d-flex align-items-center justify-content-center" style={{ marginLeft: '6px' }}>
                        <i class="fa fa-step-forward" ></i>
                    </div>

                </div>
            
        )
    }

    getButtonControlBack = () => {
        return(
            <div className="btn-pre d-flex flex-row align-items-center">
                    <div className="icon d-flex align-items-center justify-content-center">
                        <i class="fa fa-step-backward"></i>
                    </div>
                    <span style={{ color: '#FFF' }}> Bài trước </span>
                </div>
                
        )
    }

    render() {


        return (
            <div className="row m-0 learning-content">

                {/* video  */}
                <div className="col-xs-12 col-sm-7 col-md-8 col-lg-9 lecture-player">
                    <div class="row m-0 lecture-player-header">
                        <div className="d-flex flex-row align-items-center" style={{ cursor: 'pointer' }}>
                            <div class="lecture-back" style={{ color: '#fff', fontSize: '24px' }}>
                                <i class="fa fa-arrow-circle-o-left"></i>
                                <span style={{ marginLeft: '15px', fontSize: '16px' }}><b>Quay lại</b></span>
                            </div>
                        </div>

                    </div>

                    <Player
                        ref="player"
                        src={"http://localhost:3000/public/videos/courses/" + this.state.currentIdVideo + ".mp4"}
                    >
                        <BigPlayButton position="center" />
                        <LoadingSpinner />
                        <ControlBar autoHide={false}>
                            <ReplayControl seconds={5} order={2.1} />
                            <ForwardControl seconds={5} order={3.1} />
                        </ControlBar>

                    </Player>

                    {this.showButtonControl()}

                </div>

                {/* list video */}
                <div className="col-xs-0 col-sm-5 col-md-4 col-lg-3 lecture-container">
                    {this.showListVideo()}
                </div>

            </div>
        )
    }

    fancyTimeFormat = (time) => {
        // Hours, minutes and seconds
        var hrs = ~~(time / 3600);
        var mins = ~~((time % 3600) / 60);
        var secs = ~~time % 60;

        // Output like "1:01" or "4:03:59" or "123:03:59"
        var ret = "";

        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }

        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
    }

}

export default LearningCourse;