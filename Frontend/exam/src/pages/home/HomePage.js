import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import axios from 'axios'
import './../../css/StyleHome.css'
import Header from '../../components/common/Header'
import Footer from '../../components/common/Footer'
import srcBgAbout from './../../assets/images/about.png'
import srcBgImage1 from './../../assets/images/bg1.jpeg'
import srcBgImage2 from './../../assets/images/bg2.jpeg'
import srcCourse from './../../assets/images/course01.jpg'
import srcAboutVideo from './../../assets/images/about-video.jpg'

class HomePage extends Component {
    constructor(props) {
        super(props);
        // axios.get("http://54.95.40.41:3000/private", {}).then(res => {
        //     console.log(res)
        // })
    }

    render() {

        return (
            <div>
                <Header/>
                <div id="home" className="hero-area">
                    {/* <!-- Backgound Image --> */}
                    <div className="bg-image bg-parallax overlay"
                        style={
                            {
                                backgroundImage: "url('" + srcBgImage1 + "')"
                            }
                        }>
                    </div>
                    {/* <!-- /Backgound Image --> */}
                    <div className="home-wrapper">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-8">
                                    <h1 className="white-text">Học toeic online mọi lúc mọi nơi</h1>
                                    <p className="lead white-text">Mô hình theo chuẩn quốc tế,cam kết 650+</p>
                                    <p className="lead white-text">Phù hợp cho sinh viên, học sinh bắt đầu từ con số 0</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <!-- /Home --> */}
                {/* <!-- About --> */}
                <div id="about" className="section">
                    {/* <!-- container --> */}
                    <div className="container">
                        {/* <!-- row --> */}
                        <div className="row">
                            <div className="col-md-6">
                                <div className="section-header">
                                    <h2>Vì sao bạn cần học Toeic online</h2>
                                </div>
                                {/* <!-- feature --> */}
                                <div className="feature">
                                    <i className="feature-icon fa fa-question"></i>
                                    <div className="feature-content">
                                        <h3>Mất nền tảng toeic nhưng cần tiến bộ trong vòng 2 tháng</h3>
                                        <p>Tự học và tự học mỗi ngày chính là cách học tiến bộ nhanh và duy nhất lấy lại 
                                            kiến thức nền tảng một cách thực chất và bền vững!
                                            Nền tảng tự học toeic chỉ cần sử dụng máy tính bảng, điện thoại,.. rất dễ dàng, tiện lợi, khoa học.
                                        </p>
                                    </div>
                                </div>
                                {/* <!-- /feature --> */}
                                {/* <!-- feature --> */}
                                <div className="feature">
                                    <i className="feature-icon fa fa-clock-o"></i>
                                    <div className="feature-content">
                                        <h3>Bạn thiếu thốn thời gian và không tiến bộ khi đến lớp học </h3>
                                        <p>Thời gian ngắn ngủi và không tập trung tại lớp học chính là nguyên nhân lớn nhất của việc không tiến bộ
                                            Trang Web giúp bạn giải quyết hoàn toàn vấn đề này bằng cách tận dụng triệt để thời gian mọi thời gian 
                                            rảnh rỗi cho việc học tiếng anh.
                                        </p>
                                    </div>
                                </div>
                                {/* <!-- /feature --> */}
                                {/* <!-- feature --> */}
                                <div className="feature">
                                    <i className="feature-icon fa fa-graduation-cap"></i>
                                    <div className="feature-content">
                                        <h3>Cần sử dụng tiếng anh trong công việc học tập </h3>
                                        <p>Mong muốn này rất khó được đáp ứng khi mà bạn chưa thông thạo được tiếng anh. Tuy nhiên, trang Web
                                            đã hỗ trợ bằng các khóa học qua video trực tiếp từ đơn giản tới nâng cao giúp các bạn dễ dàng nâng
                                             cao khả năng nghe và ngữ pháp, từ vựng toeic </p>
                                    </div>
                                </div>
                                {/* <!-- /feature --> */}
                            </div>
                            <div className="col-md-6">
                                <div className="about-img">
                                    <img src={srcBgAbout} alt="" />
                                </div>
                            </div>
                        </div>
                        {/* <!-- row --> */}
                    </div>
                    {/* <!-- container --> */}
                </div>

                {/* <!--Thêm phần này--> */}
                <div id="" className="section">
                    <div className="container">
                        <div className="content-page">
                            {/* <!--Bai viet moi nhat, tieu de, noi dung ow tren anh Background--> */}
                            {/* <!--Danh sach cac bai viet khac--> */}
                            <div className="list-posts">
                                <div className="row">
                                    <div className="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                        <div className="post-image view overlay rounded z-depth-2 mb-lg-0 mb-4">
                                            <img className="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/img%20(27).jpg" alt="Sample image" />
                                            <a>
                                                <div className="mask rgba-white-slight waves-effect waves-light"></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                        <h3 className="font-weight-bold mb-3"><strong>Các bài thi thử</strong></h3>
                                        <p>
                                           Hàng chục bài thi thử Toeic giúp kiểm tra kiến thức liên tục và có hệ thống
                                        </p>
                                        <p>Được trích rút đề từ nhiều tài liệu tiếng anh chất lượng</p>
                                        
                                        <Link to="/TestList">
                                            <a class="btn btn-success btn-md waves-effect waves-light">
                                                Tìm hiểu thêm
                                            </a>
                                        </Link>
                                        
                                    </div>
                                </div>
                                <hr className="my-5" />
                                <div className="row">

                                    {/* <!-- Grid column --> */}
                                    <div className="col-lg-7">
                                        {/* <!-- Post title --> */}
                                        <h3 className="font-weight-bold mb-3"><strong>1000 từ vựng theo các chủ đề</strong></h3>
                                        {/* <!-- Excerpt --> */}
                                        <p>
                                            100 bài học với từ vựng theo từng chủ để nhất định cùng âm thanh và nghĩa từ giúp cho học viên dễ dàng
                                            hiểu và tiếp thu, ghi nhớ từ mới hiệu quả.
                                        </p>
                                        {/* <!-- Post data --> */}
                                        <p>Trích xuất từ nhiều tài liệu tiếng anh chất lượng</p>
                                        {/* <!-- Read more button --> */}
                                        <Link to ="/ListTopic">
                                            <a class="btn btn-indigo btn-md waves-effect waves-light">
                                                Tìm hiểu thêm
                                            </a>
                                        </Link>
                                       

                                    </div>
                                    {/* <!-- Grid column --> */}
                                    {/* <!-- Grid column --> */}
                                    <div className="col-lg-5">

                                        {/* <!-- Featured image --> */}
                                        <div className="view overlay rounded z-depth-2">
                                            <img className="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/img%20(34).jpg" alt="Sample image" />
                                            <a>
                                                <div className="mask rgba-white-slight waves-effect waves-light"></div>
                                            </a>
                                        </div>

                                    </div>
                                    {/* <!-- Grid column --> */}

                                </div>
                                <hr className="my-5" />
                                <div className="row">

                                    {/* <!-- Grid column --> */}
                                    <div className="col-lg-5">

                                        {/* <!-- Featured image --> */}
                                        <div className="view overlay rounded z-depth-2 mb-lg-0 mb-4">
                                            <img className="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/img (28).jpg" alt="Sample image" />
                                            <a>
                                                <div className="mask rgba-white-slight waves-effect waves-light"></div>
                                            </a>
                                        </div>

                                    </div>
                                    {/* <!-- Grid column --> */}
                                    {/* <!-- Grid column --> */}
                                    <div className="col-lg-7">
                                        {/* <!-- Post title --> */}
                                        <h3 className="font-weight-bold mb-3"><strong>Các khóa học hàng đầu</strong></h3>
                                        {/* <!-- Excerpt --> */}
                                        <p>
                                           Hình thức học Toeic qua hàng loạt các video khóa học từ cơ bản đến nâng cao
                                           dễ dàng tạo sử hứng khởi cho học viên để tiếp thu kiến thức 1 cách nhanh chóng
                                        </p>
                                        {/* <!-- Post data --> */}
                                        <p>Trích xuất từ nhiều tài liệu  video tiếng anh chất lượng</p>
                                        {/* <!-- Read more button --> */}
                                        <Link to ="/Course">
                                            <a class="btn btn-pink btn-md waves-effect waves-light">
                                                Tìm hiểu thêm
                                            </a>
                                        </Link>
                                       
                                    </div>
                                    {/* <!-- Grid column --> */}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <!-- /About --> */}
                {/* <!-- Courses --> */}
                <div id="courses" className="section">
                    {/* <!-- container --> */}
                    <div className="container">
                        {/* <!-- row --> */}
                        <div className="row">
                            <div className="section-header text-center w-100">
                                <h2>Những bài học được học nhiều nhất trên trang WEB</h2>
                            </div>
                        </div>
                        {/* <!-- /row --> */}
                        {/* <!-- courses --> */}
                        <div id="courses-wrapper">
                            {/* <!-- row --> */}
                            <div className="row">
                                {/* <!-- single course --> */}
                                <div className="col-md-3 col-sm-6 col-xs-6">
                                    <div className="course">
                                        <a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="course-img">
                                            <img src={srcCourse} alt="" />
                                            <i className="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a className="course-title" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">Các thì cơ bản trong toe</a>
                                        <div className="course-details">
                                            <span className="course-category">Business</span>
                                            <span className="course-price course-free">Free</span>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- /single course --> */}
                                {/* <!-- single course --> */}
                                <div className="col-md-3 col-sm-6 col-xs-6">
                                    <div className="course">
                                        <a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="course-img">
                                            <img src={srcCourse} alt="" />
                                            <i className="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a className="course-title" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">Introduction to CSS </a>
                                        <div className="course-details">
                                            <span className="course-category">Web Design</span>
                                            <span className="course-price course-premium">Premium</span>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- /single course --> */}
                                {/* <!-- single course --> */}
                                <div className="col-md-3 col-sm-6 col-xs-6">
                                    <div className="course">
                                        <a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="course-img">
                                            <img src={srcCourse} alt="" />
                                            <i className="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a className="course-title" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">The Ultimate Drawing Course | From Beginner To Advanced</a>
                                        <div className="course-details">
                                            <span className="course-category">Drawing</span>
                                            <span className="course-price course-premium">Premium</span>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- /single course --> */}
                                <div className="col-md-3 col-sm-6 col-xs-6">
                                    <div className="course">
                                        <a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="course-img">
                                            <img src={srcCourse} alt="" />
                                            <i className="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a className="course-title" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">The Complete Web Development Course</a>
                                        <div className="course-details">
                                            <span className="course-category">Web Development</span>
                                            <span className="course-price course-free">Free</span>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- /single course --> */}
                            </div>
                            {/* <!-- /row --> */}
                            {/* <!-- row --> */}
                            <div className="row">
                                {/* <!-- single course --> */}
                                <div className="col-md-3 col-sm-6 col-xs-6">
                                    <div className="course">
                                        <a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="course-img">
                                            <img src={srcCourse} alt="" />
                                            <i className="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a className="course-title" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">PHP Tips, Tricks, and Techniques</a>
                                        <div className="course-details">
                                            <span className="course-category">Web Development</span>
                                            <span className="course-price course-free">Free</span>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- /single course --> */}
                                {/* <!-- single course --> */}
                                <div className="col-md-3 col-sm-6 col-xs-6">
                                    <div className="course">
                                        <a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="course-img">
                                            <img src={srcCourse} alt="" />
                                            <i className="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a className="course-title" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">All You Need To Know About Web Design</a>
                                        <div className="course-details">
                                            <span className="course-category">Web Design</span>
                                            <span className="course-price course-free">Free</span>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- /single course --> */}
                                {/* <!-- single course --> */}
                                <div className="col-md-3 col-sm-6 col-xs-6">
                                    <div className="course">
                                        <a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="course-img">
                                            <img src={srcCourse} alt="" />
                                            <i className="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a className="course-title" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">How to Get Started in Photography</a>
                                        <div className="course-details">
                                            <span className="course-category">Photography</span>
                                            <span className="course-price course-free">Free</span>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- /single course --> */}
                                {/* <!-- single course --> */}
                                <div className="col-md-3 col-sm-6 col-xs-6">
                                    <div className="course">
                                        <a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="course-img">
                                            <img src={srcCourse} alt="" />
                                            <i className="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a className="course-title" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">Typography From A to Z</a>
                                        <div className="course-details">
                                            <span className="course-category">Typography</span>
                                            <span className="course-price course-free">Free</span>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- /single course --> */}
                            </div>
                            {/* <!-- /row --> */}
                        </div>
                        {/* <!-- /courses --> */}
                        <div className="row">
                            <div className="center-btn" style={{ margin: "40px auto 0px auto" }}>
                                <a className="main-button icon-button" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">More Courses</a>
                            </div>
                        </div>
                    </div>
                    {/* <!-- container --> */}
                </div>

                {/* <!-- /Courses --> */}
                {/* <!-- Call To Action --> */}
                <div id="cta" className="section">
                    {/* <!-- Backgound Image --> */}
                    <div className="bg-image bg-parallax overlay" style={{ backgroundImage: 'url(' + srcBgImage2 + ')' }}></div>
                    {/* <!-- /Backgound Image --> */}
                    {/* <!-- container --> */}
                    <div className="container">
                        {/* <!-- row --> */}
                        <div className="row">
                            <div className="col-md-6">
                                <h2 className="white-text">Ceteros fuisset mei no, soleat epicurei adipiscing ne vis.</h2>
                                <p className="lead white-text">Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.</p>
                                <a className="main-button icon-button" href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">Get Started!</a>
                            </div>
                        </div>
                        {/* <!-- /row --> */}
                    </div>
                    {/* <!-- /container --> */}
                </div>

                {/* <!-- /Call To Action --> */}
                {/* <!-- Why us --> */}
                <div id="why-us" className="section">
                    {/* <!-- container --> */}
                    <div className="container">
                        {/* <!-- row --> */}
                        <div className="row">
                            <div className="section-header text-center w-100">
                                <h2>Các bước để trở thành học viên</h2>
                            </div>
                            {/* <!-- feature --> */}
                            <div className="col-md-4">
                                <div className="feature">
                                    <i className="feature-icon fa fa-flask"></i>
                                    <div className="feature-content">
                                        <h4>Bước 1</h4>
                                        <p>Để lại thông tin liên lạc để được chăm sóc, tư vấn</p>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- /feature --> */}
                            {/* <!-- feature --> */}
                            <div className="col-md-4">
                                <div className="feature">
                                    <i className="feature-icon fa fa-users"></i>
                                    <div className="feature-content">
                                        <h4>Bước 2</h4>
                                        <p>Kiểm tra trình độ toeic và nhận lộ trình học</p>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- /feature --> */}
                            {/* <!-- feature --> */}
                            <div className="col-md-4">
                                <div className="feature">
                                    <i className="feature-icon fa fa-comments"></i>
                                    <div className="feature-content">
                                        <h4>Bước 3</h4>
                                        <p>Hoàn tất thủ tục và bắt đầu sử dụng học toeic online trên trang WEB</p>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- /feature --> */}
                        </div>
                        {/* <!-- /row --> */}
                        <hr className="section-hr" />
                        {/* <!-- row --> */}
                        <div className="row">
                            <div className="col-md-6">
                                <h3>Video Test khả năng nghe video của học viên</h3>
                                <p className="lead">Những từ ngữ trong video rất cơ bản dành cho người mới bắt đầu học từ vựng</p>
                                <p>Hãy cố gắng lên các bạn nhé</p>
                            </div>
                            <div className="col-md-5 col-md-offset-1">
                                {/* <a className="about-video"> */}
                                <a>
                                    <iframe width="100%" height="300px" src="https://www.youtube.com/embed/tgbNymZ7vqY">
                                    </iframe>
                                    {/* <img src={srcAboutVideo} alt="" />
                                    <i className="play-icon fa fa-play"></i> */}
                                </a>
                            </div>
                        </div>
                        {/* <!-- /row --> */}
                    </div>
                    {/* <!-- /container --> */}
                </div>

                {/* <!-- /Why us --> */}
                {/* <!-- Contact CTA --> */}
                <div id="contact-cta" className="section">
                    {/* <!-- Backgound Image --> */}
                    <div className="bg-image bg-parallax overlay" style={{backgroundImage: 'url(' + srcBgImage2 + ')'}}>
                        
                    </div>
                    {/* <!-- Backgound Image --> */}
                    {/* <!-- container --> */}
                    <div className="container">
                        {/* <!-- row --> */}
                        <div className="row">
                            <div className="col-md-8 col-md-offset-2 text-center">
                                <h2 className="white-text">Liên hệ với chúng tôi</h2>
                                <p className="lead white-text">HotLine: 0963942930</p>
                                <p className="lead white-text">Trụ sở chính: Đại học Bách Khoa Hà Nội</p>
                                <div className="main-button icon-button" style={{cursor: 'pointer'}}>
                                    Liên hệ ngay
                                </div>
                            </div>
                        </div>
                        {/* <!-- /row --> */}
                    </div>
                    {/* <!-- /container --> */}
                </div>
                
                <Footer/>
            </div>
        )

    }

}

export default HomePage;
