import React, { Component } from 'react';
import axios from 'axios';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'
import Header from '../../components/common/HeaderInContent'
import Footer from '../../components/common/Footer'

class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password :"",
            re_password : "",
            name : "",
            address : "",
            year_of_birth : "",
            email : "",
            phone_number : "",

        }
    }

    showAlert = (title, message, action) => {
        const options = {
            title: title,
            message: message,
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => { }
                }
            ],
        }
        confirmAlert(options)
    }

    onSubmitSignUp = (e) => {
        e.preventDefault();
        if(this.state.username == "" || this.state.password == "" ||this.state.name == "" ||this.state.address == "" || this.state.email == "" || this.state.phone_number == "" || this.state.year_of_birth == ""){
            this.showAlert("Thiếu thông tin", "Bạn cần nhập nhập đầy đủ thông tin yêu cầu");
            return;
        }
        if(this.state.password !== this.state.re_password){
            this.showAlert("Thông tin không chính xác!", "Bạn cần nhập lại mật khẩu đúng với mật khẩu ban đầu!");
            return;
        }


        axios.get("http://54.95.40.41:3000/check-username?username="+this.state.username, {}).then(res => {
            if(res.data.existed == 1){
                this.showAlert("Tài khoản đã tồn tại", "Tài khoản của bạn đã bị trùng, vui lòng chọn tài khoản khác");
            }else{
                this.onSignUp();
            }
        }).catch(err => {
            this.showAlert("Lỗi máy chủ", "Máy chủ tạm thời đang lỗi, vui lòng thử lại trong ít phút");
        });
    }

    onSignUp = () => {
        axios.post("http://54.95.40.41:3000/signup", this.state   ).then(res => {
            this.showAlert("Thành công", "Bạn đã đăng kí thành công, vui lòng quay lại trang đăng nhập");
        }).catch (err => {
            this.showAlert("Lỗi máy chủ", "Máy chủ tạm thời đang lỗi, vui lòng thử lại trong ít phút");
        })
    }

    onChangeInput = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name] : value
        });        
    }

    render() {
        return (
            <div>
                <Header/>
                <div className="row" style={{marginTop: '100px'}}>
                    <div className="col-md-3 col-sm-3">

                    </div>
                    <div className="col-md-6 col-sm-6">
                        <form onSubmit={this.onSubmitSignUp}>
                            <div className="form-group">
                                <label htmlFor="email">Tài khoản</label>
                                <input type="text" className="form-control" id="username" name="username" onChange={this.onChangeInput}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="pwd">Mật khẩu</label>
                                <input type="password" className="form-control" id="password" name="password" onChange={this.onChangeInput}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="pwd">Nhập lại mật khẩu</label>
                                <input type="password" className="form-control" id="re_password" name="re_password" onChange={this.onChangeInput}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="name">Họ và tên</label>
                                <input type="text" className="form-control" id="name" name="name" onChange={this.onChangeInput}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="address">Địa chỉ</label>
                                <input type="text" className="form-control" id="address" name="address" onChange={this.onChangeInput}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="year_of_birth">Năm sinh</label>
                                <input type="number" className="form-control" id="year_of_birth" name="year_of_birth" onChange={this.onChangeInput}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input type="email" className="form-control" id="email" name="email" onChange={this.onChangeInput}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="phone_number">Số điện thoại</label>
                                <input type="number" className="form-control" id="phone_number" name="phone_number" onChange={this.onChangeInput}/>
                            </div>
                            <button type="submit" className="btn btn-info">Đăng kí</button>
                        </form>
                    </div>
                    <div className="col-md-3 col-sm-3">

                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default SignUp;
