import React, { Component } from 'react';
import axios from 'axios'
import {Redirect} from 'react-router-dom'
import Header from '../../components/common/HeaderInContent'
import Footer from '../../components/common/Footer'

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password :"",
            currentUser : null
        }
    }

    componentWillMount(){
        this.setState({
            currentUser : JSON.parse(localStorage.getItem("currentUser"))
        })
    }

    onSubmitLogin = (e) => {
        e.preventDefault();
        var user_data = {
            username : this.state.username,
            password : this.state.password
        }
        axios.post("http://54.95.40.41:3000/login", user_data).then(res => {
            console.log(res.data)
            localStorage.setItem("currentUser", JSON.stringify(res.data.user));
            localStorage.setItem("authorization", res.data.token);
            this.setState({
                currentUser : res.data.user,

            })
            var newHeaders = "bearer " + res.data.token;
            axios.defaults.headers.common['Authorization'] = newHeaders;
        })
    }

    onChangeInput = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name] : value
        });
    }

    render() {
        if(this.state.currentUser != null && this.state.currentUser != null){
            return (
                <Redirect to="/"></Redirect>
            )
        }
        return (
            <div>
                <Header/>
                <div className="row" style={{marginTop: '100px'}}>
                    <div className="col-md-3 col-sm-3">
                    </div>
                    <div className="col-md-6 col-sm-6">
                        <form onSubmit={this.onSubmitLogin}>
                            <div className="form-group">
                                <label htmlFor="email">Username</label>
                                <input type="text" className="form-control" id="username" name="username" onChange={this.onChangeInput}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="pwd">Password</label>
                                <input type="password" className="form-control" id="password" name="password" onChange={this.onChangeInput}/>
                            </div>
                            <button type="submit" className="btn btn-info">Login</button>
                        </form>
                    </div>
                    <div className="col-md-3 col-sm-3">

                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Login;
