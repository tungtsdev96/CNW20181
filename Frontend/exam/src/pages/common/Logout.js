import React, { Component } from 'react';
import axios from 'axios'
import {Redirect} from 'react-router-dom'
class Logout extends Component {

    render() {
        localStorage.removeItem("currentUser");
        localStorage.removeItem("authorization")
        return (
            <Redirect to="/"></Redirect>
        );
    }
}

export default Logout;
