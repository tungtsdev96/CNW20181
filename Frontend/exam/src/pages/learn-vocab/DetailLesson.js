import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import '../../css/ListTopic.css';
import callApi from '../../utils/ApiCaller';

import MenuItem from './../../components/learn-vocab/detail-lesson/MenuItem.js';
import CarouselPreviewsVocabulary from './../../components/learn-vocab/detail-lesson/CarouselPreviewsVocabulary.js';
import ListVocabOfLesson from './../../components/learn-vocab/detail-lesson/ListVocabOfLesson.js';
import CarouselLessonRecommends from './../../components/learn-vocab/detail-lesson/lesson-recommend/CarouselLessonRecommends.js';
import Header from '../../components/common/HeaderInContent'
import Footer from '../../components/common/Footer'

class DetailLeson extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lesson: {}
        }
    }

    componentWillMount() {
        var idLesson = this.props.match.params.idLesson;
        callApi('GET', "http://54.95.40.41:3000/api/lesson/get-lesson?idLesson=" + idLesson, {})
            .then((response) => {
                this.setState({
                    lesson: response.data
                });
            })
    }

    render() {

        var { lesson } = this.state;

        return (
            <div>
                <Header />
                <div style={{height:'80px'}}></div>
                <div className="container-fluid">

                    <div className="page-content">

                        {/* <!-- main header infor lesson  --> */}
                        <div className="lesson-infor">
                            <div className="shadow-sm h-auto d-inline-block bg-white rounded">
                                <span className="lesson-vocabCount">{lesson.totalVocab} từ</span>
                            </div>
                            <br />
                            <h1 className="lesson-name">{lesson.lessonNameEn} - {lesson.lessonNameVi}</h1>
                        </div>

                        {/* <!-- menu to learns --> */}
                        <div className="menu-study">
                            <div className="menu-study-bottom"></div>
                            <div className="menu-study-type">
                                <h6>STUDY</h6>
                                <Link to={'/LearnVocabOfLesson/learn/' + lesson.id}>
                                    <MenuItem iconItemMenu="fa-question-circle" menuItemName="Trắc nghiệm từ" />
                                </Link>
                                <Link to={'/LearnVocabOfLesson/write/' + lesson.id}>
                                    <MenuItem iconItemMenu="fa-pencil-square-o" menuItemName="Học viết" />
                                </Link>
                                <Link to={'/LearnVocabOfLesson/spell/' + lesson.id}>
                                    <MenuItem iconItemMenu="fa-volume-up" menuItemName="Học nghe từ" />
                                </Link>
                            </div>
                        </div>

                        <div className="bg-main">

                            {/* <!-- flash card vocabulary --> */}
                            <CarouselPreviewsVocabulary lesson={this.state.lesson} />

                            {/* list vocab */}
                            <ListVocabOfLesson lesson={lesson} />

                            {/* list lesson */}
                            <CarouselLessonRecommends lesson={lesson} handleClickItemLesson={this.handleClickItemLesson} />

                        </div>

                    </div>
                </div>
                <Footer />
            </div>
        );
    }

    handleClickItemLesson = (les) => {
        this.setState({
            lesson: les
        })
    }
}

export default DetailLeson;
