import React, { Component } from 'react';
import SideBarControls from './../../components/learn-vocab/learn-vocab-lesson/SideBarControls.js'
import LearnByQuestion from './../../components/learn-vocab/learn-vocab-lesson/TypeLearn.js'
import LearnByWriteVocab from './../../components/learn-vocab/learn-vocab-lesson/TypeWrite.js'
import LearnBySpellVocab from './../../components/learn-vocab/learn-vocab-lesson/TypeSpell.js'
import Header from '../../components/common/HeaderInContent'
import Footer from '../../components/common/Footer'

import './../../css/LearnVocabOfTopic.css';

class LearnVocabOfTopic extends Component {

    constructor(props){
        super(props);
        this.state = {
            typeLearnVocab: '',
            idLesson: -1,
            trueWord : 0,
            falseWord : 0,
            curentQuestion: 1
        }
    }


    handleSubmit = (result) => {
        if(result) {
            this.setState({
                trueWord : this.state.trueWord + 1,
                curentQuestion : this.state.curentQuestion + 1
            });
        } else {
            this.setState({
                falseWord : this.state.falseWord + 1,
                curentQuestion : this.state.curentQuestion + 1
            });
        }
    }

    getViewContentOfComponent = () => {
        var type = this.state.typeLearnVocab;
        switch (type) {
            case 'learn':
                return (
                    <LearnByQuestion idLesson={this.state.idLesson} handleSubmit={this.handleSubmit}/>
                );            
            case 'write':
                return (
                    <LearnByWriteVocab idLesson={this.state.idLesson} handleSubmit={this.handleSubmit}/>
                ); 
            case 'spell':
                return (
                    <LearnBySpellVocab idLesson={this.state.idLesson} handleSubmit={this.handleSubmit}/>
                );
        }
    }

    showSideBar = (trueWord, falseWord) => {
        // console.log(trueWord, falseWord);
        return (
            <SideBarControls 
                trueWord={trueWord} 
                falseWord={falseWord} 
                curentQuestion={this.state.curentQuestion}
                totalQuestion={12} />
        )
    }

    componentWillMount () {
        this.setState({
            idLesson: this.props.match.params.idLesson,
            typeLearnVocab: this.props.match.params.type
        })
    }

    render() {
        return (
            <div>
                <Header/>
                    <div className="container-fluid" style={styleContent}>
                        {/* side bar controlse */}
                        {this.showSideBar(this.state.trueWord, this.state.falseWord)}
                        <div 
                            className="mode-content shadow-sm" 
                            style={{backgroundColor: '#fff', marginRight: '30px', overflow: 'none'}}>
                            {this.getViewContentOfComponent()}
                        </div>
                    </div>
                {/* <Footer/> */}
            </div>
            
        );
    }
}

const styleContent = {
    backgroundColor: '#eee',
    minHeight: '100vh'
}

export default LearnVocabOfTopic;
