import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import '../../css/ListTopic.css';
import callApi from '../../utils/ApiCaller';
import ItemTopic from '../../components/topic/ItemTopic';
import ItemLesson from '../../components/topic/ItemLesson'
import Header from '../../components/common/HeaderInContent'
import Footer from '../../components/common/Footer'

class ListTopic extends Component {

    constructor(props) {
        super(props);
        this.state = {
            topics: [],
            popularLessons: []
        }
    }

    componentWillMount() {

        callApi('GET', "http://54.95.40.41:3000/api/topic/get-all", {})
            .then((response) => {
                this.setState({
                    topics: response.data,
                });
            })

        callApi('GET', "http://localhost:3000/api/lesson/get-popular-lesson", {})
            .then((response) => {
                this.setState({
                    popularLessons: response.data
                })
            })
    }

    showListPopularLesson = () => {
        var popularLessons = this.state.popularLessons;
        if (popularLessons.length == 0) return null;
        
        var result = popularLessons.map((lesson) => {
            var link = "/DetailLesson/" + lesson.id;
            return (
                <div className="col-md-3">
                    {this.getItemLesson(link, lesson)}
                </div>
            )
        })
        return result;
    }

    getItemLesson = (linkTo, lesson) => {
        return (
            <Link to={linkTo}>
                <ItemLesson lesson={lesson} />
            </Link>
        )
    }

    showListTopic(topics) {
        var result = null;
        result = topics.map((topic, index) => {
            return (
                <ItemTopic topic={topic} key={index} />
            );
        })
        return result;
    }

    render() {
        var { topics } = this.state;

        return (
            <div>
                <Header />

                {/* popular lesson */}
                <section className="lesson-area">
                    <div className="container" >
                        {/* heading */}
                        <div className="row">
                            <div className="col-12">
                                <div className="section-heading">
                                    <h3>Bài học phổ biến</h3>
                                </div>
                            </div>
                        </div>

                        {/* leson */}
                        <div className="row">
                            {this.showListPopularLesson()}
                        </div>
                    </div>
                </section>

                {/* topic */}
                <section className="lesson-area">
                    <div className="container" >
                        {/* heading */}
                        <div className="row">
                            <div className="col-12">
                                <div className="section-heading">
                                    <h3>Bài học theo chủ đề</h3>
                                </div>
                            </div>
                        </div>

                        {/* topic */}
                        <div className="row">
                            {this.showListTopic(topics)}
                        </div>
                    </div>
                </section>

                <Footer />
            </div>

        );
    }
}

export default ListTopic;