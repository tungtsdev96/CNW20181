import React, { Component } from 'react';
import Part1 from './../../components/exams/parts/Part1'
import Part2 from './../../components/exams/parts/Part2'
import Part3 from './../../components/exams/parts/Part3'
import Part4 from './../../components/exams/parts/Part4'
import Part5 from './../../components/exams/parts/Part5'
import Part6 from './../../components/exams/parts/Part6'
import Part7 from './../../components/exams/parts/Part7'
import Info from './../../components/exams/info/Info';
import callApi from './../../utils/ApiCaller';
import Modal from 'react-responsive-modal';
import Header from '../../components/common/HeaderInContent'
import Footer from '../../components/common/Footer'

import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
class QuizPage extends Component {

    constructor(props) {
        super(props);
        this.audio = null;
        this.state = {
            currentPart: 0,
            currentTest: {},
            testId: '',
            isShowStartQuiz: false,
            isShowCancelQuiz: false,
            isShowSummitQuiz: false,
            isShowOutOfTine: false,
            trueAnswers: [],
            trueListening: 0,
            trueReading: 0,
        }
    }

    componentDidMount() {
        var listTrueAnswers = new Array(201);
        if (localStorage.getItem("isDoingQuiz") == "true") {
            this.setState({
                isShowStartQuiz: false
            });
        } else {
            this.setState({
                isShowStartQuiz: true
            });
        }
        this.setState({
            isShowStartDoing: !localStorage.getItem('isDoingExam')
        });
        var testId = this.props.match.params.testId;
        callApi('GET', "http://54.95.40.41:3000/test/" + testId, {}).then((response) => {
            console.log((response.data));
            this.setState({
                currentTest: response.data.data,
            });
            console.log(this.state.currentTest);
            var lstQuestionsPart1 = [];
            var lstQuestionsPart2 = [];
            var lstQuestionsPart3 = [];
            var lstQuestionsPart4 = [];
            var lstQuestionsPart5 = [];
            var lstGroupPart6 = [];
            var lstGroupPart7 = [];
            for (var i = 0; i < 100; i++) {
                if (i < 10) {
                    lstQuestionsPart1.push(this.state.currentTest.content.listening.question[i]);
                } else if (i < 40) {
                    lstQuestionsPart2.push(this.state.currentTest.content.listening.question[i]);
                } else if (i < 70) {
                    lstQuestionsPart3.push(this.state.currentTest.content.listening.question[i]);
                } else if (i < 100) {
                    lstQuestionsPart4.push(this.state.currentTest.content.listening.question[i]);
                }
                listTrueAnswers[i + 1] = this.state.currentTest.content.listening.question[i].answer;
            }
            lstQuestionsPart5 = this.state.currentTest.content.reading.part5;
            for (var i = 0; i < lstQuestionsPart5.length; i++) {
                listTrueAnswers[lstQuestionsPart5[i].index] = lstQuestionsPart5[i].answer;
            }
            lstGroupPart6 = this.state.currentTest.content.reading.part6;
            for (var i = 0; i < lstGroupPart6.length; i++) {
                var g = lstGroupPart6[i];
                for (var j = 0; j < g.questions.length; j++) {
                    listTrueAnswers[g.questions[j].index] = g.questions[j].answer;
                }
            }
            lstGroupPart7 = this.state.currentTest.content.reading.part7;
            for (var i = 0; i < lstGroupPart7.length; i++) {
                var g = lstGroupPart7[i];
                for (var j = 0; j < g.questions.length; j++) {
                    listTrueAnswers[g.questions[j].index] = g.questions[j].answer;
                }
            }

            console.log(listTrueAnswers);
            this.setState({
                trueAnswers: listTrueAnswers
            })
            this.setState({
                lstQuestionsPart1, lstQuestionsPart2, lstQuestionsPart3, lstQuestionsPart4, lstQuestionsPart5, lstGroupPart6, lstGroupPart7
            });
            this.setTimeOutOfPart();
        });
    }


    onChangePart = (part) => {
        console.log("state", this.state)
        this.setState({
            currentPart: part
        });
    }

    showContentOfPart = () => {
        var { currentPart } = this.state;
        console.log(currentPart)
        switch (currentPart) {
            case 1: return <Part1>{this.state.lstQuestionsPart1}</Part1>
            case 2: return <Part2>{this.state.lstQuestionsPart2}</Part2>
            case 3: return <Part3>{this.state.lstQuestionsPart3}</Part3>
            case 4: return <Part4>{this.state.lstQuestionsPart4}</Part4>
            case 5: return <Part5>{this.state.lstQuestionsPart5}</Part5>
            case 6: return <Part6>{this.state.lstGroupPart6}</Part6>
            case 7: return <Part7>{this.state.lstGroupPart7}</Part7>
            default: return null;
        }
    }

    render() {

        console.log("Render QuizPage");
        return (
            <div>
                <Header />
                <div className="row" style={{marginTop: '100px'}}>
                    <Modal open={this.state.isShowStartQuiz} onClose={this.onCloseStartQuiz} center>
                        <h5>Bạn muốn bắt đầu làm bài không</h5>
                        <div>
                            <Link to="/" className="btn btn-danger btn-cancel-submit-modal" onClick={this.onCloseStartQuiz}>Quay lại</Link>
                            <button className="btn btn-success btn-cancel-submit-modal" onClick={this.onStartQuiz}>Bắt đầu</button>
                        </div>

                    </Modal>

                    <Modal open={this.state.isShowCancelQuiz} onClose={this.onCloseStartQuiz} center>
                        <h5>Bạn muốn thoát làm bài kiểm tra không?</h5>
                        <div>
                            <button className="btn btn-danger btn-cancel-submit-modal" onClick={this.onCloseCancelQuiz}>Không</button>
                            <Link className="btn btn-success btn-cancel-submit-modal"  onClick={this.onCancelQuiz} to="/">Thoát</Link>
                        </div>

                    </Modal>
                    <Modal open={this.state.isShowSummitQuiz} onClose={this.onCloseStartQuiz} center>
                        <h5>Bạn muốn bắt đầu làm bài không</h5>
                        <div>
                            <button className="btn btn-danger btn-cancel-submit-modal" onClick={this.onCancelSubmitQuiz}>Quay lại</button>
                            <button className="btn btn-success btn-cancel-submit-modal" onClick={this.onSubmitQuiz}>Nộp bài</button>
                        </div>

                    </Modal>
                    <Modal open={this.state.isShowOutOfTine} onClose={this.onCloseStartQuiz} center>
                        <h5>Bạn muốn bắt đầu làm bài không</h5>
                        <div>
                            <button className="btn btn-success" onClick={this.onStartQuiz}>Bắt đầu</button>
                        </div>

                    </Modal>

                    <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    </div>
                    <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7" ref="contentOfPart">
                        {this.showContentOfPart()}
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div className="current-quiz-process">
                            <Info
                                onChangePart={this.onChangePart}
                                currentPart={this.state.currentPart}
                                currentTest ={this.state.currentTest}
                                onShowSubmitQuiz = {this.onShowSubmitQuiz}
                                onShowCancelQuiz = {this.onShowCancelQuiz}
                                >
                                </Info>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
            
        );
    }

    onShowStartQuiz = () => {
        this.setState({
            isShowStartQuiz: true
        });
    }

    onStartQuiz = () => {
        this.setState({
            isShowStartQuiz: false
        });
        localStorage.setItem("isDoingQuiz", true);
        for (var i = 1; i <= 100; i++) {
            if (localStorage.getItem(i + "")) {
                localStorage.removeItem(i + "");
            }
        }
        localStorage.removeItem("minute");
        localStorage.removeItem("second");
        console.log(this.state.currentTest.content);
        if (!this.audio) {
            this.audio = new Audio(this.state.currentTest.content.listening.audio);
            this.audio.play();
        }

    }

    onCloseStartQuiz = () => {
        this.setState({
            isShowStartQuiz: false
        })
    }

    onShowCancelQuiz = () => {
        this.setState({
            isShowCancelQuiz: true
        })
    }

    onCloseCancelQuiz = () => {
        this.setState({
            isShowCancelQuiz: false
        });
    }

    onCancelQuiz = () => {
        localStorage.setItem("isDoingQuiz", false);
        for (var i = 1; i <= 100; i++) {
            if (localStorage.getItem(i + "")) {
                localStorage.removeItem(i + "");
            }
        }
        localStorage.removeItem("minute");
        localStorage.removeItem("second");
        this.setState({
            isShowCancelQuiz: false
        });
    }

    onShowSubmitQuiz = () => {
        this.setState({
            isShowSummitQuiz: true
        });
    }

    onSubmitQuiz = () => {
        localStorage.setItem("isDoingQuiz", false);
        console.log("onSubmitQuiz")
        this.setState({
            isShowSummitQuiz: false
        });
        var newCompletedQuiz = this.summaryQuiz();
        console.log(newCompletedQuiz)
        callApi('POST', "http://54.95.40.41:3000/quiz/add-completed-quiz", newCompletedQuiz).then((response) => {
            console.log(response.data);
        });
    }

    onCancelSubmitQuiz = () => {
        this.setState({
            isShowSummitQuiz: false
        });
    }

    onShowOutOfTime = () => {

    }

    summaryQuiz = () => {
        var list_answer = [];
        var correct_listening = 0;
        var correct_reading = 0;
        list_answer[0] = null;
        for (var i = 1; i <= 200; i++) {
            list_answer.push(localStorage.getItem(i + ""));
        }
        console.log("list_answer : ", list_answer);
        var newCompletedQuiz = {};
        newCompletedQuiz.list_answer = list_answer;
        var currentUser = JSON.parse(localStorage.getItem("currentUser") + "");
        newCompletedQuiz.user_id = currentUser.id;
        var { currentTest } = this.state;
        newCompletedQuiz.exam_id = currentTest.fID;
        newCompletedQuiz.test_name = currentTest.name;
        var listListenQuestion = this.state.currentTest.content.listening.question;
        // var listReadingQuestion = this.state.currentTest.content.listening.question;
        var correct_choice_listening = [];
        var correct_choice_reading = [];

        var { trueAnswers } = this.state;
        for (var i = 1; i <= 100; i++) {
            if (trueAnswers[i] == list_answer[i]) {
                correct_listening++;
            }
        }

        var { trueAnswers } = this.state;
        for (var i = 101; i <= 200; i++) {
            if (trueAnswers[i] == list_answer[i]) {
                correct_reading++;
            }
        }
        newCompletedQuiz.listening = correct_listening;
        newCompletedQuiz.reading = correct_reading;
        newCompletedQuiz.used_time = localStorage.getItem("displayTime");
        return newCompletedQuiz;
    }

    setTimeOutOfPart = () => {
        this.setState({
            currentPart: 1
        });
    }

}

export default QuizPage;
