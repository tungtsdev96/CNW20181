import React, { Component } from 'react';
import callApi from '../../utils/ApiCaller';
import HeaderInContent from './../../components/common/HeaderInContent'
import TestItem from './../../components/exams/TestItem'
import Header from '../../components/common/HeaderInContent'
import Footer from '../../components/common/Footer'
import {Redirect} from 'react-router-dom'

class ListTestPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            lstTest : [],
            filter : 0,
            currentPage : 1
        }
    }


    componentDidMount(){
        if(this.state.filter == 0){
            callApi('GET', "http://54.95.40.41:3000/tests?limit=5&page=" + this.state.currentPage, {}).then( (response) => {
                console.log(response)
                this.setState({
                    lstTest : response.data.data
                })
            })
        }

    }

    showListExam = () => {
        
        var {lstTest} = this.state;
        var result = null;
        if(lstTest == null) return null;
        result = lstTest.map( (test, index) => {
            return (<TestItem key={index} >{test}</TestItem>)
        });
        return result;
    }

    showPagition = () => {
        var listOfPage = [1,2,3,4,5,6,7];
        var result = null;
        result=listOfPage.map( (page, index) => {
            return (<li key={index} onClick={() => this.onChangePage(page)}  className="page-item"><a className={page == this.props.currentPage ? "page-link pagation-active" : "page-link"}>{page}</a></li>);
        });
        return result;
    }

     onChangePage = async function(page) {
        await this.setState({
            currentPage : page
        })
        await console.log(this.state.currentPage, page);
        await this.reloadData();
    }

    reloadData = () => {
      if(this.state.filter == 0){
        callApi('GET', "http://54.95.40.41:3000/tests?limit=5&page=" + this.state.currentPage, {}).then( (response) => {
            console.log(response)
            this.setState({
                lstTest : response.data.data
            })
        })
      }else{
        callApi('GET', "http://54.95.40.41:3000/tests?limit=5&page=" + this.state.currentPage, {}).then( (response) => {
            console.log(response)
            this.setState({
                lstTest : response.data.data
            })
        })
      }
    }

    onChangFilter = (filter) => {
      this.setState({
        filter : filter
      });
    }

    render() {
        var currentUser = JSON.parse(localStorage.getItem("currentUser") + "")
        if(currentUser){
            return (
                <div>
                    <Header/>
                    <div className="container list-test-result">
                        {this.showListExam()}
                        <ul className="pagination">
                            {this.showPagition()}
                        </ul>
                    </div>
                    
                    <Footer/>
                </div>
            );
        }else{
            return <Redirect to='/Login'  />
        }
        
    }
}

export default ListTestPage;
