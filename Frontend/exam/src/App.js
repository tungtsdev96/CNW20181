import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import routes from './utils/Router';
import Header from './components/common/Header'
import Footer from './components/common/Footer'

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    {/* <Header/> */}
                    <div id="content">
                        {this.showContent(routes)}
                    </div>
                    {/* <Footer/> */}
                </div>

            </Router>
        );
    }

    showContent = (routes) => {
        var result = null;
        if(routes != null){
            result = routes.map( (route, index) => {
                return (
                    <Route key={index} path = {route.path}  exact = {route.exact} component = {route.main} ></Route>
                );
            })
        }
        return result;
    }
}

export default App;
