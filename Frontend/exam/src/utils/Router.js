import React, { Component } from 'react';
import QuizPage from './../pages/exams/QuizPage';
import HomePage from '../pages/home/HomePage';
import ListTestPage from './../pages/exams/ListTestPage';
import ListTopic from '../pages/learn-vocab/ListTopic';
import DetailLesson from '../pages/learn-vocab/DetailLesson';
import LearnVocabOfLesson from '../pages/learn-vocab/LearnVocabOfLesson';
import Course from '../pages/course/Course';
import DetailCourse from '../pages/course/DetailCourse'
import CourseLearning from '../pages/course/LearningCourse'
import LoginPage from './../pages/common/LoginPage'
import SignUp from './../pages/common/SignUp'
import Logout from './../pages/common/Logout'
const routes = [
    {
        path : '/QuizPage/:testId',
        exact : true,
        main : ({match}) => <QuizPage match={match}/>
    },
    {
        path : '/TestList',
        exact : true,
        main : () => <ListTestPage />
    },
    {
        path : '/ListTopic',
        exact : true,
        main : () => <ListTopic />
    },
    {
        path : '/DetailLesson/:idLesson',
        exact : true,
        main : ({match}) => <DetailLesson match={match}/>
    },
    {
        path : '/LearnVocabOfLesson/:type/:idLesson',
        exact : true,
        main : ({match}) => <LearnVocabOfLesson match={match}/>
    },
    {
        path: '/Course',
        exact: true,
        main: () => <Course/>
    },
    {
        path: '/Course/detail-course/:id',
        exact: true,
        main: ({match}) => <DetailCourse match={match}/>
    },
    {
        path: '/course/learning/:idCourse/:idVideo',
        exact: true,
        main: ({match}) => <CourseLearning match={match}/>
    },
    {
        path : '/Login',
        exact : true,
        main : () => <LoginPage />
    },
    {
        path : '/SignUp',
        exact : true,
        main : () => <SignUp />
    },
    {
        path : '/Logout',
        exact : true,
        main : () => <Logout />
    },
    {
        path : '/',
        exact : true,
        main : () => <HomePage />
    }
];

export default routes;
