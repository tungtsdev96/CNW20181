import React, {Component} from 'react';
import Question from '../questions/Question';

class Part3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lstQuestions: [],
        }
    }

    showQuestions = (questions) => {
        var result = null;
        if(questions != null){
            result = questions.map( (element, index) => {
                return <Question key={index}>{element}</Question>
            });
        }
        return result;
    }

    componentDidMount(){
        this.setState({
            lstQuestions : this.props.children
        })
    }

    render() {
        var listQuestions = this.state.lstQuestions;
        return (
            <div >
                <div className="part-intro">
                    <h3 className="part-title">Part 3</h3>
                    <strong>Directions:</strong> You will hear several conversations between two people. You will be asked to answer three
                    questions about what the speakers say in each conversation. Select the best response to each question and
                    mark answer (A), (B), (C), or (D) on the answer sheet. The conversations are not printed in your test book and
                    you may only listen to the audio one time. 
                </div><br /><br />
                {this.showQuestions(listQuestions)}
            </div>
        );
    }
}

export default Part3;