import React, {Component} from 'react';
import GroupQuestion from './GroupQuestion';
class Part7 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lstGroups: [],
        }
    }

    showQuestions = (lstGroups) => {
        var result = null;
        if(lstGroups != null){
            result = lstGroups.map( (element, index) => {
                return <GroupQuestion key={index}>{element}</GroupQuestion>
            });
        }
        return result;
    }

    

    componentDidMount(){
        this.setState({
            lstGroups : this.props.children
        })
    }

    render() {
        var lstGroups = this.state.lstGroups;
        return (
            <div >
                <div className="part-intro">
                    <h3 className="part-title">Part 7</h3>
                    <strong>Directions:</strong> : A word or phrase is missing in each of the sentences below. Four answer choices are given below
                        each sentence. Select the best answer to complete the sentence. Then mark answer (A), (B), (C), or (D).
                </div><br /><br />
                {this.showQuestions(lstGroups)}
                <div></div>
            </div>
        );
    }
}

export default Part7;
