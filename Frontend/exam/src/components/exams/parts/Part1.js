import React, {Component} from 'react';
import Question from '../questions/Question';

class Part1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lstQuestions: [],
        }
    }

    showQuestions = (questions) => {
        var result = null;
        if(questions != null){
            result = questions.map( (element, index) => {
                return <Question key={index}>{element}</Question>
            });
        }
        // console.log("result of part 1",result);
        return result;
    }

    componentDidMount(){
        this.setState({
            lstQuestions : this.props.children
        })
    }

    componentWillMount(){
        this.setState({
            lstQuestions : this.props.children
        })
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            lstQuestions : nextProps.children
        })
    }

    render() {
        console.log("Render part 1", this.state.lstQuestions)
        var listQuestions = this.state.lstQuestions;
        return (
            <div style={{marginTop:50}}>
                <div className="part-intro">
                    <b className="part-title">Part 1</b>
                    Directions: For each question in this part, you will hear four statements about a picture in your test book.
                    When you hear the statements, you must select the one statement that best describes what you see in the
                    picture by marking the correct answer option: A, B, C, or D on the answer sheet. The statements are not
                    printed in your test book and you may only listen to audio one time.
                </div><br /><br />
                {this.showQuestions(listQuestions)}
            </div>
        );
    }
}

export default Part1;