import React, {Component} from 'react';
import Question from '../questions/Question';

class Part4 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lstQuestions: [],
        }
    }

    showQuestions = (questions) => {
        var result = null;
        if(questions != null){
            result = questions.map( (element, index) => {
                return <Question key={index}>{element}</Question>
            });
        }
        return result;
    }

    componentDidMount(){
        this.setState({
            lstQuestions : this.props.children
        })
    }

    render() {
        var listQuestions = this.state.lstQuestions;
        return (
            <div >
                <div className="part-intro">
                    <h3 className="part-title">Part 5</h3>
                    <strong>Directions:</strong> : A word or phrase is missing in each of the sentences below. Four answer choices are given below
                        each sentence. Select the best answer to complete the sentence. Then mark answer (A), (B), (C), or (D).
                </div><br /><br />
                {this.showQuestions(listQuestions)}
            </div>
        );
    }
}

export default Part4;
