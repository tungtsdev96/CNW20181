import React, {Component} from 'react';
import Question from '../questions/Question';

class GroupQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            questions: [],
            common: '',
            common_bellow : '',
            part : 0
        }
    }

    showCommontBellow = () => {
      if(this.state.common_bellow){
        return (
          <div>
            {this.state.common_bellow}
          </div>
        )
      }
    }

    componentWillMount(){
      console.log("render group", this.props.children);
        this.setState({
            questions: this.props.children.questions,
            common: this.props.children.common,
            common_bellow : this.props.children.common_bellow,
            part : this.props.children.questions[0].part
        })
    }
    

    showAllQuestion = (questions) => {console.log(questions)
        var result = null;
        if(questions != null){
            result = questions.map( (element, index) => {
                console.log(element);
                return <Question key={index}>{element}</Question>
            })
        }
        return result;
    }

    render() {
        var numberQuestion = this.state.questions.length;
        var title = "This paragraph is for question from " + this.state.questions[0].index + "  to " + this.state.questions[numberQuestion - 1].index;
        return (
            <div className="group-title">
                <i><b>{title}</b></i> <br />
                {this.state.common} <br />
                {this.showAllQuestion(this.state.questions)}
                {this.showCommontBellow()}
            </div>
        );
    }
}

export default GroupQuestion;
