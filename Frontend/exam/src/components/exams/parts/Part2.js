import React, {Component} from 'react';
import Question from '../questions/Question';

class Part2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lstQuestions: [],
        }
    }

    showQuestions = (questions) => {
        var result = null;
        if(questions != null){
            result = questions.map( (element, index) => {
                return <Question key={index}>{element}</Question>
            });
        }
        return result;
    }

    componentDidMount(){
        this.setState({
            lstQuestions : this.props.children
        })
    }

    render() {
        var listQuestions = this.state.lstQuestions;
        return (
            <div >
                <div className="part-intro">
                    <h3 className="part-title">Part 2</h3>
                    Directions: You will hear a question or statement and three responses spoken in English. They are not printed
                    in your test book and you may only listen to the audio one time. Select the best response to the question or
                    statement and select (A), (B), or (C) to mark your answer on the answer sheet.
                </div><br /><br />
                {this.showQuestions(listQuestions)}
            </div>
        );
    }
}

export default Part2;