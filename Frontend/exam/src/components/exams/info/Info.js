import React, { Component } from 'react';
class Info extends Component {

    constructor(props){
        super(props);
        this.state={
            minute : localStorage.getItem("minute"),
            second : localStorage.getItem("second"),
            displayTime : localStorage.getItem("displayTime"),
            currentTest : {}
        }
    }

    componentWillMount(){

    }

    componentWillReceiveProps(nextProps){
      this.setState({
          currentTest : this.props.currentTest
      });
    }

    componentDidMount(){
        var localMinute = localStorage.getItem("minute");
        var localSecond = localStorage.getItem("second");
        var displayTime;
         var minute = 0;
         var second = 0;
        var displayMinute = minute;
        var displaySecond =  second;
        if(localMinute && localSecond){
            if(localMinute > 0 && localSecond > 0){
                minute = localMinute;
                second = localSecond
            }
        }
        if(second < 10){
            displaySecond = '0'+second;
        }
        if(minute < 10){
            displayMinute = '0'+minute;
        }
        displayTime = displayMinute + ' : ' + displaySecond;
        this.setState({
            minute : minute,
            second : second,
            displayTime : displayTime
        });

        console.log(this.props.currentTest);
        this.setState({
            currentTest : this.props.currentTest
        });
        setInterval(this.onUpdateTime, 1000);
    }

    onUpdateTime = () => {
        if (localStorage.getItem("isDoingQuiz") == "true") {
            var {
                minute,
                second,
                displayTime
            } = this.state;
            second++;
            if (second == 60) {
                second = 0;
                minute++;
            }
            var displayMinute = minute;
            var displaySecond = second;
            if (second < 10) {
                displaySecond = '0' + second;
            }
            if (minute < 10) {
                displayMinute = '0' + minute;
            }
            displayTime = displayMinute + ' : ' + displaySecond;
            this.setState({
                minute: minute,
                second: second,
                displayTime: displayTime
            });
            localStorage.setItem("minute", minute);
            localStorage.setItem("displayTime", displayTime);
            localStorage.setItem("second", second);
        }else{
            localStorage.setItem("minute", 0);
            localStorage.setItem("displayTime", '00 : 00');
            localStorage.setItem("second", 0);
        }

    }

    showListOfPart = () => {
        var listOfPart = [1,2,3,4,5,6,7];
        var result = null;
        result=listOfPart.map( (part, index) => {
            return (<li key={index} onClick={() => this.props.onChangePart(part)}  className="page-item"><a className={part == this.props.currentPart ? "page-link pagation-active" : "page-link"}>{part}</a></li>);
        });
        return result;
    }

    render() {
        console.log("Render Infor");
        return (
            <div className="card info-card">
                <div className="card-header">
                    Thông tin cá nhân
                </div>
                <div className="card-body">
                   <b>Họ tên</b><br />
                   <span>Phạm Hải Linh</span><br /><br />
                   <b>Thông tin bài kiểm tra</b><br />
                   <span>{this.state.currentTest.name}</span><br /><br />
                   <b>Thời gian làm bài</b><br />
                   <span>90:00</span><br /><br />
                   <b>Thời gian đã làm</b><br />
                   <span>{this.state.displayTime}</span><br /><br />
                   <b>Danh sách các part</b><br />
                    <nav>
                        <ul className="pagination">
                            {this.showListOfPart()}
                        </ul>
                    </nav>
                </div>
                <div>
                    <button className="btn btn-danger btn-cancel-submit" onClick={this.props.onShowCancelQuiz}>Cancel</button>
                    <button className="btn btn-primary btn-cancel-submit" onClick={this.props.onShowSubmitQuiz}> Submit</button>
                </div>
            </div>
        );
    }
}

export default Info;
