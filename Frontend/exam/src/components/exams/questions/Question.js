import React, { Component } from 'react';

class Question extends Component {
    constructor(props) {
        super(props);
        this.state = {
            question : {},
            selected: '',
            isViewingResult : null
        }
    }

    showResult = () => {
        if(this.state.isViewingResult){
            return (
                <div><b>Answer : {this.props.children.answer}</b></div>
            );
        }
        return null;
    }
    componentWillMount(){
        this.setState({
            question : this.props.children
        })
    }

    handleEventSelect = (event) => {
        var value = event.target.value;
        localStorage.setItem("" + this.props.children.index, value);
        this.setState({
            selected: value
        })
    }

    componentDidMount() {
        var selectedChoice = localStorage.getItem(this.props.children.index);
        var isViewingResult = localStorage.getItem("isViewingResult");
        this.setState({
            selected: selectedChoice,
            isViewingResult : isViewingResult
        })
    }

    handleChoice = (selectedChoice) => {
        localStorage.setItem("" + this.props.children.index, selectedChoice);
        this.setState({
            selected: selectedChoice
        })
    }


    handleBreakLine = () => {
      if(this.props.children.part == 6){
        return <br />
      }else{
        return null;
      }
    }

    render() {
        var result = null;
        var question = this.props.children;
        var index = question.index;
        var part = question.part;
        var title = question.title;
        var image = question.image;
        console.log(image)
        console.log("Render part 1 q ", index);
        var nameOfQuestion = "question" + index;
        if (part == 1) {
            title = null;
            result = (
             <div className="card question-cart">
                <div className="card-body">
                    <h5 className="card-title question-title"><span className="question-index">Question {index} {this.state.isViewingResult ? title : ''}</span> </h5>
                    <img src={image} className="img-part1" alt=""/>
                    <div className="radio radio-part1">
                        <label className="label-choice-part1">
                            <input type="radio" name={nameOfQuestion} checked={'A' === this.state.selected} value='A' onChange={this.handleEventSelect} /> A:{this.state.isViewingResult ? question.A : ''}
                        </label>
                        <label className="label-choice-part1">
                            <input type="radio" name={nameOfQuestion} checked={'B' === this.state.selected} value='B' onChange={this.handleEventSelect} /> B: {this.state.isViewingResult ? question.B : ''}
                        </label>
                        <label className="label-choice-part1">
                            <input type="radio" name={nameOfQuestion} checked={'C' === this.state.selected} value='C' onChange={this.handleEventSelect} /> C: {this.state.isViewingResult ? question.C : ''}
                        </label>
                        <label className="label-choice-part1">
                            <input type="radio" name={nameOfQuestion} checked={'D' === this.state.selected} value='D' onChange={this.handleEventSelect} /> D: {this.state.isViewingResult ? question.D : ''}
                        </label>
                    </div>
                </div>
            </div>);
        }

        if (part === 2) {
            title = null;
            result = (
            <div className="card question-cart">
                <div className="card-body">
                    <h5 className="card-title question-title"><span className="question-index">Question {index}</span>. </h5>
                    <div className="radio radio-part1">
                        <label className="label-choice-part2">
                            <input type="radio" name={nameOfQuestion} checked={'A' === this.state.selected} value='A' onChange={this.handleEventSelect} /> A: {this.state.isViewingResult ? question.A : ''}
                        </label>
                        <label className="label-choice-part2">
                            <input type="radio" name={nameOfQuestion} checked={'B' === this.state.selected} value='B' onChange={this.handleEventSelect} /> B: {this.state.isViewingResult ? question.B : ''}
                        </label>
                        <label className="label-choice-part2">
                            <input type="radio" name={nameOfQuestion} checked={'C' === this.state.selected} value='C' onChange={this.handleEventSelect} /> C: {this.state.isViewingResult ? question.C : ''}
                        </label>
                    </div>
                </div>
            </div>);
        }

        if (part >= 3) {
            result = (
            <div className="card question-cart">
                <div className="card-body">
                    <h5 className="card-title question-title"><span className="question-index">Question {index}</span>.{this.handleBreakLine()} {title}</h5>
                    <div className="radio radio-part1">
                        <label className="label-choice-part1">
                            <input type="radio" name={nameOfQuestion} checked={'A' === this.state.selected} value='A' onChange={this.handleEventSelect} /> A. {question.A}
                        </label><br />
                        <label className="label-choice-part1">
                            <input type="radio" name={nameOfQuestion} checked={'B' === this.state.selected} value='B' onChange={this.handleEventSelect} /> B. {question.B}
                        </label><br />
                        <label className="label-choice-part1">
                            <input type="radio" name={nameOfQuestion} checked={'C' === this.state.selected} value='C' onChange={this.handleEventSelect} /> C.  {question.C}
                        </label><br />
                        <label className="label-choice-part1">
                            <input type="radio" name={nameOfQuestion} checked={'D' === this.state.selected} value='D' onChange={this.handleEventSelect} /> D.  {question.D}
                        </label>
                    </div>
                </div>
            </div>);
        }




        return (
            <div>
                {result}
                <br></br>
                {this.showResult()}
            </div>
        );
    }
}

export default Question;
