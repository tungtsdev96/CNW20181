import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
class TestItem extends Component {
    constructor(props){
        super(props);
        this.state={
            test : null,
        }
    }

    componentWillMount(){
        this.setState({
            test: this.props.children
        })
    }    

    render() {
        
        var {test} = this.state;
        var to = "/QuizPage/" + test.id;
        return (
            <div>
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">{test.name}</h5>
                        <p className="card-text">{test.descripton}</p>
                        <Link to= {to} className="btn btn-primary">Xem chi tiết</Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default TestItem;
