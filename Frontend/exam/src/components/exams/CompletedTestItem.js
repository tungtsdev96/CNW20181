import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
class TestItem extends Component {
    constructor(props){
        super(props);
        this.state={
            testInfor : {},
        }
    }

    componentWillMount(){
        this.setState({
            testInfor: this.props.children
        })
    }

    render() {
        var {testInfor} = this.state;
        var to = "/QuizPage/" + test._id;
        return (
            <div>
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">{testInfor.name}</h5>
                        <p className="card-text">{testInfor.descripton}</p>
                        <Link to= {to} className="btn btn-primary">Xem chi tiết</Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default TestItem;
