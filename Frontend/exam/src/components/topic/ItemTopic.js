import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import '../../css/ListTopic.css';
import callApi from '../../utils/ApiCaller';
import ItemLesson from '../../components/topic/ItemLesson'
import Slider from 'react-slick'


class ItemTopic extends Component {

    constructor(props) {
        super(props);
        this.state = {
            topic: {},
            lessons: [],
            page: 0
        }
    }

    componentWillMount() {
        var { topic } = this.props;

        callApi('GET', "http://54.95.40.41:3000/api/lesson/get-list-lesson-topic?idTopic=" + topic.id, {})
            .then((response) => {
                this.setState({
                    topic: topic,
                    lessons: response.data
                });
            })

    }


    showListLesson = () => {
        var page = this.state.page;
        var result = null;
        var { lessons } = this.state;
        if (lessons.length == 0) return null;

        var ListLessonsToShow = [];
        for (var i = 0; i < 4; i++) {
            var l = lessons[4 * page + i];
            if (l) ListLessonsToShow.push(l);
        }

        result = ListLessonsToShow.map((lesson, index) => {
            var linkTo = "/DetailLesson/" + lesson.id;
            return (
                <div className="col-md-3" key={index}>
                    <Link to={linkTo} key={index}>
                        <ItemLesson lesson={lesson} />
                    </Link>
                </div>
            );
        });

        return result;
    }

    render() {
        var { topic } = this.state;
        return (
            <div className="item-topic" style={{ width: '100%', margin: '0 15px' }}>
                <div className="topic-header d-flex flex-row align-items-center">
                    <h6 className="item-topic-title">{topic.topicEn}</h6>
                    <div className="icon-slide-lesson" onClick={this.prevPage}>
                        <i className="fa fa-arrow-circle-left" aria-hidden="true"></i>
                    </div>
                    <div className="icon-slide-lesson" onClick={this.nextPage}>
                        <i className="fa fa-arrow-circle-right" aria-hidden="true"></i>
                    </div>
                </div>

                <div className="row" style={{ marginLeft: 0, padding: 0 }}>
                    {this.showListLesson()}
                </div>
            </div>
        );
    }

    nextPage = () => {
        var {page} = this.state;
        var totalPage = parseInt(this.state.lessons.length / 4);
        if (page < totalPage) {
            this.setState({
                page: page + 1
            })
        } else {
            this.setState({
                page: 0
            })
        }
    }

    prevPage = () => {
        var {page} = this.state;
        var totalPage = parseInt(this.state.lessons.length / 4);
        if (page > 0) {
            this.setState({
                page: page - 1
            })
        } else {
            this.setState({
                page: totalPage
            })
        }
    }

}

export default ItemTopic;