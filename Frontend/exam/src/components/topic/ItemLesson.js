import React, { Component } from 'react';
import '../../css/ListTopic.css';

class ItemLesson extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        var { lesson } = this.props;
        var srcImage = "http://54.95.40.41:3000/public/images/lesson/ls_" + lesson.id + ".jpg";
        return (
            // <div className="item-lesson d-flex flex-column">
            //     <img src={srcImage} className="img-lesson rounded-0 float-left" alt="img"/>
            //     <div className="title-infor-lesson align-content-center">
            //         <div className="d-flex flex-column" style={{lineHeight: 1.1, fontSize: '0.875rem', margin: '10px'}}>
            //             <b>{lesson.lessonNameEn}</b>
            //             <span style={{marginTop: '0.5rem'}}>{lesson.totalVocab} từ</span>
            //         </div>
            //     </div>
            // </div>
            <div className="single-lesson-item">

                <img className="img-lesson" src={srcImage} alt="" />

                {/* <!-- Course Content --> */}
                <div className="lesson-content">
                    <h4>{lesson.lessonNameEn}</h4>
                    <div className="meta d-flex align-items-center">
                        <div style={{fontWeight: '500'}}>{lesson.totalVocab} từ</div>
                        {/* <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="https://colorlib.com/preview/theme/clever/?fbclid=IwAR2a8nOn-579AwAj5Us_klTyfGLsKrMN-6oB0ONYlvSX8AQswfODVre0ups#">Art &amp; Design</a> */}
                    </div>
                    {/* <p>{course.description}</p> */}
                </div>

                
            </div>
        );
    }
}

export default ItemLesson;