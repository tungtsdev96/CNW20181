import React, { Component } from 'react';
import callApi from '../../../utils/ApiCaller'

class LearnByWrite extends Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            fillQuestions: [],
            indexQuestion: 0,
            currentQuestion: '',
            titleQuestion: '',
            clickAnswer: false,
            answerCorrect: false
        }
    }

    componentWillMount() {
        var {idLesson} = this.props;
        callApi('GET', "http://54.95.40.41:3000/api/question/make-list-listen?idLesson=" + idLesson, {})
        .then( (response) => {
            var questions = response.data;
            this.setState({
                fillQuestions: response.data,
                indexQuestion: 0,
                currentQuestion: questions[0],
                titleQuestion: questions[0].vocabulary.vocabularyVi
            });
        })
    }

    render() {
        var {currentQuestion} = this.state;
        if (currentQuestion.vocabulary === undefined) return null;
        return (
            <div className="type-write">
                <div className="question d-flex justify-content-between">
                    <div id="title-vocabulary" style={{ fontSize: '2.5rem', paddingTop: '20px' }}>{currentQuestion.vocabulary.vocabularyVi}</div>
                    {/* <div
                        className="btn-dont-know d-flex align-items-center"
                        id="title-vocabulary"
                        style={{ float: 'right', fontSize: '0.875rem', cursor: 'pointer' }}>
                        Không biết
                    </div> */}
                </div>

                <div style={{ marginTop: '2.5rem', paddingTop: '2.5rem', borderTop: '0.125rem solid #f0f0f0' }} />
                <form className="row" onSubmit={this.onSubmitInput}>
                    <div className="col-md-10" style={{ marginBottom: '10px' }}>
                        <label className="sr-only" for="input-answer"></label>
                        <input
                            className="form-control"
                            type="text"
                            id="input-answer"
                            name="input-answer"
                            placeholder="Viết đáp án vào đây" 
                            ref={this.textInput}/>
                    </div>
                    <div className="col-auto">
                        <button
                            type="button"
                            className="btn mb-2 btn-answer"
                            onClick={this.handleClickAnswer}>
                            Trả lời
                        </button>
                    </div>
                </form>

                <div className={"question-notify " + (this.state.clickAnswer ? "" : "d-none")}>
                    <h3 
                        id="notify" 
                        style={this.state.answerCorrect ? styleCorrect : styleIncorrect}>
                        <b>{this.state.answerCorrect ? "Chính xác" : "Bạn đã sai"}</b>
                    </h3>
                    <div id="correct-answer" style={{marginTop: '10px'}}>
                        <h5><b style={{color: '#23b26d'}}>Đáp án đúng: {currentQuestion.vocabulary.vocabularyEn}</b></h5>
                    </div>
                    <button 
                        type="button" 
                        className="btn btn-info btn-register btn-continue" 
                        style = {{marginTop: 15}}
                        onClick={this.handleClickBtnContinue}>
                        Tiếp tục
                    </button>    
                </div>
            </div>
        );
    }

    onSubmitInput = (e) => {
        e.preventDefault();
    }

    handleClickAnswer = () => {
        var answer = this.textInput.current.value;
        var {currentQuestion} = this.state;
        var isCorrect = (currentQuestion.vocabulary.vocabularyEn.toUpperCase() == answer.toUpperCase());

        // update Ui
        this.setState({
            clickAnswer: true,
            answerCorrect: isCorrect
        })
        
        // update side bar
        var {handleSubmit} = this.props;
        handleSubmit(isCorrect);

        // update db
        callApi('POST', "http://54.95.40.41:3000/api/detail-learn-vocab/handle-score-vocab",{
            idVocab: currentQuestion.vocabulary.id,
            idUser: 1,
            idLesson: currentQuestion.vocabulary.lessonId,
            isCorrect: isCorrect
        }).then( (response) => {
            //console.log("learn", response.data);
        })
    }

    handleClickBtnContinue = () =>  {
        this.textInput.current.value = "";
        var {indexQuestion} = this.state; 
        var {fillQuestions} = this.state;
        if (indexQuestion < fillQuestions.length) {
            this.setState({
                indexQuestion: indexQuestion + 1,
                currentQuestion: fillQuestions[indexQuestion + 1], 
                titleQuestion: fillQuestions[indexQuestion + 1].vocabulary.vocabularyVi,
                clickAnswer: false,
                answerCorrect: false
            })
        } else {
            alert("aaaaa");
        }
    }

}

var styleCorrect = {
    color: '#23b26d'
}

var styleIncorrect = {
    color: '#ff725b'
}

export default LearnByWrite;