import React, { Component } from 'react';
import callApi from '../../../utils/ApiCaller';

class LearnByQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mutilQuestions: [],
            indexQuestion : 0,
            currentQuestion :{},
            titleQuestion: '',
            clickAnswer: false,
            answerCorrect: false
        }
    }

    componentWillMount() {
        var {idLesson} = this.props;
        callApi('GET', "http://54.95.40.41:3000/api/question/make-list-mutil-choose?idLesson=" + idLesson, {})
        .then( (response) => {
            var questions = response.data;
            this.setState({
                mutilQuestions: response.data,
                indexQuestion: 0,
                currentQuestion: questions[0],
                titleQuestion: questions[0].vocabulary.vocabularyVi
            });
        })
    }

    showQuestion = () => {
        var currentQuestion = this.state.currentQuestion;
        if (currentQuestion.vocabulary === undefined) return null;
        var srcImg = "http://54.95.40.41:3000/public/images/vocabulary/" + currentQuestion.vocabulary.vocabularyEn.replace(' ', '_') + ".jpg";
        return(
            <div className="type-learn">
                <div className="question">
                    <div id="text-question" style={{fontSize: '2.5rem', paddingTop: '20px'}}>
                        {this.state.titleQuestion}
                    </div>
                    <div className="image-question" style={{marginTop: '25px'}}>
                        <img src={srcImg} className="rounded float-left w-auto h-100" alt="img" />
                    </div>
                    <div className="choose-answer-question row">
                        <div className="col-md-3" style={{padding: '10px'}} onClick={() => this.handleClickAnswer(1)}>
                            <div className="answer p-2">
                                <span><b>1. </b></span>
                                <span>{currentQuestion.answerA}</span>
                            </div>
                        </div>
                        <div className="col-md-3" style={{padding: '10px'}} onClick={() => this.handleClickAnswer(2)}>
                            <div className="answer p-2">
                                <span><b>2. </b></span>
                                <span>{currentQuestion.answerB}</span>
                            </div>
                        </div>
                        <div className="col-md-3" style={{padding: '10px'}} onClick={() => this.handleClickAnswer(3)}>
                            <div className="answer p-2">
                                <span><b>3. </b></span>
                                <span>{currentQuestion.answerC}</span>
                            </div>
                        </div>
                        <div className="col-md-3" style={{padding: '10px'}} onClick={() => this.handleClickAnswer(4)}>
                            <div className="answer p-2">
                                <span><b>4. </b></span>
                                <span>{currentQuestion.answerD}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"question-notify " + (this.state.clickAnswer ? "" : "d-none")}>
                    <h3 
                        id="notify" 
                        style={this.state.answerCorrect ? styleCorrect : styleIncorrect}>
                        <b>{this.state.answerCorrect ? "Chính xác" : "Bạn đã sai"}</b>
                    </h3>
                    <div id="correct-answer" style={{marginTop: '10px'}}>
                        <h5><b style={{color: '#23b26d'}}>Đáp án đúng: </b>{currentQuestion.vocabulary.vocabularyEn}</h5>
                    </div>
                    <button 
                        type="button" 
                        className="btn btn-info btn-register btn-continue" 
                        style = {{marginTop: 15}}
                        onClick={this.handleClickBtnContinue}>
                        Tiếp tục
                    </button>    
                </div>
            </div>
        );
    }

    render() {
        return (
            this.showQuestion()
        );
    }

    handleClickAnswer(number){
        if (this.state.clickAnswer) return;

        var answer = "";
        var {currentQuestion} = this.state;
        switch(number) {
            case 1:
                answer = currentQuestion.answerA;
                break;
            case 2:
                answer = currentQuestion.answerB;
                break;
            case 3:
                answer = currentQuestion.answerC;
                break;
            case 4:
                answer = currentQuestion.answerD;
                break;
            
        }

        var isCorrect = (currentQuestion.vocabulary.vocabularyEn == answer);

        // update Ui
        this.setState({
            clickAnswer: true,
            answerCorrect: isCorrect
        })
        
        // update side bar
        var {handleSubmit} = this.props;
        handleSubmit(isCorrect);

        // update db
        callApi('POST', "http://54.95.40.41:3000/api/detail-learn-vocab/handle-score-vocab",{
            idVocab: currentQuestion.vocabulary.id,
            idUser: 1,
            idLesson: currentQuestion.vocabulary.lessonId,
            isCorrect: isCorrect
        }).then( (response) => {
            //console.log("learn", response.data);
        })
    }

    handleClickBtnContinue = () => {
        var {indexQuestion} = this.state;
        var {mutilQuestions} = this.state;
        if (indexQuestion < mutilQuestions.length) {
            this.setState({
                indexQuestion: indexQuestion + 1,
                currentQuestion: mutilQuestions[indexQuestion + 1], 
                titleQuestion: mutilQuestions[indexQuestion + 1].vocabulary.vocabularyVi,
                clickAnswer: false,
                answerCorrect: false
            })
        } else {
            alert("aaaaa");
        }
    }
    

}

var styleCorrect = {
    color: '#23b26d'
}

var styleIncorrect = {
    color: '#ff725b'
}

export default LearnByQuestion;