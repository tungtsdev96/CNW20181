import React, { Component } from 'react';
import callApi from '../../../utils/ApiCaller'

class LearnBySpell extends Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.state = {
            listenQuestions: [],
            indexQuestion: 0,
            currentQuestion: '',
            titleQuestion: '',
            clickAnswer: false,
            answerCorrect: false
        }
    }

    componentWillMount() {
        var { idLesson } = this.props;
        callApi('GET', "http://54.95.40.41:3000/api/question/make-list-listen?idLesson=" + idLesson, {})
            .then((response) => {
                var questions = response.data;
                this.setState({
                    listenQuestions: response.data,
                    indexQuestion: 0,
                    currentQuestion: questions[0],
                    titleQuestion: questions[0].vocabulary.vocabularyVi
                });
            })
    }

    render() {
        var { currentQuestion } = this.state;
        if (currentQuestion.vocabulary === undefined) return null;
        this.playAudio();
        return (
            <div className="type-spell p-3">
                <div className="row">
                    <div className="col-2 col-md-2 col-sm-2 play-audio">
                        <i 
                            className="fa fa-volume-up" 
                            aria-hidden="true"
                            style={{fontSize: '4.5rem'}}
                            onClick={this.playAudio}>
                        </i>
                    </div>
                    <div className="col-10 col-md-10 col-sm-10">
                        <form 
                            className="row" 
                            style={{marginTop: '1rem'}}
                            onSubmit={this.onSubmitInput}>
                            <div className="col-md-10" style={{ marginBottom: '10px' }}>
                                <label className="sr-only" for="input-answer"></label>
                                <input
                                    autoComplete="off"
                                    className="form-control"
                                    type="text"
                                    id="input-answer"
                                    name="input-answer"
                                    placeholder="Viết đáp án vào đây"
                                    ref={this.textInput} />
                            </div>
                            <div className="col-auto">
                                <button
                                    type="button"
                                    className="btn mb-2 btn-answer"
                                    onClick={this.handleClickAnswer}>
                                    Trả lời
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div style={{ marginTop: '2.5rem', paddingTop: '2.5rem', borderTop: '0.125rem solid #f0f0f0' }} />
                <div style={{marginBottom: '20'}}>
                    <span id="title-vocabulary">{currentQuestion.vocabulary.vocabularyVi}</span>
                </div>
                <div className={"question-notify " + (this.state.clickAnswer ? "" : "d-none")}>
                    <h3 
                        id="notify" 
                        style={this.state.answerCorrect ? styleCorrect : styleIncorrect}>
                        <b>{this.state.answerCorrect ? "Chính xác" : "Bạn đã sai"}</b>
                    </h3>
                    <div id="correct-answer" style={{marginTop: '10px'}}>
                        <h5><b style={{color: '#23b26d'}}>Đáp án đúng: {currentQuestion.vocabulary.vocabularyEn}</b></h5>
                    </div>
                    <button 
                        type="button" 
                        className="btn btn-info btn-register btn-continue" 
                        style = {{marginTop: 15}}
                        onClick={this.handleClickBtnContinue}>
                        Tiếp tục
                    </button>    
                </div>
            </div>
        );
    }

    playAudio = () => {
        var { currentQuestion } = this.state;
        var srcAudio = "http://54.95.40.41:3000/public/audio/vocabulary/" + currentQuestion.vocabulary.vocabularyEn.replace(' ', '_') + ".mp3";
        var audio = new Audio(srcAudio);
        audio.play();
    }

    onSubmitInput = (e) => {
        e.preventDefault();
    }

    handleClickAnswer = () => {
        var answer = this.textInput.current.value;
        var {currentQuestion} = this.state;
        var isCorrect = (currentQuestion.vocabulary.vocabularyEn.toUpperCase() == answer.toUpperCase());

        // update Ui
        this.setState({
            clickAnswer: true,
            answerCorrect: isCorrect
        })
        
        // update side bar
        var {handleSubmit} = this.props;
        handleSubmit(isCorrect);

        // update db
        callApi('POST', "http://54.95.40.41:3000/api/detail-learn-vocab/handle-score-vocab",{
            idVocab: currentQuestion.vocabulary.id,
            idUser: 1,
            idLesson: currentQuestion.vocabulary.lessonId,
            isCorrect: isCorrect
        }).then( (response) => {
            //console.log("learn", response.data);
        })
    }

    handleClickBtnContinue = () =>  {
        this.textInput.current.value = "";
        var {indexQuestion} = this.state; 
        var {listenQuestions} = this.state;
        if (indexQuestion < listenQuestions.length) {
            this.setState({
                indexQuestion: indexQuestion + 1,
                currentQuestion: listenQuestions[indexQuestion + 1], 
                titleQuestion: listenQuestions[indexQuestion + 1].vocabulary.vocabularyVi,
                clickAnswer: false,
                answerCorrect: false
            })
        } else {
            alert("aaaaa");
        }
    }

}

var styleCorrect = {
    color: '#23b26d',
    marginTop: '20px'
}

var styleIncorrect = {
    color: '#ff725b',
    marginTop: '20px'
}

export default LearnBySpell;