import React, { Component } from 'react';

class SideBarControls extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trueWord: 0,
            falseWord : 0,
            curentQuestion: 1
        }
    }

    componentWillMount() {
        
    }

    render() {
        var {totalQuestion} = this.props;
        return (
            <div className="mode-controls">
                <div className="controls-sidebar">
                    <div className="sidebar-back">
                        <a className="UIlink" href="#">
                            <i className="fa fa-chevron-circle-left" aria-hidden="true"></i>
                            <span className="sidebar-backText"><span>Back</span></span>
                        </a>
                    </div>
                    <div className="sidebar-main">
                        <div className="sidebar-main-name">
                            Học từ
                            </div>
                        <div className="sidebar-main-progress">
                            <h1 className="text-center">25%</h1>
                            <div className="progress">
                                <div className="progress-bar" role="progressbar" style={{width: '25%'}} aria-valuenow="25"
                                    aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div className="count-vocab-learn">
                                <div> 
                                    <span><b>{"Câu: "} </b></span>
                                    <span>{this.props.curentQuestion + " / " + totalQuestion}</span>
                                </div>
                                <div className="count-correct">
                                    <span><b>Đúng: </b></span>
                                    <span>{this.props.trueWord} từ</span>
                                </div>
                                <div className="count-incorrect">
                                    <span><b>Sai: </b></span>
                                    <span>{this.props.falseWord} từ</span>
                                </div>
                            </div>
                        </div>
                        <div className="sidebar-main-action">

                        </div>
                    </div>
                </div>
            </div>
        );
    }


}

export default SideBarControls;
