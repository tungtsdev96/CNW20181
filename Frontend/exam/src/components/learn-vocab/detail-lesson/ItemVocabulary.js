import React, { Component } from 'react';
import callApi from '../../../utils/ApiCaller'
import ModalDetailItemVocabulary from '../detail-lesson/ModalDetailItemVocabulary';

class ItemVocabulary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isFavorite: false,
            srcAudio: '',
            openModalDetail: false
        }
    }

    componentWillMount() {
        var {lesson} = this.props;
        callApi('GET', "http://54.95.40.41:3000/api/vocabulary/check-favorite?idUser=1&idLesson=" + lesson.id + "&idVocab=" + this.props.word.id ,{})
        .then( (response) => {
            this.setState({
                isFavorite: response.data.isFavorite
            })
        })
    }

    componentWillReceiveProps(nextProps){
    }

    render() {
        var {word} = this.props;
        var {isAnswered} = this.props;
        var srcImg = "http://54.95.40.41:3000/public/images/vocabulary/" + word.vocabularyEn.replace(' ', '_') + ".jpg";
        return (
            <div className="row item-vocab shadow-sm">
                <div className="score-vocab col-1 col-sm-1 col-md-1 col-lg-1 d-flex align-items-center justify-content-center">
                    <span>
                        <b>{isAnswered == true ? (word.score >= 0 ? "+" + word.score : word.score) : "?"}</b>
                    </span>
                </div>
                <div className="col-8 col-sm-8 col-md-8 col-lg-8 d-flex align-items-center" 
                    onClick={this.clickItemVocab}
                    style={{cursor:'pointer'}}>
                        <div className="content-vocab" >
                            <span className="vocab-en"><b>{word.vocabularyEn + " "}</b></span>
                            <span className="vocab-pronun">{" " + word.pronunciation}</span>
                            <p className="vocab-explantion">{word.explantion}</p>
                            <p className="voca-vi">{word.vocabularyVi}</p>
                        </div>
                </div>
                <div className="source-vocab col-3 col-sm-3 col-md-3 col-lg-3 d-flex align-items-center">
                    <img src={srcImg} className="rounded float-left w-50 h-auto" alt="img" />
                    <div style={styleMarginLeft}>
                        <div 
                            className="play-audio"
                            onClick={this.clickAudio}>
                            <i className="fa fa-volume-up" aria-hidden="true"></i>
                        </div>
                        <div 
                            className={this.state.isFavorite ? "star-vocab-favorite" : "star-vocab"}
                            onClick={this.clickFavorite}>
                                <i 
                                    className={this.state.isFavorite ? "fa fa-star" :"fa fa-star-o"} 
                                    aria-hidden="true">
                                </i>
                        </div>
                    </div>
                </div>
                <ModalDetailItemVocabulary vocab={word} open={this.state.openModalDetail}/>
            </div>
        );
    }   

    clickItemVocab = () => {
        // this.setState({
        //     openModalDetail: !this.state.openModalDetail
        // })
        var {word} = this.props;
        var srcAudio = "http://54.95.40.41:3000/public/audio/vocabulary/" + word.vocabularyEn.replace(' ', '_') + ".mp3";
        var audio = new Audio(srcAudio);
        audio.play();
    }

    clickAudio = () => {
        var {word} = this.props;
        var srcAudio = "http://54.95.40.41:3000/public/audio/vocabulary/" + word.vocabularyEn.replace(' ', '_') + ".mp3";
        var audio = new Audio(srcAudio);
        audio.play();
    }

    clickFavorite = () => {
        var {isFavorite} = this.state;
        if (!isFavorite) {
            callApi('POST', "http://54.95.40.41:3000/api/vocabulary/add-to-favorite", {
                idVocab: this.props.word.id,
                idUser: 1,
                idLesson: this.props.lesson.id
            }).then((response) => {
                if (!response.data) alert('Đã có lỗi xảy ra');
                this.setState({
                    isFavorite : !isFavorite,
                    openModalDetail: false
                })
            })
        } else {
            callApi('delete', "http://54.95.40.41:3000/api/vocabulary/delete-vocab-favorite", {
                idVocab: this.props.word.id,
                idUser: 1,
            }).then((response) => {
               if (!response.data) alert('Đã có lỗi xảy ra');
                this.setState({
                    isFavorite : !isFavorite,
                    openModalDetail: false
                })
            })
        }
    }

}

const styleMarginLeft = {
    marginLeft: 50,
};

export default ItemVocabulary;