import React, { Component } from 'react';
import callApi from '../../../utils/ApiCaller';

import './CarouselPreviewsVocabulary.css';

class CarouselPreviewsVocabulary extends Component {
    constructor(props){
        super(props);
        this.state={
            lesson: {},
            listVocabOfLesson: [],
            indexCurrentVocab : 0,
        }
    }
    
    showCarouselOfWord = (words) => {
        var result = null;
        result = words.map( (word, index) => {
            var {indexCurrentVocab} = this.state;
            return(
                <div key={index} className={indexCurrentVocab == index ? "carousel-item active text-center" : "carousel-item text-center"} key={index}>
                    <div className="jumbotron bg-white d-flex align-items-center justify-content-center">
                        <div className="d-flex-inline">
                            <h1>{word.vocabularyEn}</h1>
                            <h5>{word.vocabularyVi}</h5>
                        </div>
                    </div>
                </div>
            );
        });
        return result;
    }
    
    componentWillReceiveProps(nextProps) {
        var {lesson} = nextProps;
        this.setState({
            lesson: lesson
        })

        callApi('GET', "http://54.95.40.41:3000/api/vocabulary/get-list-vocab-by-id-lesson?idLesson=" + lesson.id, {})
        .then( (response) => {
        // console.log("aa",  response.data);
            this.setState({
                listVocabOfLesson: response.data,
                indexCurrentVocab : 0,
           });
        })
    }

    componentWillMount () {
        var {lesson} = this.props;
        this.setState({
            lesson: lesson
        })
    }

    render() {
        var words = this.state.listVocabOfLesson; 
        var {lesson} = this.state;

        return (
            // <!-- flash card vocabulary -->
            <div className="card-previews">
                <div id="carousel-vocabulary" className="carousel slide" data-ride="carousel">
                    <div className="carousel-inner">
                        {this.showCarouselOfWord(words)}
                    </div>
                    <div className="control-flashcard  text-center d-flex align-items-center justify-content-center" style={{cursor: 'pointer'}}>
                        <span className="previous-control" onClick={this.clickPreviousWord}>
                            <i className="fa fa-chevron-circle-left icon-control-flashcard" aria-hidden="true"></i>
                        </span>
                        <span className="seq-vocab">{this.state.indexCurrentVocab < 9 ? "0" + (this.state.indexCurrentVocab + 1) : this.state.indexCurrentVocab + 1}/{lesson.totalVocab}</span>
                        <span className="next-control" onClick={this.clickNextWord}>
                            <i className="fa fa-chevron-circle-right icon-control-flashcard" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>
        );
    }

    clickPreviousWord = () => {
        var {indexCurrentVocab} = this.state;
        if (indexCurrentVocab != 0) {
            this.setState({
                indexCurrentVocab: indexCurrentVocab - 1
            })
        }
    }

    clickNextWord = () => {
        var {indexCurrentVocab} = this.state;
        var {lesson} = this.state;
        if (indexCurrentVocab != lesson.totalVocab - 1) {
            this.setState({
                indexCurrentVocab: indexCurrentVocab + 1
            })
        }
    }
}

export default CarouselPreviewsVocabulary;
