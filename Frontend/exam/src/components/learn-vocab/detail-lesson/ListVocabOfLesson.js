import React, { Component } from 'react';
import ItemVocabulary from '../detail-lesson/ItemVocabulary'
import callApi from '../../../utils/ApiCaller'

class ListVocabOfLesson extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lesson: {},
            listVocab: [],
            listVocabOftenMissed: [],
            listVocabNeverMind: [],
            ListVocabNoAnswerYet: []            
        }
    }

    componentWillReceiveProps(nextProps) {
        var {lesson} = nextProps;
        this.setState({
            lesson: lesson
        })

        // check login 
        // if login 

            // get list vocab often missed
            callApi('GET', "http://54.95.40.41:3000/api/detail-learn-vocab/get-vocab-learn-by-user?idUser=1&idLesson="+ lesson.id + "&type=1", {})
            .then( (response) => {
                this.setState({
                    listVocabNeverMind: response.data,
                });
            })

            // get list vocab never mind
            callApi('GET', "http://54.95.40.41:3000/api/detail-learn-vocab/get-vocab-learn-by-user?idUser=1&idLesson="+ lesson.id + "&type=2", {})
            .then( (response) => {
                this.setState({
                    listVocabOftenMissed: response.data,
                });
            })

            // get list vocab no ansewer yet
            callApi('GET', "http://54.95.40.41:3000/api/detail-learn-vocab/get-vocab-learn-by-user?idUser=1&idLesson="+ lesson.id + "&type=3", {})
            .then( (response) => {
                this.setState({
                    ListVocabNoAnswerYet: response.data,
                });
            })

        // else 

            // get list vocab of lesson
    }

    showListVocab = (isAnswered, words) => {
        var result = null;
        result = words.map( (word, index) => {
            return (
                <ItemVocabulary key={index} isAnswered={isAnswered} word={word} lesson={this.state.lesson}></ItemVocabulary>
            );
        });
        return result;
    }

    showItemMainCommponent = (title, listVocab, isAnswered) => {
       
       return (
           <div className={"vocab-learned " + (listVocab.length > 0 ? "" : "d-none")}>
               <div className="title-list-vocab">
                   <b>{title}</b>
               </div>
               <div className="list-vocab">
                   {this.showListVocab(isAnswered, listVocab)}
               </div>
           </div>
       );
    }

    render() {
        var {listVocabOftenMissed} = this.state;
        var {listVocabNeverMind} = this.state;
        var {ListVocabNoAnswerYet} = this.state;

        return (
            // list vocabulary of lesson
            <div className="list-vocab-in-lesson">

                {this.showItemMainCommponent("Sai nhiều", listVocabOftenMissed, true)}

                {this.showItemMainCommponent("Đúng nhiều", listVocabNeverMind, true)}

                {this.showItemMainCommponent("Chưa học", ListVocabNoAnswerYet, false)}
                
            </div>
        );
    }
}

export default ListVocabOfLesson;
