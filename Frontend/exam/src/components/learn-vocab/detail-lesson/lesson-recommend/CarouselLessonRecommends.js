import React, { Component } from 'react';
import './LessonRecommend.css';
import ListLessonRecommend from './ListLessonRecommend.js';
import callApi from '../../../../utils/ApiCaller'

class CarouselLessonRecommends extends Component {

    constructor(props) {
        super(props);  
        this.state = {
            listRecommendLesson : [],
            lesson: {},
            nextProps: ''
       }
    }
    
    componentWillMount() {
        callApi('GET', "http://54.95.40.41:3000/api/lesson/get-recommend-lesson", {})
            .then( (response) => {
                this.setState({
                    listRecommendLesson: response.data,
                });
            })
    }

    componentWillReceiveProps(nextProps){
        //console.log(nextProps);
        this.setState({
            lesson: nextProps.lesson,
            nextProps: nextProps
        })
    }

    showCarouselItem = (isActived, lessons, key) => {
        var result = null;
        result = (
            <div className={isActived == true ? "carousel-item active" : "carousel-item"} key={key}> 
                <ListLessonRecommend lessons={lessons} handleClickItemLesson={this.handleClickItemLesson}/>
            </div>
        );
        return result;
    }

    handleClickItemLesson = (lesson) => {
        this.state.nextProps.handleClickItemLesson(lesson);
    }
    // showItem = (listRecommendLesson) => {
    //     var result = null;
    //     result = listRecommendLesson.map( (element, index) => {
    //         var {currentIndex} = this.state;
    //         return this.showCarouselItem(index == currentIndex, element, index);
    //     });
    //     return result;
    // }

    render() {  
        var {listRecommendLesson} = this.state;
        return (
            <div>
                <div id="card-lesson" className="carousel slide" data-ride="carousel" ref={this.recommendLesson}>
                    <h5 style={mrgLeft}><b>Bài học liên quan</b></h5>
                    <div className="carousel-inner">
                       {this.showCarouselItem(true, listRecommendLesson, 1)}
                    </div>

                    {/*  control list leson */}
                    {/* <div className="d-flex justify-content-center">
                        <div id="btn-prev-lesson" className="d-inline-block" style={{ backgroundColor: 'turquoise', width: 100, height: 100, }}>

                        </div>
                        <div    
                             //onClick={this.clickNextRecommendLesson(array)}
                             id="btn-next-lesson" 
                             className="d-inline-block" 
                             style={{ backgroundColor: 'slateblue', width: 100, height: 100}}>
                        </div>
                    </div> */}

                </div>
            </div>
        );
    }

    // clickNextRecommendLesson (array) {
    //     var {currentIndex} = this.state;
    //     if (currentIndex < array.length - 1) {
    //         currentIndex++;
    //     } else {
    //         currentIndex = 0;
    //     }
    //     this.setState({
    //         currentIndex: currentIndex,
    //     })
    // }

}

const mrgLeft = {
    marginLeft: 15
}

export default CarouselLessonRecommends;