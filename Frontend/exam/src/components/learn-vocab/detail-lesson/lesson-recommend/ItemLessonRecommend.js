import React, { Component } from 'react';
import './LessonRecommend.css';

class ItemLessonRecommend extends Component {

    constructor(props){
        super(props);
        this.state={
            lesson: this.props.lesson,
        }
    }

    render() {
        
        var lesson = this.state.lesson;
        
        return (
            <div className="col-md-6 col-lg-6" onClick={() => this.clickItemRecommendLesson(lesson)}>
                <div className="item-lesson shadow-sm">
                    <div style={mrgBottom}>
                        <p className="title-lesson">{lesson.lessonNameEn + ' - ' + lesson.lessonNameVi}</p>
                        <p className="title-lesson-count">{lesson.lessonNameVi + ' ' + "từ"}</p>
                    </div>
                    <div>
                        <span className="d-inline-block">
                            <div className="rounded-circle shadow-sm" style={styleAuthor}>
                                s
                            </div>
                        </span>
                        <span className="d-inline-block" style={mrgLeft}>Sơn Tùng</span>
                    </div>
                </div>
            </div>
        );
    }

    clickItemRecommendLesson = (lesson) => {
        this.props.handleClickItemLesson(lesson);
    }

    componentDidUpdate() {
        window.scrollTo(0, 0)
    }

}

const mrgBottom = {
    marginBottom : 25
}

const mrgLeft = {
    marginLeft : 5
}

const styleAuthor = {
    width: 30,
    height: 30,
    textAlign: 'center',
    backgroundColor: "#0096e0",
    color: '#FFF'
}

export default ItemLessonRecommend;