import React, { Component } from 'react';
import './LessonRecommend.css';
import ItemLessonRecommend from './ItemLessonRecommend.js';

class ListLessonRecommend extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lessons: this.props,
            nextProps: ''
        }
    }

    componentWillReceiveProps(nextProps) {
        //console.log("tungts", nextProps);
        this.setState({
            lessons: nextProps.lessons,
            nextProps: nextProps
        })
    }

    showCarouselOflesson = (lessons) => {
        var result = null;
        result = lessons.map((lesson, index) => {
            return (
                <ItemLessonRecommend lesson = {lesson} key={index} handleClickItemLesson={this.handleClickItemLesson}/>
            );
        });
        return result;
    }

    render() {
        
        var  {lessons} = this.state.nextProps;
        if (!lessons) return null;

        return (
            <div className="list-lesson row">
                {this.showCarouselOflesson(lessons)}
            </div>
        );
    }

    handleClickItemLesson = (lesson) => {
        this.state.nextProps.handleClickItemLesson(lesson);
    }
}



export default ListLessonRecommend;
