import React, { Component } from 'react';
import Modal from 'react-responsive-modal';

class ModalDetailItemVocabulary extends Component {

    constructor(props){
        super(props);
        this.state = {
            openModal: this.props.open
        }
    }

    componentWillReceiveProps(nextProps){
        this.setState ({
            openModal: nextProps.open
        })
    }

    render() {
        var {vocab} = this.props;
        return(
            <Modal 
                open={this.state.openModal}
                showCloseIcon={false}
                center
                onClose={this.closeModal}
                onEscKeyDown={this.closeModal}
                onOverlayClick={this.closeModal}>

                <h2>tungts</h2>

            </Modal>
        )
    }

    closeModal = () => {
        this.setState({
            openModal: false
        })
    }

}

export default ModalDetailItemVocabulary;