import React, { Component } from 'react';
class MenuItem extends Component {
    render() {
        var iconItemMenu = this.props.iconItemMenu;
        var menuItemName = this.props.menuItemName;
        var classNameOfIconItem = "icon-menu-study fa " + iconItemMenu;
        return (
            <span className="menu-study-item">
                    <div id="card-item" className="shadow-lg h-100 d-inline-block bg-white rounded-0">
                        <i className={classNameOfIconItem}></i>
                        <p className="item-name">{menuItemName}</p>
                    </div>
            </span>
        );
    }
}

export default MenuItem;
