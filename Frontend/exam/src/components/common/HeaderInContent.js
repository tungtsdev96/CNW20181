import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import srcLogo from '../../assets/images/logo.png'
import '../../css/Course.css'

class HeaderInContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser: null
        }
    }

    componentWillMount() {
        this.setState({
            currentUser: JSON.parse(localStorage.getItem("currentUser") + "")
        })
    }

    showInfoUser = () => {
        var { currentUser } = this.state;
        console.log(currentUser)
        if (currentUser) {
            return (
                <ul className="main-menu nav navbar-nav navbar-right d-flex flex-row">
                    <li><Link to='/Info'>Xin chào {currentUser.name}</Link></li>
                    <li><Link to='/Logout'>Đăng xuất</Link></li>
                </ul>
            )
        } else {
            return (
                <ul className="main-menu nav navbar-nav navbar-right d-flex flex-row">
                    <li><Link to='/Login'>Đăng nhập</Link></li>
                    <li><Link to='/SignUp'>Đăng kí</Link></li>
                </ul>
            )
        }
    }

    render() {
        return (
            // {/* header */ }
            <header className="header-area fixed-top">
                <div className="container">
                    <div className="navbar-header">
                        {/* <!-- Logo --> */}
                        <div className="navbar-brand">
                            <Link to="/">
                                <div className="logo">
                                    <img src={srcLogo} alt="logo" />
                                </div>
                            </Link>
                        </div>
                        {/* <!-- /Logo --> */}
                        {/* <!-- Mobile toggle --> */}
                        <button className="navbar-toggle">
                            <span></span>
                        </button>
                        {/* <!-- /Mobile toggle --> */}
                    </div>

                    {/* <!-- Navigation --> */}
                    <nav id="nav">
                        <ul className="main-menu nav navbar-nav navbar-left d-flex flex-row">
                            <li><Link to='/'>Trang chủ</Link></li>
                            <li><Link to='/TestList'>Thi Thử</Link></li>
                            <li><Link to='/ListTopic'>Học từ vựng</Link></li>
                            <li><Link to='/Course'>Khóa học</Link></li>
                        </ul>
                        {this.showInfoUser()}
                    </nav>

                    {/* <!-- /Navigation --> */}
                </div>
            </header >
        );
    }
}

export default HeaderInContent;
