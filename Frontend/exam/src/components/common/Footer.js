import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import srcLogo from '../../assets/images/logo.png'
import '../../css/StyleHome.css'

class Footer extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                
                {/* <!-- /Contact CTA --> */}
                {/* <!-- Footer --> */}
                <footer id="footer" className="section">
                    <hr/>
                    {/* <!-- container --> */}
                    <div className="container">
                        {/* <!-- row --> */}
                        <div className="row">
                            {/* <!-- footer logo --> */}
                            <div className="col-md-6">
                                <div className="footer-logo">
                                    <a className="logo" href="https://colorlib.com/etc/edusite/index.html">
                                        <img src={srcLogo} alt="logo" />
                                    </a>
                                </div>
                            </div>
                            {/* <!-- footer logo --> */}
                            {/* <!-- footer nav --> */}
                            <div className="col-md-6">
                                <ul className="footer-nav">
                                    <li><a href="https://colorlib.com/etc/edusite/index.html">Home</a></li>
                                    <li><a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">About</a></li>
                                    <li><a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#">Courses</a></li>
                                    <li><a href="https://colorlib.com/etc/edusite/blog.html">Blog</a></li>
                                    <li><a href="https://colorlib.com/etc/edusite/contact.html">Contact</a></li>
                                </ul>
                            </div>
                            {/* <!-- /footer nav --> */}
                        </div>
                        {/* <!-- /row --> */}
                        {/* <!-- row --> */}
                        <div id="bottom-footer" className="row">
                            {/* <!-- social --> */}
                            <div className="col-md-4 col-md-push-8">
                                <ul className="footer-social">
                                    <li><a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="facebook"><i className="fa fa-facebook"></i></a></li>
                                    <li><a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="twitter"><i className="fa fa-twitter"></i></a></li>
                                    <li><a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="google-plus"><i className="fa fa-google-plus"></i></a></li>
                                    <li><a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="instagram"><i className="fa fa-instagram"></i></a></li>
                                    <li><a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="youtube"><i className="fa fa-youtube"></i></a></li>
                                    <li><a href="https://colorlib.com/etc/edusite/index.html?fbclid=IwAR1sr9aVmgOZuApZFbxy9R5vQUtJOzJeS3NTV303TXTrN7uBaBRUI2rzVa0#" className="linkedin"><i className="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                            {/* <!-- /social --> */}
                            {/* <!-- copyright --> */}
                            <div className="col-md-8 col-md-pull-4">
                                <div className="footer-copyright">
                                    <span>© Copyright 2018. All Rights Reserved. | This template is made with <i className="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com/">Colorlib</a></span>
                                </div>
                            </div>
                            {/* <!-- /copyright --> */}
                        </div>
                        {/* <!-- row --> */}
                    </div>
                    {/* <!-- /container --> */}
                </footer>

            </div>
        );
    }

}

export default Footer;